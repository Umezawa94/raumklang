class Renderer {
    constructor(gl){
        this.gl = gl;

        this.programInfo = new SoundProgram(gl);
        this.wireProgram = new SoundProgram(gl);
        
        this.buffers = new WebGLAttributes(gl);

        this.projScreenMatrix = new THREE.Matrix4();
        this.rot4Mat = new THREE.Matrix4();
    
        this.drawMode = gl.TRIANGLE_STRIP;
        this.isReady = false;
        Promise.all([this.programInfo.ready, this.wireProgram.ready]).then(()=>{
          this.isReady = true;
        });
    }

    getRenderList(scene){
      let list = [];
      for (var i = 0; i < scene.children.length; i++) {
        let mesh = scene.children[i];
        if(mesh.isMesh|| mesh.isLine){
          list.push(mesh);
        }
        list = list.concat(this.getRenderList(mesh));
        
        // this.shapes[i].render(programInfo);
      }
      return list;
    }

    render(scene, camera, frameBuffer){
        if(!this.isReady) return;
        const gl = this.gl;

        if(frameBuffer){
          frameBuffer.bind();
        } else {
          gl.bindFramebuffer(gl.FRAMEBUFFER, null);
          gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
        }

        if ( scene.autoUpdate === true ) scene.updateMatrixWorld();

        // update camera matrices and frustum
  
        if ( camera.parent === null ) camera.updateMatrixWorld();
        this.projScreenMatrix.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );
    
        let clear = true;
        if (clear){
          gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
          gl.clearDepth(1.0);                 // Clear everything
          gl.enable(gl.DEPTH_TEST);           // Enable depth testing
          gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
    
          // Clear the canvas before we start drawing on it.
    
          gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        }
    
        gl.disable(gl.BLEND);
        // gl.blendFunc(gl.ONE, gl.ZERO);
        // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
    
        gl.enable(gl.CULL_FACE);
        // gl.cullFace(gl.FRONT_AND_BACK);
    
    
    
        for (var i = 0; i < scene.children.length; i++) {
          let light = scene.children[i];
          if(light.isPointLight){
            this.lightpos = light.position;
          }
        }
    
        let renderList = this.getRenderList(scene);
        // renderList.sort((a,b)=>{
        //   a = a.isMesh?1:0;
        //   b = b.isMesh?1:0;
        //   return  b-a;
        // });
        for (var i = 0; i < renderList.length; i++) {
          let mesh = renderList[i];
          this.renderMesh(mesh,camera);
        }
    
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
      }

      renderMesh(mesh,camera){
          const gl = this.gl;
          let programInfo = this.programInfo;
          // debugger;
          if(mesh.material && mesh.material.isShaderProgram){
            if(mesh.material.isReady){
              programInfo = mesh.material;
            } else {
              return;
            } 
          }
          
          const geometry = mesh.geometry;
          if(!geometry.isBufferGeometry){
            throw "unsupported geometry";
          }
          // console.log(geometry.attributes);
          // mesh.modelViewMatrix.multiplyMatrices( camera.matrixWorldInverse, mesh.matrixWorld );



          // Tell WebGL to use our program when drawing
          gl.useProgram(programInfo.program);

          for (let i = 0; i < 10; i++) {
            gl.disableVertexAttribArray(i);
          }

          if(programInfo.uniformLocations.projectionMatrix !== undefined){
            if (camera){
              gl.uniformMatrix4fv(
                programInfo.uniformLocations.projectionMatrix,
                false,
                this.projScreenMatrix.elements);
              let campos = camera.position;
              gl.uniform4f(
                  programInfo.uniformLocations.cameraPosition, campos.x, campos.y, campos.z, 1);
            }
          }
          if(programInfo.uniformLocations.soundPosition && this.lightpos){
            gl.uniform4f(programInfo.uniformLocations.soundPosition, this.lightpos.x, this.lightpos.y, this.lightpos.z, 1);
          }

          this.rot4Mat.makeRotationFromEuler ( mesh.rotation );
          mesh.normalMatrix.setFromMatrix4 (this.rot4Mat);
      
          if(programInfo.uniformLocations.modelViewMatrix !== undefined){
            gl.uniformMatrix4fv(
              programInfo.uniformLocations.modelViewMatrix,
              false,
              mesh.matrixWorld.elements);
          }
      
          if(programInfo.uniformLocations.normalMatrix !== undefined){
            gl.uniformMatrix3fv(
              programInfo.uniformLocations.normalMatrix,
              false,
              mesh.normalMatrix.elements);
          }
          
      
          // if(programInfo.uniformLocations.drawMode !== undefined){
          //   gl.uniform1i(
          //     programInfo.uniformLocations.drawMode,
          //     this.drawMode == gl.LINE_STRIP ? 1 : 0);
          // }
      
          // if(programInfo.uniformLocations.tex !== undefined){
          //   gl.activeTexture(gl.TEXTURE0);
          //   gl.bindTexture(gl.TEXTURE_2D, this.texture);
          //   gl.uniform1i(programInfo.uniformLocations.tex, 0);
          // }
      
           // Tell WebGL how to pull out the positions from the position
           // buffer into the vertexPosition attribute.
           if(programInfo.attribLocations.vertexPosition !== undefined)
           {
             const numComponents = 3;  // pull out 2 values per iteration
             const type = gl.FLOAT;    // the data in the buffer is 32bit floats
             const normalize = false;  // don't normalize
             const stride = 0;         // how many bytes to get from one set of values to the next
                                       // 0 = use type and numComponents above
             const offset = 0;         // how many bytes inside the buffer to start from
             gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.get(geometry.attributes.position));
             gl.vertexAttribPointer(
                 programInfo.attribLocations.vertexPosition,
                 numComponents,
                 type,
                 normalize,
                 stride,
                 offset);
             gl.enableVertexAttribArray(
                 programInfo.attribLocations.vertexPosition);
           }
      
           if(programInfo.attribLocations.normal !== undefined && geometry.attributes.normal)
           {
             const numComponents = 3;  // pull out 2 values per iteration
             const type = gl.FLOAT;    // the data in the buffer is 32bit floats
             const normalize = false;  // don't normalize
             const stride = 0;         // how many bytes to get from one set of values to the next
                                       // 0 = use type and numComponents above
             const offset = 0;         // how many bytes inside the buffer to start from
             gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.get(geometry.attributes.normal));
             gl.vertexAttribPointer(
                 programInfo.attribLocations.normal,
                 numComponents,
                 type,
                 normalize,
                 stride,
                 offset);
             gl.enableVertexAttribArray(
                 programInfo.attribLocations.normal);
           }
      
      
          if(programInfo.attribLocations.uv !== undefined && geometry.attributes.uv)
          {
            const numComponents = 2;  // pull out 2 values per iteration
            const type = gl.FLOAT;    // the data in the buffer is 32bit floats
            const normalize = false;  // don't normalize
            const stride = 0;         // how many bytes to get from one set of values to the next
                                      // 0 = use type and numComponents above
            const offset = 0;         // how many bytes inside the buffer to start from
            gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.get(geometry.attributes.uv));
            gl.vertexAttribPointer(
                programInfo.attribLocations.uv,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.uv);
          }
      
          if(programInfo.attribLocations.color !== undefined)
          {
            const numComponents = 4;  // pull out 2 values per iteration
            const type = gl.FLOAT;    // the data in the buffer is 32bit floats
            const normalize = false;  // don't normalize
            const stride = 0;         // how many bytes to get from one set of values to the next
                                      // 0 = use type and numComponents above
            const offset = 0;         // how many bytes inside the buffer to start from
            gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.get(geometry.attributes.color));
            gl.vertexAttribPointer(
                programInfo.attribLocations.color,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.color);
          }
      

          let drawMode = null;
          if(mesh.isMesh) {
            drawMode = gl.TRIANGLE_STRIP;
            
            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.BLEND);
            // gl.blendFunc(gl.ONE, gl.ONE);
          }
          if(mesh.isLine) {
            drawMode = gl.LINE_STRIP;

            gl.disable(gl.DEPTH_TEST);
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.ONE, gl.ZERO);
          };
          if(mesh.isLineSegments) drawMode = gl.LINES;
          if(mesh.isLineLoop) drawMode = gl.LINE_LOOP;
           // Set the shader uniforms
      
           {
             const offset = 0;
             const vertexCount = geometry.attributes.position.count;
             gl.drawArrays(drawMode, offset, vertexCount);
             // gl.drawArrays(gl.POINTS, offset, vertexCount);
      
           }
      
      }
}



function WebGLAttributes( gl ) {

  var buffers = {};

  function createBuffer( attribute, bufferType ) {

    var array = attribute.array;
    var usage = attribute.dynamic ? gl.DYNAMIC_DRAW : gl.STATIC_DRAW;

    var buffer = gl.createBuffer();

    gl.bindBuffer( bufferType, buffer );
    gl.bufferData( bufferType, array, usage );

    attribute.onUploadCallback();

    var type = gl.FLOAT;

    if ( array instanceof Float32Array ) {

      type = gl.FLOAT;

    } else if ( array instanceof Float64Array ) {

      console.warn( 'THREE.WebGLAttributes: Unsupported data buffer format: Float64Array.' );

    } else if ( array instanceof Uint16Array ) {

      type = gl.UNSIGNED_SHORT;

    } else if ( array instanceof Int16Array ) {

      type = gl.SHORT;

    } else if ( array instanceof Uint32Array ) {

      type = gl.UNSIGNED_INT;

    } else if ( array instanceof Int32Array ) {

      type = gl.INT;

    } else if ( array instanceof Int8Array ) {

      type = gl.BYTE;

    } else if ( array instanceof Uint8Array ) {

      type = gl.UNSIGNED_BYTE;

    }

    return {
      buffer: buffer,
      type: type,
      bytesPerElement: array.BYTES_PER_ELEMENT,
      version: attribute.version
    };

  }

  function updateBuffer( buffer, attribute, bufferType ) {

    var array = attribute.array;
    var updateRange = attribute.updateRange;

    gl.bindBuffer( bufferType, buffer );

    if ( attribute.dynamic === false ) {

      gl.bufferData( bufferType, array, gl.STATIC_DRAW );

    } else if ( updateRange.count === - 1 ) {

      // Not using update ranges

      gl.bufferSubData( bufferType, 0, array );

    } else if ( updateRange.count === 0 ) {

      console.error( 'THREE.WebGLObjects.updateBuffer: dynamic THREE.BufferAttribute marked as needsUpdate but updateRange.count is 0, ensure you are using set methods or updating manually.' );

    } else {

      gl.bufferSubData( bufferType, updateRange.offset * array.BYTES_PER_ELEMENT,
        array.subarray( updateRange.offset, updateRange.offset + updateRange.count ) );

      updateRange.count = - 1; // reset range

    }

  }

  //

  function get( attribute ) {

    if ( attribute.isInterleavedBufferAttribute ) attribute = attribute.data;
    let buffer = buffers[ attribute.uuid ];
    if(buffer === undefined){
      let res = createBuffer(attribute, gl.ARRAY_BUFFER)
      buffer = buffers[ attribute.uuid ] = res.buffer;
    }
    return buffer;

  }

  function remove( attribute ) {

    if ( attribute.isInterleavedBufferAttribute ) attribute = attribute.data;

    var data = buffers[ attribute.uuid ];

    if ( data ) {

      gl.deleteBuffer( data.buffer );

      delete buffers[ attribute.uuid ];

    }

  }

  function update( attribute, bufferType ) {

    if ( attribute.isInterleavedBufferAttribute ) attribute = attribute.data;

    var data = buffers[ attribute.uuid ];

    if ( data === undefined ) {

      buffers[ attribute.uuid ] = createBuffer( attribute, bufferType );

    } else if ( data.version < attribute.version ) {

      updateBuffer( data.buffer, attribute, bufferType );

      data.version = attribute.version;

    }

  }

  return {

    get: get,
    remove: remove,
    update: update

  };

}