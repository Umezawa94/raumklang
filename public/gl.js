
class GLStuff {
  constructor(opt) {
    opt = opt || {};

    this.viewOnAudio = opt.viewOnAudio;
    this.controlMic = false;
    this.displayRays = opt.displayRays;

    this.measureResolution = 5;

    let canvas = this.canvas = document.querySelector("#glCanvas");
    // Initialize the GL context
    let gl = this.gl = canvas.getContext("webgl2");
    gl.getExtension('EXT_color_buffer_float');
    this.clock = new THREE.Clock();
    this.stats = new Stats();


    let scene = this.scene = new THREE.Scene();
    let renderer = this.renderer = new Renderer(gl);
    let sceneRaycast = this.sceneRaycast  = new SceneRaycast(gl, opt);

    // let basicMaterial = new THREE.MeshPhongMaterial( { color: 0x00ff00 } );
    let shapesLib = this.shapesLib = new ShapesLib(gl);

    let wallColor = new THREE.Color(0.5,0.5,0.3);
    //Walls
    let plane = shapesLib.createPlane( 2.0,1, {wireframe: true, color: wallColor});
    plane.position.set(0,0,-1.0);
    plane.rotation.set(0,Math.PI*1.0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    plane = shapesLib.createPlane( 2.5,1, {wireframe: true, color: wallColor});
    plane.position.set(-1.0,0,0.25);
    plane.rotation.set(0,-Math.PI*0.5,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    plane = shapesLib.createPlane( 2.5,1, {wireframe: true, color: wallColor});
    plane.position.set(+1.0,0,0.25);
    plane.rotation.set(0,+Math.PI*0.5,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    plane = shapesLib.createPlane( 1.5,1, {wireframe: true, color: wallColor});
    plane.position.set(0.25,0,+1.0);
    plane.rotation.set(0,Math.PI*0.0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);
    
    plane = shapesLib.createPlane( 1.5,1, {wireframe: true, color: wallColor});
    plane.position.set(0.25,0,+1.0);
    plane.rotation.set(0,Math.PI*1.0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    plane = shapesLib.createPlane( 2.0,1, {wireframe: true, color: wallColor});
    plane.position.set(0.0,0,+1.5);
    plane.rotation.set(0,Math.PI*0.0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    //Ceiling
    plane = shapesLib.createPlane( 2.0*10,2.5*10 );
    plane.position.set(0,+0.5,0.25);
    plane.rotation.set(-Math.PI/2,0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);
    //Floor
    plane = shapesLib.createPlane( 2.0*10,2.5*10 );
    plane.position.set(0,-0.5,0.25);
    plane.rotation.set(Math.PI/2,0,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);

    let subWallColor = new THREE.Color(0.8,0.,0.)
    //SubWalls
    plane = shapesLib.createPlane( 1,1, {wireframe: true, color: subWallColor})
    plane.position.set(0,0,-0.7);
    plane.rotation.set(0,+Math.PI*0.25,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);


    plane = shapesLib.createPlane( 1,1, {wireframe: true, color: subWallColor});
    plane.position.set(0,0,-0.7);
    plane.rotation.set(0,+Math.PI*1.25,0);
    scene.add(plane);
    sceneRaycast.addShape(plane);


    let sound = new THREE.PointLight( 0xffffff, 1, 100 );
    sound.position.set(-0.8,+0.3,-0.8);
    // let sound = new SoundSource(gl).translate(-0.8,+0.3,-0.8);
    scene.add(sound);
    sceneRaycast.addSound(sound);

    let soundMarker = this.shapesLib.createPlane( 0.02, 0.02, {color: new THREE.Color(1.0,0.0,0.0), shadeless: true} );
    soundMarker.position.copy(sound.position);
    soundMarker.rotation.set(Math.PI/2,0,0);
    this.scene.add(soundMarker);


    sceneRaycast.init();

    
    this.rayToIR = new RayToIR(gl, sceneRaycast.rayBufferWidth, sceneRaycast.rayBufferHeight, opt.speedOfSound, opt.irLength);

    let aspect = canvas.clientWidth / canvas.clientHeight;
    let camera;
    if(opt.orthographic){
      let width = 3 * aspect;
      let height = 3;
      camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 0.1, 1000 );
    } else {
      camera = new THREE.PerspectiveCamera( 90, aspect, 0.1, 1000 );
    }
    scene.add(camera);
    this.camera = window.camera = camera;

    let mic;
    if(this.viewOnAudio){
      mic = camera;
    } else {
      mic = camera.clone();
      scene.add(mic);
    }
    this.mic = window.mic = mic;

    this.updateFromNumberBoxes();


    //Arrow
    let arrow = shapesLib.createArrow();
    mic.add(arrow);
    arrow = shapesLib.createArrow();
    arrow.rotation.set(0,0,Math.PI);
    mic.add(arrow);

    arrow = shapesLib.createArrow();
    arrow.rotation.set(0,0,0.5*Math.PI);
    mic.add(arrow);
    arrow = shapesLib.createArrow();
    arrow.rotation.set(0,0,-0.5*Math.PI);
    mic.add(arrow);

    this.input = new Input(canvas);
    this.controls = new Controls([camera, mic], canvas, this.input);
    this.controls.onChange = ()=>{
      this.updateNumberBoxes();
    }

    this.input.addAction('measure').onPress = ()=>{
      this.createMeasurement();
    };
    this.input.addKeybind(69,'measure');
    
    this.input.addAction('measureGrid').onPress = ()=>{
      this.measureGrid();
    };
    this.input.addKeybind(82,'measureGrid');
    
    this.debugInfo = new DebugInfo(this);

    this.ready = this.shapesLib.ready.then(()=>{
      this.isReady = true;
    });
    this.isReady = false;
  }
  start(){
    this.running = true;
    this.frame();
  }
  stop(){
    this.running = false;
  }

  createMeasurement(noPlane){
    let dataLeft = this.rayToIR.getData(0);
    let dataRight = this.rayToIR.getData(1);
    let sum = dataLeft.reduce((a,b)=>{return a+b;});
    sum = dataRight.reduce((a,b)=>{return a+b;}, sum);
    if(!noPlane){
      let plane = this.shapesLib.createPlane( 1. / this.measureResolution, 1. / this.measureResolution, {color: this.getHeatColor(sum/300), shadeless: true} );
      plane.position.copy(mic.position);
      plane.rotation.set(Math.PI/2,0,0);
      this.scene.add(plane);
    }
    return sum;
  }

  measureGrid(){
    let measureDist = 1./this.measureResolution;
    let output = [];
    let max = 0;
    for(let i = 0; i <= 4*this.measureResolution ;i++){
      let x = -2. + measureDist * i;
      if(!output[i]) output[i] = [];
      for(let j = 0; j <= 4*this.measureResolution ;j++){
        let z = -2. + measureDist * j;
        this.mic.position.set(x,0,z);
        this.sceneRaycast.calculateToCamera(this.mic);
        this.rayToIR.render(
          this.mic,
          this.sceneRaycast.toCameraBuffer.textures[0],
          this.sceneRaycast.toCameraBuffer.textures[1],
          this.sceneRaycast.toCameraBuffer.textures[2],
          this.sceneRaycast.toCameraBufferLength,
          this.sceneRaycast.toCameraBufferLength,
          true);
        output[i][j] = this.createMeasurement(false);
        if (max < output[i][j] ) max = output[i][j];
      }

    }
    console.log(output);
    console.log(max);
    for(let i = 0; i <= 4*this.measureResolution ;i++){
      let x = -2. + measureDist * i;
      for(let j = 0; j <= 4*this.measureResolution ;j++){
        let z = -2. + measureDist * j;
        output[i][j] = output[i][j] / max;
        let plane = this.shapesLib.createPlane( 1. / this.measureResolution, 1. / this.measureResolution, {color: this.getHeatColor(output[i][j]), shadeless: true} );
        plane.position.set(x,0,z);
        plane.rotation.set(Math.PI/2,0,0);
        this.scene.add(plane);
      }

    }
    console.log(output);
    
    return output;
  }

  getHeatColor(intensity){
    function clamp (value, min, max) {
      return Math.min(Math.max(value, min), max);
    };
    function smoothstep(edge0, edge1, x){
      let t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
      return t * t * (3.0 - 2.0 * t);
    }
    function fade(low, high, value){
      let mid = (low+high)*0.5;
      let range = (high-low)*0.5;
      let x = 1.0 - clamp(Math.abs(mid-value)/range, 0.0, 1.0);
      return smoothstep(0.0, 1.0, x);
    }
    let blue = new THREE.Color(0.0, 0.0, 1.0);
    blue = blue.multiplyScalar(fade(-0.25, 0.25, intensity));
    let cyan = new THREE.Color(0.0, 1.0, 1.0);
    cyan = cyan.multiplyScalar(fade(0.0, 0.5, intensity));
    let green = new THREE.Color(0.0, 1.0, 0.0);
    green = green.multiplyScalar(fade(0.25, 0.75, intensity));
    let yellow = new THREE.Color(1.0, 1.0, 0.0);
    yellow = yellow.multiplyScalar(fade(0.5, 1., intensity));
    let red = new THREE.Color(1.0, 0.0, 0.0);
    red = red.multiplyScalar(smoothstep(0.75, 1.0, intensity));
  
    let color = blue.add(cyan).add(green).add(yellow).add(red);
    // color = new THREE.Color(intensity, intensity, intensity);
    return color;
  }


  frame(time){
    if(!this.running) return;
    let delta = this.clock.getDelta();
    this.stats.update();
    this.debugInfo.frame(delta);

    this.controls.update(delta);

    this.renderer.render ( this.scene, this.camera, null, true );
    this.sceneRaycast.render(this.camera, this.mic);
    if(this.displayRays)
      this.sceneRaycast.display(this.camera);

    this.rayToIR.render(
      this.mic,
      this.sceneRaycast.toCameraBuffer.textures[0],
      this.sceneRaycast.toCameraBuffer.textures[1],
      this.sceneRaycast.toCameraBuffer.textures[2],
      this.sceneRaycast.toCameraBufferLength,
      this.sceneRaycast.toCameraBufferLength,
      true);

    if(window.convolution && !this.convRunning){
      this.calcConv(this.convBuffers);
    }
    requestAnimationFrame(this.frame.bind(this));
  }


  calcConv(){
    let dataLeft = this.rayToIR.getData(0);
    let dataRight = this.rayToIR.getData(1);
    window.convolution.fromData(dataLeft, dataRight);
    window.convolution.visualize(this.visualizer);
  }

  initGraphicShaders(){
    let gl = this.gl;

    return ;
  }

  updateNumberBoxes(){
    let camPos = this.camera.position;
    let camRot = this.camera.rotation;
    let micPos = this.mic.position;
    let micRot = this.mic.rotation;
    $(`#i_view_camera_pos_x`).val(camPos.x);
    $(`#i_view_camera_pos_y`).val(camPos.y);
    $(`#i_view_camera_pos_z`).val(camPos.z);

    $(`#i_view_camera_dir_x`).val(camRot.x/Math.PI * 180);
    $(`#i_view_camera_dir_y`).val(camRot.y/Math.PI * 180);
    
    $(`#i_view_mic_pos_x`).val(micPos.x);
    $(`#i_view_mic_pos_y`).val(micPos.y);
    $(`#i_view_mic_pos_z`).val(micPos.z);

    $(`#i_view_mic_dir_x`).val(micRot.x/Math.PI * 180);
    $(`#i_view_mic_dir_y`).val(micRot.y/Math.PI * 180);
  }

  updateFromNumberBoxes(){//i_view_camera_pos_x
    this.camera.position.set(
      $(`#i_view_camera_pos_x`).val()*1,
      $(`#i_view_camera_pos_y`).val()*1,
      $(`#i_view_camera_pos_z`).val()*1);

    this.camera.rotation.set(
      $(`#i_view_camera_dir_x`).val()/180 * Math.PI,
      $(`#i_view_camera_dir_y`).val()/180 * Math.PI,
      0);
    
    this.mic.position.set(
      $(`#i_view_mic_pos_x`).val()*1,
      $(`#i_view_mic_pos_y`).val()*1,
      $(`#i_view_mic_pos_z`).val()*1);

    this.mic.rotation.set(
      $(`#i_view_mic_dir_x`).val()/180 * Math.PI,
      $(`#i_view_mic_dir_y`).val()/180 * Math.PI,
      0);
  }

}
