class FFT {
  constructor(len) {
    this.len = len;
    this.cos = new Float64Array(Math.log2(len));
    this.sin = new Float64Array(Math.log2(len));
    this.initSinTable();
    this.perm = null;
    this.initPerm();
    // this.initWorker();
  }
  static create(len){
    return new FFT(len);
  }
  initSinTable(){
    for (var i = 0; i < this.cos.length; i++) {
      let width = 1 << i;
      this.cos[i] = Math.cos(Math.PI / width); // Math.exp(−2 * Math.Pi * i/m)
      this.sin[i] = 1 * Math.sin(Math.PI / width); // Math.exp(−2 * Math.Pi * i/m)
    }
  }
  initPerm(){
    let type = Uint8Array;
    if (this.len > 256)
      type = Uint16Array;
    if (this.len > 65536)
      type = Uint32Array;
    this.perm = new type(this.len);
    this.fillBitRev(this.perm, 0, this.len - 1);
  }
  initWorker(){
    this.worker = new Worker('audioProcessing/fft_worker.js');
  }
  fillBitRev(arr, start, end) {
    // debugger;
    if(start == end){
      arr[start] = 0;
      return;
    }
    let middle = (start + end - 1) / 2
    this.fillBitRev(arr, start, middle);
    this.fillBitRev(arr, middle + 1, end);
    for (let i = start; i <= middle; i++) {
      arr[i] <<= 1;
    }
    for (let i = middle + 1; i <= end; i++) {
      arr[i] <<= 1;
      arr[i] |= 1;
    }
  }
  applyPermutation(output, perm){
    for (var i = 0; i < perm.length; i++) {
      if(i < perm[i]){
        let tmp = output[i];
        output[i] = output[perm[i]];
        output[perm[i]] = tmp;
      }
    }
  }

  butterfly(Ar, Ai){
    let n = this.len;
    for (var b = 0; b < this.cos.length; b++) {
      let width = 1 << b;
      let wmR = this.cos[b]; // Math.exp(−2 * Math.Pi * i/m)
      let wmI = this.sin[b];
      // let wmR = Math.cos(Math.PI / width); // Math.exp(−2 * Math.Pi * i/m)
      // let wmI = (false ? -1 : 1) * Math.sin(Math.PI / width); // Math.exp(−2 * Math.Pi * i/m)
      for (var i = 0; i < n; i+=(width << 1)) {
        let wR = 1;
        let wI = 0;
        for (var j = 0; j < width; j++) {
          let l_index = i + j;
          let r_index = l_index + width;


          let left_r = Ar[l_index];
          let left_i = Ai[l_index];
          let right_r = wR * Ar[r_index] - wI * Ai[r_index];
          let right_i = wI * Ar[r_index] + wR * Ai[r_index];


          // Ar[l_index] = Math.SQRT1_2 * (left_r + right_r);
          // Ai[l_index] = Math.SQRT1_2 * (left_i + right_i);
          // Ar[r_index] = Math.SQRT1_2 * (left_r - right_r);
          // Ai[r_index] = Math.SQRT1_2 * (left_i - right_i);

          Ar[l_index] = (left_r + right_r);
          Ai[l_index] = (left_i + right_i);
          Ar[r_index] = (left_r - right_r);
          Ai[r_index] = (left_i - right_i);

          let oldwR = wR;
          wR = oldwR * wmR - wI * wmI;
          wI = oldwR * wmI + wI * wmR;
        }
      }
    }
    // return [Ar, Ai];
  }
  fft(Ar, Ai){
    this.applyPermutation(Ar, this.perm);
    this.applyPermutation(Ai, this.perm);
    this.butterfly(Ar, Ai);
  }
  normalize(output, factor)
  {
    if(!factor) factor = 1/this.len;
    for (var i = 0; i < output.length; i++) {
      output[i] *= factor;
    }
  }

  rfft(A, Ar, Ai){
    // let N = A.length>>1;
    // for (var i = 0; i < N ; i++) {
    //   Ar[i]=A[i << 1]
    //   Ai[i]=A[i << 1 | 1]
    // }
    Ar.set(A);
    Ai.fill(0);
    this.fft(Ar, Ai);
    for (var i = 0; i < N; i++) {
      Ar[N - i] =  Ar[i + 1];
    }
  }

  asyncfft(Ar, Ai, callback){
    this.fft(Ar, Ai);
    setTimeout(callback, 0);
  }
}

  function multiply(outputR, outputI, input1R, input1I, input2R, input2I){
    for (var i = 0; i < input1R.length; i++) {
      let oldR =  input1R[i];
      outputR[i] = input1R[i] * input2R[i]  - input1I[i] * input2I[i];
      outputI[i] = oldR * input2I[i]  + input1I[i] * input2R[i];
    }
  }

  function add(output, input){
    for (var i = 0; i < input.length; i++) {
      output[i] += input[i];
    }
  }
