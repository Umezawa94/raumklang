
onnodecreate = function(e) {
  var node = e.node;
  node.rtcs = [];
  for (let i = 0; i < e.outputs.length; i++) {
    node.rtcs[i] = new RealTimeConvolver(512, e.options.irLength);
  }
  

  // node.sendBufferL = new Float64Array(node.rtc.outputBuffer.length);
  // node.sendBufferR = new Float64Array(node.rtc.outputBuffer.length);

  // let kernel = new Float64Array(bufferLength * 2);
  // kernel[0] = 1;
  // kernel[bufferLength/2] = 0.5;
  // kernel[bufferLength-1] = 0.2;
  // console.log(kernel);
  // node.rtc.setIR(kernel);

  node.onmessage = function(e) {
    let msg = e.data;
    if(msg.ir){
      
      for (let i = 0; i < msg.ir.numberOfChannels; i++) {
        node.rtcs[i].setIR(msg.ir.getChannelData(i));
      }
    }
  };
};

onaudioprocess = function(e) {
  var node = e.node;
  if (!node.rtcs) return;

  // for (var channel = 0; channel < 1/*e.inputs[0].length*/; channel++) {
  //   var input = e.inputs[0][channel];
  //   var output = e.outputs[0][channel];
  //   var inputBuffer = node.inputBuffer[channel];
  //   var outputBuffer = node.outputBuffer[channel];
  //
  //   inputBuffer.set(input, node.inputIndex);
  //   if(!outputBuffer[0] == 0)
  //   output.set(outputBuffer.slice(node.inputIndex, node.inputIndex + output.length));
  // }
  // node.inputIndex += input.length;
  // if(node.inputIndex>=bufferLength){
  //   bufferedProcess(node.inputBuffer, node.outputBuffer, e);
  //   node.inputIndex = 0;
  //   // debugger;
  // }


  bufferedProcess(e.inputs[0], e.outputs[0], e);
}


function bufferedProcess(inputs, outputs, e) {
  var node = e.node;

  for (var channel = 0; channel < e.outputs[0].length; channel++) {
    var inputBuffer = inputs[channel % inputs.length];
    var outputBuffer = outputs[channel];
    let rtc = node.rtcs[channel];

    let out = rtc.process(inputBuffer);
    outputBuffer.set(out);

    // node.sendBufferL.set(outputBuffer, node.rtc.outputIndex);
    // node.sendBufferR.set(node.rtc.outputBuffer);


    // node.postMessage([node.sendBufferL, node.sendBufferR], [node.sendBufferL, node.sendBufferR]);
  }

  // if (node.buffers !== null) {
  //   node.bufferIndex += e.inputs[0][0].length;
  //   if (bufferLength <= node.bufferIndex) {
  //     node.postMessage(e, [ e ]);
  //     node.buffers = null;
  //   }
  // }
};
