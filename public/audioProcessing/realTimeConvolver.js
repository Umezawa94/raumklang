// $(document).ready(()=>{
//   window.rtc = new RealTimeConvolver(128, 128*14);
//   window.testBlock = new Float64Array(128);
//   for (var i = 0; i < 128; i++) {
//     testBlock[i] = Math.sin(2*Math.PI / 128 * i);
//   }
// });



class RealTimeConvolver {
  constructor(N, irLength) {
    console.assert(Math.log2(256)%1 == 0, "Block size needs to be power of 2");
    this.N = N;
    this.blocks = Math.ceil(irLength / this.N);
    this.irLength = this.blocks*N;
    this.ir = new Float64Array(this.blocks*N);
    this.bufferConvolver = [];
    this.outputBuffer = new CircularFloat64Array(this.blocks*N << 1);
    this.outputIndex = 0;
    this.buffers = [];
    this.ffts = [];
    this.bufferTargets = [];
    this.bufferTargets = [];
    this.initConvolvers();
  }
  setIR(ir){
    this.ir.set(ir);
    let blocks = this.blocks;
    for (let i = 0; i < this.bufferConvolver.length; i++) {
      let conv = this.bufferConvolver[i];
      let offset = conv.offset;
      let len = conv.length >> 1;
      conv.setIR(this.ir.slice(offset, offset + len));
    }

  }
  initConvolvers(){
    let irLength = this.irLength;
    let blocks = this.blocks;
    let offset = 0;
    for (var i = 0; blocks > 0; i++) {
      let len = this.getBlockLength(i);
      this.bufferConvolver[i] = (len*this.N) << 1;

      let bufferId = Math.log2(len);
      if(!this.buffers[bufferId]){
        let bufferLen = (len*this.N) << 1;
        this.buffers[bufferId] = {
          buffer: new Float64Array(bufferLen),
          bufferR: new Float64Array(bufferLen),
          bufferI: new Float64Array(bufferLen),
          fft: FFT.create(bufferLen),
          // ffthalf: new FFT(bufferLen>>1),
          targets: [],
          index: 0,
          length: len*this.N
        }
      }

      this.bufferConvolver[i] = new BufferConvolver((len*this.N) << 1, offset, this.buffers[bufferId].fft, this.outputBuffer);
      this.buffers[bufferId].targets.push(this.bufferConvolver[i]);
      offset += len*this.N;
      blocks -= len;
    }
  }
  getBlockLength(index){
    return 1<<(index>>1);
  }

  process(block){
    for (let i = 0; i < this.buffers.length; i++) {
      let buffer = this.buffers[i];
      // let index = bufferObj.index;
      // let buffer = bufferObj.buffer;
      buffer.buffer.set(block, buffer.index)
      buffer.index += this.N
      if(buffer.index >= buffer.length){
        buffer.index = 0;
        this.processBuffer(buffer);
        // let targets = buffer.targets;
        // for (let i = 0; i < targets.length; i++) {
        //   targets[i].process(buffer.buffer, this.outputIndex);
        // }
        // buffer.index = 0;
      }
    }
    // this.outputBuffer.set(block, this.outputIndex)
    let output = this.outputBuffer.slice(this.outputIndex, this.outputIndex + this.N);
    for (let i = this.outputIndex; i < this.outputIndex + this.N; i++) {
      this.outputBuffer[i] = 0;
    }
    this.outputIndex += this.N;
    if(this.outputIndex >= this.outputBuffer.length)
      this.outputIndex = 0;
    return output;

  }
  processBuffer(buffer){
      buffer.bufferR.set(buffer.buffer);
      buffer.bufferI.fill(0);
      if(false){
        buffer.fft.asyncfft(buffer.bufferR, buffer.bufferI, this.toConvolver.bind(this, buffer));
      } else {
        buffer.fft.fft(buffer.bufferR, buffer.bufferI);
        this.toConvolver(buffer);
      }
  }
  toConvolver(buffer){
    let targets = buffer.targets;
    for (let i = 0; i < targets.length; i++) {
      targets[i].process(buffer, this.outputIndex);
    }
  }
}

class CircularFloat64Array extends Float64Array {
  constructor(len) {
    super(len);
  }
  set(arr, offset){
    let splitIndex = this.length - offset;

    super.set(arr.slice(0,splitIndex), offset);
    super.set(arr.slice(splitIndex), 0);
  }
  add(arr, offset){
    for (var i = 0; i < arr.length; i++, offset++) {
      while(offset>=this.length) offset -= this.length;
      this[offset] += arr[i];
      // offset++;
    }

  }
}


class BufferConvolver {
  constructor(length, offset, fft, output) {
    this.length = length;
    this.workBufferR = new Float32Array(length);
    this.workBufferI = new Float32Array(length);
    this.irBuffer = new Float32Array(length>>1);
    this.irBufferR = new Float32Array(length);
    this.irBufferI = new Float32Array(length);
    this.offset = offset;
    this.fft = fft;
    this.output = output;
  }
  process(buffer, offset){
    // if(this.offset != 0 && this.offset != 128 && this.offset != 256 && this.offset != 512) return;
    // this.workBufferR.set(buffer.buffer);
    // this.workBufferI.fill(0);
    // this.fft.fft(this.workBufferR, this.workBufferI);

    multiply(this.workBufferR, this.workBufferI, buffer.bufferR, buffer.bufferI, this.irBufferR, this.irBufferI);
    this.fft.fft(this.workBufferI, this.workBufferR);

    this.fft.normalize(this.workBufferR);
    // this.fft.normalize(this.workBufferI);
    // // for (var i = 0; i < this.workBufferR.length>>1; i++) {
    // //   this.workBufferR[(this.workBufferR.length>>1) + i] = this.workBufferR[i];
    // // }
    this.output.add(this.workBufferR, this.offset + offset);

    // this.fft.asyncfft(this.workBufferI, this.workBufferR).then(()=>{
    //   this.fft.normalize(this.workBufferR);
    //   this.output.add(this.workBufferR, this.offset + offset);
    // });
  }
  setIR(buffer){
    this.irBuffer.set(buffer);
    this.irBufferR.fill(0);
    this.irBufferR.set(buffer);
    this.irBufferI.fill(0);
    this.fft.fft(this.irBufferR, this.irBufferI);
    // this.fft.normalize(this.irBufferR, 1/Math.sqrt(this.length));
    // this.fft.normalize(this.irBufferI, 1/Math.sqrt(this.length));
  }
}
