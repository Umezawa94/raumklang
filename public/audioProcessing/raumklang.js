
AudioWorkerShim.polyfill();

class RaumklangNode{
  constructor(ctx, silent, opt){
    this.ctx = ctx;
    // this.sampleRate = 44100;
    opt = opt || {};
    this.irLength = opt.irLength || 4096 * 2 * 4;
    this.bufferLength = opt.bufferLength || 1024;
    this.dspBufLength = opt.dspBufLength || 512;


    // this.frame1 = this.frameFromDistance(1);
    this.silent = silent;
    this.initBuffer();
    this.initWorker();
    this.impulseResponse;

    this.visualizer = null;
  }
  initWorker(){
    let ctx = this.ctx;

    let input = this.input = ctx.createGain();
    let output = this.output = ctx.createGain();
    if(this.silent) return;
    ctx.createAudioWorker("audioProcessing/raumklang_worker.js").then((factory) =>{
      this.factory = factory;
      let workerNode = this.workerNode = factory.createNode(1, 2, {bufferLength: this.bufferLength, dspBufLength: this.dspBufLength,  irLength: this.irLength});

      // workerNode.glitch.setValueAtTime(1,0);
      // workerNode.onmessage = console.log;

      workerNode.onmessage = (e) => {
        let [left, right] = e.data;
        // this.visualizer.visualize(left, right,'#00FF00');
        // left.reverse();right.reverse();
        // this.visualizer.visualize(left, right, 'cyan', true);
      }

      workerNode.connect(output);
      input.connect(workerNode);
    });
  }

  initBuffer(){
    let ctx = this.ctx;
    this.impulseResponse = ctx.createBuffer(2, this.irLength, this.ctx.sampleRate);
    let left = this.left = this.impulseResponse.getChannelData(0);
    let right = this.right = this.impulseResponse.getChannelData(1);
  }
  // frameFromDistance(distance){
  //   let speedOfSound = 340;
  //   let time = distance/speedOfSound;
  //   return Math.floor(time * this.sampleRate)
  // }
  // volFromDistance(distance){
  //   return 1/distance;
  // }
  // volFromFrame(frame){
  //   return 1/(frame/this.frame1);
  // }
  visualize(){
    if(this.visualizer === null) return;
    let left = this.impulseResponse.getChannelData(0);
    let right = this.impulseResponse.getChannelData(1);
    this.visualizer.visualize(left, right);
  }
  setIR(ir){
    // let left = this.impulseResponse.getChannelData(0);
    // let right = this.impulseResponse.getChannelData(1);
    if(!this.workerNode) return;
    this.workerNode.postMessage({ir}, [ir]);
  }
  // fromGraphics(data){
  //   let left = this.impulseResponse.getChannelData(0).fill(0);
  //   let right = this.impulseResponse.getChannelData(1).fill(0);
  //   // let max = 0, min = Infinity;
  //   let points = data.length * data[0].length/4;
  //   let normalize = 4000/points;
  //   for (let i = 0; i < data.length; i++) {
  //     let RGBA = data[i];
  //     for (let i = 0; i < RGBA.length; i+=4) {
  //       let distance = RGBA[i];
  //       let intensity = RGBA[i + 1];
  //       if (distance == 0) continue;

  //       let frame = this.frameFromDistance(distance);
  //       // max = frame > max ? frame : max;
  //       // min = frame < min ? frame : min;

  //       left[frame] += intensity * normalize;
  //       right[frame] += intensity * normalize;
  //     }
  //   }
  //   for (var i = 0; i < left.length; i++) {
  //     left[i] = 1 - 1 / (1 + left[i]);
  //     right[i] = 1 - 1 / (1 + right[i]);
  //   }

  //   // left.fill(0);
  //   // right.fill(0);
  //   //
  //   // left[0] = 1;


  //   // console.log(min, max);
  //   this.setIR(this.impulseResponse);
  // }

  fromData(dataLeft, dataRight){
      if(dataRight === undefined) dataRight = dataLeft;
      this.impulseResponse.getChannelData(0).set(dataLeft);
      this.impulseResponse.getChannelData(1).set(dataRight);
      this.setIR(this.impulseResponse);
  }
}
