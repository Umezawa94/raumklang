class Controls {
    constructor(objects, domElement, input){
        this.objects = objects;
        this._activeObject = 0;

        this.onChange = null;

        this.input = input || new Input(canvas);

        this.input.addAction('forward');
        this.input.addKeybind(87,'forward');
        this.input.addKeybind(38,'forward');
        this.input.addAction('backward');
        this.input.addKeybind(83,'backward');
        this.input.addKeybind(40,'backward');
        this.input.addAction('left');
        this.input.addKeybind(65,'left');
        this.input.addKeybind(37,'left');
        this.input.addAction('right');
        this.input.addKeybind(68,'right');
        this.input.addKeybind(39,'right');
        this.input.addAction('up');
        this.input.addKeybind(32,'up');
        this.input.addKeybind(33,'up');
        this.input.addAction('down');
        this.input.addKeybind(16,'down');
        this.input.addKeybind(34,'down');
    
        this.input.addAction('switchControl').onPress = ()=>{
            this._activeObject++;
            this._activeObject %= this.objects.length;
        };
        this.input.addKeybind(88,'switchControl');
          
        this.input.onDrag = (e)=>{    
            let cam = this.objects[this._activeObject];

            let rotX = - Math.PI*e.deltaX*0.002;
            cam.rotateOnWorldAxis( cam.up, rotX );
            let rotY = - Math.PI*e.deltaY*0.002;
            cam.rotateX(rotY);
            // if(cam.rotation.x > 0.5 * Math.PI)cam.rotation.x = 0.5 * Math.PI;
            // if(cam.rotation.x < -0.5 * Math.PI)cam.rotation.x = -0.5 * Math.PI;
            if(this.onChange) this.onChange();
        };
    }
    update(delta){
        let cam = this.objects[this._activeObject];    
    
        if(this.input.isActionDown('right')){
          let speed = delta*1;
          cam.translateX(speed);
          if(this.onChange) this.onChange();
        }
        if(this.input.isActionDown('left')){
          let speed = delta*1;
          cam.translateX(-speed);
          if(this.onChange) this.onChange();
        }
    
        if(this.input.isActionDown('forward')){
          let speed = delta*1;
          cam.translateZ(-speed);
          if(this.onChange) this.onChange();
          
        }
        if(this.input.isActionDown('backward')){
          let speed = delta*1;
          cam.translateZ(speed);
          if(this.onChange) this.onChange();
        }
        if(this.input.isActionDown('up')){
          let speed = delta*1;
          cam.translateY(speed);
          if(this.onChange) this.onChange();
        }
        if(this.input.isActionDown('down')){
          let speed = delta*1;
          cam.translateY(-speed);
          if(this.onChange) this.onChange();
        }
      }
}