
class ShapesLib{
    constructor(gl){
        this.gl = gl;
        this.soundProgram = new SoundProgram(gl);
        this.lineProgram = new LineProgram(gl);
        this.ready = Promise.all([this.soundProgram, this.lineProgram]).then(()=>{return this});
    }

    createPlane(w,h, opts){
        let {color, material, wireframe, shadeless, outlines} = opts || {};
        if(color === undefined) color = new THREE.Color(1,1,1);
        if(material === undefined) material = [1,1,1,1];
        let geo = new THREE.PlaneBufferGeometry( w,h )

        let arr = new Float32Array(geo.attributes.position.count * 4);
        let attribute = new THREE.BufferAttribute( arr, 4 );
        for (let i = 0; i < geo.attributes.position.count; i++) {
            attribute.setXYZW( i, color.r, color.g, color.b ,1);
        }
        geo.addAttribute( 'color', attribute );

        arr = new Float32Array(geo.attributes.position.count * 4);
        attribute = new THREE.BufferAttribute( arr, 4 );
        for (let i = 0; i < geo.attributes.position.count; i++) {
            attribute.setXYZW( i, material[0], material[1], material[2] ,material[3]);
        }
        geo.addAttribute( 'material', attribute );

        if(wireframe){
            let {color} = wireframe;
            if(color === undefined) color = new THREE.Color(0,0,0);
            let wire = new THREE.WireframeGeometry( geo );
            arr = new Float32Array(wire.attributes.position.count * 4);
            attribute = new THREE.BufferAttribute( arr, 4 );
            for (let i = 0; i < wire.attributes.position.count; i++) {
                attribute.setXYZW( i, color.r, color.g, color.b ,1);
            }
            wire.addAttribute( 'color', attribute );
            var wireLine = new THREE.LineSegments( wire, this.lineProgram);
        }

        if(outlines){
            let {color} = outlines;
            if(color === undefined) color = new THREE.Color(0,0,0);
            let outlineGeo = new THREE.BufferGeometry();
            let vertCount = geo.attributes.position.count;
            let pos = new Float32Array(geo.attributes.position.count * 3);
            let attribPos = new THREE.BufferAttribute( pos, 3 );
            let col = new Float32Array(geo.attributes.position.count * 4);
            let attribCol = new THREE.BufferAttribute( col, 4 );
            
            attribPos.setXYZ( 0, geo.attributes.position.getX(0), geo.attributes.position.getY(0), geo.attributes.position.getZ(0));
            attribPos.setXYZ( 1, geo.attributes.position.getX(1), geo.attributes.position.getY(1), geo.attributes.position.getZ(1));
            attribPos.setXYZ( 2, geo.attributes.position.getX(3), geo.attributes.position.getY(3), geo.attributes.position.getZ(3));
            attribPos.setXYZ( 3, geo.attributes.position.getX(2), geo.attributes.position.getY(2), geo.attributes.position.getZ(2));

            for (let i = 0; i < vertCount; i++) {
                attribCol.setXYZW( i, color.r, color.g, color.b ,1);
            }
            outlineGeo.addAttribute( 'position', attribPos );
            outlineGeo.addAttribute( 'color', attribCol );

            var outline = new THREE.LineLoop( outlineGeo, this.lineProgram);
        }

        let mat = shadeless ? this.lineProgram : this.soundProgram;
        let mesh = new THREE.Mesh(geo, mat );
        if(wireLine)
            mesh.add(wireLine);
        if(outline)
            mesh.add(outline);
        // mesh.name = "arrow";
        return mesh;
    }

    createShadelessPlane(w,h, color){
        if(color === undefined) color = new THREE.Color(1,1,1);
        let geo = new THREE.PlaneBufferGeometry( w,h )

        let arr = new Float32Array(geo.attributes.position.count * 4);
        let attribute = new THREE.BufferAttribute( arr, 4 );
        for (let i = 0; i < geo.attributes.position.count; i++) {
            attribute.setXYZW( i, color.r, color.g, color.b ,1);
        }
        geo.addAttribute( 'color', attribute );

        let mesh = new THREE.Mesh(geo, this.lineProgram );
        return mesh;
    }

    createArrow( color, material){
        if(color === undefined) color = new THREE.Color(0,1,0);
        let geo = new THREE.PlaneBufferGeometry( 1,1 )
        let arr = new Float32Array(geo.attributes.position.count * 4);
        geo.attributes.position.setXYZ(0, -0.01, 0.0, 0.0);
        geo.attributes.position.setXYZ(1, 0.0, 0.0, -0.1);
        geo.attributes.position.setXYZ(2, 0.0, 0.0, 0.01);
        geo.attributes.position.setXYZ(3, 0.01, 0.0, 0.0);

        let attribute = new THREE.BufferAttribute( arr, 4 );
        for (let i = 0; i < geo.attributes.position.count; i++) {
        attribute.setXYZW( i, color.r, color.g, color.b ,1);
        }
        geo.addAttribute( 'color', attribute );
        let mesh = new THREE.Mesh(geo, this.lineProgram );
        mesh.name = "arrow";
        return mesh;
    }
}