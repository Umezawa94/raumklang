class PostProcessor extends Scene{
  constructor(gl, program) {
    super(gl, program);
    this.canvas = new Plane(gl);
    // this.canvas.scale(1/4,1/3,1).translate(+4/4,0,0);
    this.addShape(this.canvas);
  }
  createBuffer(inputTexture){
    return new PostProcessorBuffer(this, inputTexture);
  }
}

class PostProcessorBuffer extends FrameBuffer{
  constructor(postProcessor, inputTexture){
    super(postProcessor.gl)
    this.postProcessor = postProcessor;
    this.inputTexture = inputTexture;
  }
  update(){
    this.postProcessor.canvas.setTexture(this.inputTexture);
    this.postProcessor.render(null, this);
  }
}
