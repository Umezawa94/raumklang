class FrameBuffer {
  constructor(gl, resX, resY , texCount) {
    this.gl = gl;

    resX = this.resolutionX = resX ? resX : 128;
    resY = this.resolutionY = resY ? resY : 128;
    texCount = this.texCount = texCount ? texCount : 1;

    let attachments = [gl.COLOR_ATTACHMENT0];
    if (texCount > 1){
      // if (!gl.WEBGL_draw_buffers){
      //   gl.WEBGL_draw_buffers = gl.getExtension('WEBGL_draw_buffers');
      // }
      // let ext = gl.WEBGL_draw_buffers;
      attachments = [gl.COLOR_ATTACHMENT0
        , gl.COLOR_ATTACHMENT1
        , gl.COLOR_ATTACHMENT2
        , gl.COLOR_ATTACHMENT3
        , gl.COLOR_ATTACHMENT4
        , gl.COLOR_ATTACHMENT5
        , gl.COLOR_ATTACHMENT6
        , gl.COLOR_ATTACHMENT7
        , gl.COLOR_ATTACHMENT8
        , gl.COLOR_ATTACHMENT9];
    }

    const fb = this.fb = gl.createFramebuffer();

    gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

    this._textures = [];
    for (var i = 0; i < texCount; i++) {
      let texture = this._textures[i] = gl.createTexture();
      // gl.activeTexture(gl.TEXTURE0)
      gl.bindTexture(gl.TEXTURE_2D, texture);
      // gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1)
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA16F, resX, resY, 0,
       gl.RGBA, gl.HALF_FLOAT, null)

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);


      gl.framebufferTexture2D(gl.FRAMEBUFFER, attachments[i],
        gl.TEXTURE_2D, texture, 0);
    }
    gl.drawBuffers(attachments.slice(0,texCount));

    this._textureReaders = [];
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);


    // var renderbuffer = gl.createRenderbuffer();
    // gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
    // gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, resX, resY);
    // gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);

    this._data = new Float32Array(resX * resY * 4);
  }
  get texture (){
    return this._textures[0];
  }
  get textures (){
    return this._textures;
  }

  bind(){
    const gl = this.gl;
    gl.viewport(0,0,this.resolutionX,this.resolutionY);
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
  }
  getData(ind){
    if(ind !== undefined){
      return this.getTextureReader(ind).getData();
    }
    let gl = this.gl;
    let resX = this.resolutionX;
    let resY = this.resolutionY;
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
    gl.readPixels(0,0,resX,resY,gl.RGBA,gl.FLOAT,this._data);
    return this._data;
  }

  getTextureReader(ind){
    if(this._textureReaders[ind])
    {
      return this._textureReaders[ind];
    }
    this._textureReaders[ind] = new TextureReader(gl, this.textures[ind], this.resolutionX, this.resolutionY);
    return this._textureReaders[ind];
  }
}
