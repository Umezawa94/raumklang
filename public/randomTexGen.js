class RandomTexGen extends FrameBuffer{
    constructor(gl){
      super(gl);

      this.program = RandomProgram
      this.program.ready.then(()=>{
        this.isReady = true;
        // this.calculateToCamera();
      });

    }

    createUVBuffer(){
        const gl = this.gl;
        {
          let arr = new Float32Array([
            -1,-1,
            -1,+1,
            +1,-1,
            +1,+1
          ]);
          this.uvBuffer = gl.createBuffer()
          gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
          gl.bufferData(gl.ARRAY_BUFFER, arr, gl.STATIC_DRAW);
        }
    }

    update(){
        if(!this.isReady) return;
        
            const gl = this.gl;
            const programInfo = this.program;
        
            // if(frameBuffer){
        
            let bufferFrom = this.rayBuffers[this.activeRayBuffer];
            let bufferTo = this.rayBuffers[this.activeRayBuffer == 1 ? 0  : 1];
            bufferTo.bind();
            // } else {
              // gl.bindFramebuffer(gl.FRAMEBUFFER, null);
              // gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
            // }
        
        
            let clear = true;
            if (clear){
              gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
              gl.clearDepth(1.0);                 // Clear everything
              gl.enable(gl.DEPTH_TEST);           // Enable depth testing
              gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
        
              // Clear the canvas before we start drawing on it.
        
              gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            }
        
            gl.disable(gl.BLEND);
            // gl.blendFunc(gl.ONE, gl.ZERO);
            // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
        
            // gl.enable(gl.CULL_FACE);
        
            // Tell WebGL to use our program when drawing
            gl.useProgram(programInfo.program);
        
            let newRays = this.rayCount - this.handledRayCount;
            this.handledRayCount = this.rayCount;
            this.rayCount = this.rayCount + newRays;
            if (this.rayCount > this.rayBufferWidth * this.rayBufferHeight){
              this.rayCount = this.rayBufferWidth * this.rayBufferHeight;
              if(this.rayCount == this.handledRayCount) return;
            };
        
        
            gl.uniform1ui(programInfo.uniformLocations.rayWidth, this.rayBufferWidth);
            gl.uniform1ui(programInfo.uniformLocations.rayHeight, this.rayBufferHeight);
            gl.uniform1ui(programInfo.uniformLocations.rayCount, this.rayCount);
            gl.uniform1ui(programInfo.uniformLocations.handledRayCount, this.handledRayCount);
        
            gl.uniform1ui(programInfo.uniformLocations.vertWidth, this.vertWidth);
            gl.uniform1ui(programInfo.uniformLocations.vertHeight, this.vertHeight);
            gl.uniform1ui(programInfo.uniformLocations.vertCount, this.vertexCount);
        
        
        
        
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[0]);
            gl.uniform1i(programInfo.uniformLocations.texPos, 0);
        
            gl.activeTexture(gl.TEXTURE1);
            gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[1]);
            gl.uniform1i(programInfo.uniformLocations.texDir, 1);
        
            gl.activeTexture(gl.TEXTURE2);
            gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[2]);
            gl.uniform1i(programInfo.uniformLocations.texData, 2);
        
            gl.activeTexture(gl.TEXTURE3);
            gl.bindTexture(gl.TEXTURE_2D, this.texVert);
            gl.uniform1i(programInfo.uniformLocations.texVert, 3);
        
            gl.activeTexture(gl.TEXTURE4);
            gl.bindTexture(gl.TEXTURE_2D, this.texVertNorm);
            gl.uniform1i(programInfo.uniformLocations.texVertNorm, 4);
        
        
            {
              gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBufferProp);
              // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        
             // Tell WebGL how to pull out the positions from the position
             // buffer into the vertexPosition attribute.
        
               const numComponents = 2;  // pull out 2 values per iteration
               const type = gl.UNSIGNED_SHORT;    // the data in the buffer is 32bit floats
               const stride = 0;         // how many bytes to get from one set of values to the next
                                         // 0 = use type and numComponents above
               const offset = 0;         // how many bytes inside the buffer to start from
              //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
               gl.vertexAttribIPointer(
                   programInfo.attribLocations.uv,
                   numComponents,
                   type,
                   stride,
                   offset);
               gl.enableVertexAttribArray(
                   programInfo.attribLocations.uv);
             }
             // this.rayCount <<=1;
        
             // Set the shader uniforms
             const offset = 0;
             // const vertexCount = this.inputResX * this.inputResY;
             // const vertexCount = this.rayCount;
             const vertexCount = this.rayCount;
            //  gl.drawArrays(this.drawMode, offset, vertexCount);
             gl.drawArrays(gl.POINTS, offset, vertexCount);
        
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            this.activeRayBuffer = this.activeRayBuffer == 1 ? 0  : 1;
        
      this.postProcessor.render(null, this);
    }
  }