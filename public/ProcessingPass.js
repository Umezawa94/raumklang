class ProcessingPass {
  constructor(gl, options, fragmShader) {
    options = this.options = options || {};
    this.gl = gl;
    this.shapes = [];
    this.sounds = [];
    this.cameras = [];
    // this.programInfo = programInfo;

    this.rayBufferWidth = 128;
    this.rayBufferHeight = 128;
    this.rayCount = 3;
    this.rayBuffers = [new FrameBuffer(gl, this.rayBufferWidth, this.rayBufferHeight, 3)
      , new FrameBuffer(gl, this.rayBufferWidth, this.rayBufferHeight, 3)];
    this.activeRayBuffer = 0;

    this.createUVBuffer();
    this.initRayBuffer();

    this.programProp = new RayPropProgram(gl);
    this.programDisplay = new RayDisplayProgram(gl);


    Promise.all([this.programProp.ready, this.programDisplay.ready]).then(()=>{
      this.isReady = true;
      this.propagate();
    });

    this.isReady = false;
  }
}
