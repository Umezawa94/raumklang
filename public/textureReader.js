class TextureReader {
  constructor(gl, texture, resX, resY) {
    this.gl = gl;

    this.texture = texture;
    this.resolutionX = resX;
    this.resolutionY = resY;


    let attachments = [gl.COLOR_ATTACHMENT0];

    const fb = this.fb = gl.createFramebuffer();

    gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

    gl.bindTexture(gl.TEXTURE_2D, texture);


    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
      gl.TEXTURE_2D, texture, 0);


    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);


    // var renderbuffer = gl.createRenderbuffer();
    // gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
    // gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, resX, resY);
    // gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);

    this._data = new Float32Array(resX * resY * 4);
  }

  getData(){
    let gl = this.gl;
    let resX = this.resolutionX;
    let resY = this.resolutionY;
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
    gl.readPixels(0,0,resX,resY,gl.RGBA,gl.FLOAT,this._data);
    return this._data;
  }
}
