class SceneRaycast {
  constructor(gl, opt) {
    this.gl = gl;

    opt = opt || {};
    this.initRayCount = opt.rayCount || 400000;
    this.bounceCount = opt.bounceCount || 10;
    this.speedOfSound = opt.speedOfSound || 340;
    this.rayBufferWidth = opt.rayBufferWidth || 1024*2;
    this.rayBufferHeight = opt.rayBufferHeight || 1024*2;
    this.rayMultiplier = opt.rayMultiplier || 2;



    this.scene = [];
    this.shapes = [];
    this.sounds = [];
    this.cameras = [];
    // this.programInfo = programInfo;

    this.handledRayCount = 0;
    this.newRayCount = 0;
    
    this.rayBuffers = [new FrameBuffer(gl, this.rayBufferWidth, this.rayBufferHeight, 5)
      , new FrameBuffer(gl, this.rayBufferWidth, this.rayBufferHeight, 5)];
    this.activeRayBuffer = 0;

    this.toCameraBuffer = new FrameBuffer(gl, this.rayBufferWidth, this.rayBufferHeight, 5);
    this.toCameraBufferLength = 0;

    this.createUVBuffer();
    // this.initRayBuffer();

    this.programProp = new RayPropProgram(gl);
    this.programDisplay = new RayDisplayProgram(gl);
    this.programToCamera = new RayToCameraProgram(gl);
    this.programRender = new RayRenderProgram(gl);


    this.vertWidth = this.vertHeight = 128;
    this.texVert = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texVert);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    this.texVertNorm = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texVertNorm);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    this.texVertMat = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texVertMat);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    this.texVertInd = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texVertInd);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    this.vertexCount = 0;

    this.projScreenMatrix = new THREE.Matrix4();
    // this.refreshVertexBuffer();

    gl.WEBGL_draw_buffers = gl.getExtension('WEBGL_draw_buffers');

    this.isReady = false;
  }
  init(){
    this.isReady = false;
    this.refreshVertexBuffer();
    this.initRayBuffer();
    Promise.all([this.programProp.ready, this.programDisplay.ready, this.programToCamera.ready, this.programRender.ready]).then(()=>{
      this.isReady = true;
      for (var i = 0; i < this.bounceCount; i++) {
        this.propagate();
      }
      // this.calculateToCamera();
    });
  }
  createUVBuffer(){
    const gl = this.gl;
    {
      let arr = new Float32Array(this.rayBufferWidth * this.rayBufferHeight * 2 * 3);
      for (var x = 0; x < this.rayBufferWidth; x++) {
        for (var y = 0; y < this.rayBufferHeight; y++) {
          for (var i = 0; i < 2; i++) {
            let ind = (i + (x + y * this.rayBufferWidth) * 2) * 3 ;
            arr[ind] = x / (this.rayBufferWidth);
            arr[ind + 1] = y / (this.rayBufferHeight);
            arr[ind + 2] = i;
          }
        }
      }
      this.uvBuffer = gl.createBuffer()
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, arr, gl.STATIC_DRAW);
    }

    {
      let arr = new Uint16Array(this.rayBufferWidth * this.rayBufferHeight * 2);
      for (var x = 0; x < this.rayBufferWidth; x++) {
        for (var y = 0; y < this.rayBufferHeight; y++) {
          let ind = (x + y * this.rayBufferWidth) * 2 ;
          arr[ind] = x;// / (this.rayBufferWidth);
          arr[ind + 1] = y;// / (this.rayBufferHeight);
        }
      }
      this.uvBufferProp = gl.createBuffer()
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBufferProp);
      gl.bufferData(gl.ARRAY_BUFFER, arr, gl.STATIC_DRAW);
      console.log(arr);
    }
    

  }
  initRayBuffer(){
    const gl = this.gl;
    {
      let pos = new Float32Array(this.rayBufferWidth * this.rayBufferWidth * 4);
      let dir = new Float32Array(this.rayBufferWidth * this.rayBufferWidth * 4);
      let data = new Float32Array(this.rayBufferWidth * this.rayBufferWidth * 4);
      let normal = new Float32Array(this.rayBufferWidth * this.rayBufferWidth * 4);
      let mat = new Float32Array(this.rayBufferWidth * this.rayBufferWidth * 4);

      this.newRayCount = this.handledRayCount = this.initRayCount;
      for (var i = 0; i < this.newRayCount; i++) {
        let index = i << 2;
        let source = this.sounds[i%this.sounds.length];

        pos.set(source.position.toArray(), index);
        dir[index+0] = Math.tan(Math.random() * 2 - 1);
        dir[index+1] = Math.tan(Math.random() * 2 - 1);
        dir[index+2] = Math.tan(Math.random() * 2 - 1);
        data[index+1] = 0;
        normal[index+0] = dir[index+0];
        normal[index+1] = dir[index+1];
        normal[index+2] = dir[index+2]
      }

      gl.bindTexture(gl.TEXTURE_2D, this.rayBuffers[0].textures[0]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.rayBufferWidth, this.rayBufferHeight, 0,
        gl.RGBA, gl.FLOAT, pos);

      gl.bindTexture(gl.TEXTURE_2D, this.rayBuffers[0].textures[1]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.rayBufferWidth, this.rayBufferHeight, 0,
        gl.RGBA, gl.FLOAT, dir);


      gl.bindTexture(gl.TEXTURE_2D, this.rayBuffers[0].textures[2]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.rayBufferWidth, this.rayBufferHeight, 0,
        gl.RGBA, gl.FLOAT, data);

      gl.bindTexture(gl.TEXTURE_2D, this.rayBuffers[0].textures[3]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.rayBufferWidth, this.rayBufferHeight, 0,
        gl.RGBA, gl.FLOAT, normal);

      gl.bindTexture(gl.TEXTURE_2D, this.rayBuffers[0].textures[4]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.rayBufferWidth, this.rayBufferHeight, 0,
        gl.RGBA, gl.FLOAT, mat);
    }
  }
  refreshVertexBuffer(){
    const gl = this.gl;
    console.log(this.shapes);

    let pos = new THREE.BufferAttribute (new Float32Array(this.vertWidth * this.vertHeight * 3), 3);
    let norm = new THREE.BufferAttribute (new Float32Array(this.vertWidth * this.vertHeight * 3), 3);
    let material = new THREE.BufferAttribute (new Float32Array(this.vertWidth * this.vertHeight * 4), 4);
    let index = new THREE.BufferAttribute (new Uint32Array(this.vertWidth * this.vertHeight * 1), 1);

    this.vertexCount = 0;
    let v0Pos = vec4.create();
    let v0Norm = vec4.create();
    let v1Pos = vec4.create();
    let v1Norm = vec4.create();
    let v2Pos = vec4.create();
    let v2Norm = vec4.create();

    let posIndex = 0;
    let indIndex = 0;
    for (let i = 0; i < this.shapes.length; i++) {
      let shape = this.shapes[i];
      let bakedGeometry = shape.bakedGeometry;
      if(!bakedGeometry){
        shape.updateMatrixWorld();
        bakedGeometry = shape.bakedGeometry = shape.geometry.clone();
        bakedGeometry.applyMatrix(shape.matrixWorld);
        bakedGeometry.setIndex(new THREE.BufferAttribute( new Float32Array((bakedGeometry.attributes.position.count - 2)*3), 1 ) );
        for (let i = 0; i < bakedGeometry.attributes.position.count - 2; i++){
          bakedGeometry.index.setXYZ(i * 3, i, i+1, i+2);
        }
      }
      let attr = bakedGeometry.attributes;
      pos.array.set(attr.position.array, posIndex * pos.itemSize);
      norm.array.set(attr.normal.array, posIndex * norm.itemSize);
      material.array.set(attr.material.array, posIndex * material.itemSize);

      for (let i = 0; i < bakedGeometry.index.array.length; i++) {
        index.array[indIndex] = bakedGeometry.index.array[i] + posIndex;
        indIndex++;
      }

      posIndex += attr.position.count;
      // let matrix = shape.modelViewMatrix;
      // let rotateMatrix = shape.rotateMatrix;
      // let vertices = shape.vertices;
      // for (let i = 0; i < bakedGeometry.position.count - 2; i++) {
      //   let v0 = vertices[i];
      //   let v1 = vertices[i+1];
      //   let v2 = vertices[i+2];

      //   let index = this.vertexCount * 4;
      //   pos.set(v0Pos, index);
      //   pos.set(v1Pos, index + 4);
      //   pos.set(v2Pos, index + 8);

      //   norm.set(v0Norm, index);
      //   norm.set(v1Norm, index + 4);
      //   norm.set(v2Norm, index + 8);

      //   material.set(v0.material, index);
      //   material.set(v1.material, index + 4);
      //   material.set(v2.material, index + 8);

      //   this.vertexCount += 3;
      // }
    }
    this.vertexCount = indIndex;

    gl.bindTexture(gl.TEXTURE_2D, this.texVert);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB32F, this.vertWidth, this.vertHeight, 0,
      gl.RGB, gl.FLOAT, pos.array);

    gl.bindTexture(gl.TEXTURE_2D, this.texVertNorm);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB32F, this.vertWidth, this.vertHeight, 0,
      gl.RGB, gl.FLOAT, norm.array);

    gl.bindTexture(gl.TEXTURE_2D, this.texVertMat);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, this.vertWidth, this.vertHeight, 0,
      gl.RGBA, gl.FLOAT, material.array);
      
    gl.bindTexture(gl.TEXTURE_2D, this.texVertInd);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.R32UI, this.vertWidth, this.vertHeight, 0,
      gl.RED_INTEGER, gl.UNSIGNED_INT, index.array);

  }

  propagate(){
    if(!this.isReady) return;

    const gl = this.gl;
    const programInfo = this.programProp;

    // if(frameBuffer){

    let bufferFrom = this.rayBuffers[this.activeRayBuffer];
    let bufferTo = this.rayBuffers[this.activeRayBuffer == 1 ? 0  : 1];
    bufferTo.bind();
    // } else {
      // gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      // gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
    // }


    let clear = true;
    if (clear){
      gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
      gl.clearDepth(1.0);                 // Clear everything
      gl.enable(gl.DEPTH_TEST);           // Enable depth testing
      gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

      // Clear the canvas before we start drawing on it.

      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }

    gl.disable(gl.BLEND);
    // gl.blendFunc(gl.ONE, gl.ZERO);
    // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

    // gl.enable(gl.CULL_FACE);

    // Tell WebGL to use our program when drawing
    gl.useProgram(programInfo.program);

    let newRays = this.newRayCount * this.rayMultiplier;

    if (this.handledRayCount + newRays > this.rayBufferWidth * this.rayBufferHeight){
      newRays = this.rayBufferWidth * this.rayBufferHeight - this.handledRayCount;
      this.newRayCount = 0;
      if(newRays <= 0){
        this.handledRayCount = this.rayBufferWidth * this.rayBufferHeight;
        return;
      } 
    };

    gl.uniform1ui(programInfo.uniformLocations.rayWidth, this.rayBufferWidth);
    gl.uniform1ui(programInfo.uniformLocations.rayHeight, this.rayBufferHeight);
    gl.uniform1ui(programInfo.uniformLocations.newRayCount,  this.newRayCount);
    gl.uniform1ui(programInfo.uniformLocations.handledRayCount, this.handledRayCount);
    gl.uniform1ui(programInfo.uniformLocations.rayMultiplier, this.rayMultiplier);

    gl.uniform1ui(programInfo.uniformLocations.vertWidth, this.vertWidth);
    gl.uniform1ui(programInfo.uniformLocations.vertHeight, this.vertHeight);
    gl.uniform1ui(programInfo.uniformLocations.vertCount, this.vertexCount);




    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[0]);
    gl.uniform1i(programInfo.uniformLocations.texPos, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[1]);
    gl.uniform1i(programInfo.uniformLocations.texDir, 1);

    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[2]);
    gl.uniform1i(programInfo.uniformLocations.texData, 2);

    gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[3]);
    gl.uniform1i(programInfo.uniformLocations.texNorm, 3);

    gl.activeTexture(gl.TEXTURE4);
    gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[4]);
    gl.uniform1i(programInfo.uniformLocations.texMat, 4);

    gl.activeTexture(gl.TEXTURE5);
    gl.bindTexture(gl.TEXTURE_2D, this.texVert);
    gl.uniform1i(programInfo.uniformLocations.texVert, 5);

    gl.activeTexture(gl.TEXTURE6);
    gl.bindTexture(gl.TEXTURE_2D, this.texVertNorm);
    gl.uniform1i(programInfo.uniformLocations.texVertNorm, 6);

    gl.activeTexture(gl.TEXTURE7);
    gl.bindTexture(gl.TEXTURE_2D, this.texVertMat);
    gl.uniform1i(programInfo.uniformLocations.texVertMat, 7);

    gl.activeTexture(gl.TEXTURE8);
    gl.bindTexture(gl.TEXTURE_2D, this.texVertInd);
    gl.uniform1i(programInfo.uniformLocations.texVertInd, 8);


    {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBufferProp);
      // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

     // Tell WebGL how to pull out the positions from the position
     // buffer into the vertexPosition attribute.

       const numComponents = 2;  // pull out 2 values per iteration
       const type = gl.UNSIGNED_SHORT;    // the data in the buffer is 32bit floats
       const stride = 0;         // how many bytes to get from one set of values to the next
                                 // 0 = use type and numComponents above
       const offset = 0;         // how many bytes inside the buffer to start from
      //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
       gl.vertexAttribIPointer(
           programInfo.attribLocations.uv,
           numComponents,
           type,
           stride,
           offset);
       gl.enableVertexAttribArray(
           programInfo.attribLocations.uv);
     }
     // this.rayCount <<=1;

     // Set the shader uniforms
     const offset = 0;
     // const vertexCount = this.inputResX * this.inputResY;
     // const vertexCount = this.rayCount;
     const vertexCount = this.handledRayCount + newRays;
    //  gl.drawArrays(this.drawMode, offset, vertexCount);
     gl.drawArrays(gl.POINTS, offset, vertexCount);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    this.activeRayBuffer = this.activeRayBuffer == 1 ? 0  : 1;
    
    this.handledRayCount += newRays;
    this.newRayCount = newRays;
    console.log(newRays, this.handledRayCount, this.newRayCount, this.rayMultiplier, vertexCount);
  }
  calculateToCamera(camera){
      if(!this.isReady) return;

      const gl = this.gl;
      const programInfo = this.programToCamera;

      // if(frameBuffer){
      this.toCameraBufferLength = this.handledRayCount + this.newRayCount;

      let bufferFrom = this.rayBuffers[this.activeRayBuffer];
      let bufferTo = this.toCameraBuffer;
      bufferTo.bind();
      // } else {
        // gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        // gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
      // }


      let clear = true;
      if (clear){
        gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
        gl.clearDepth(1.0);                 // Clear everything
        gl.enable(gl.DEPTH_TEST);           // Enable depth testing
        gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

        // Clear the canvas before we start drawing on it.

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
      }

      gl.disable(gl.BLEND);
      // gl.blendFunc(gl.ONE, gl.ZERO);
      // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

      // gl.enable(gl.CULL_FACE);

      // Tell WebGL to use our program when drawing
      gl.useProgram(programInfo.program);



      // gl.uniform1i(programInfo.uniformLocations.rayWidth, this.rayBufferWidth);
      // gl.uniform1i(programInfo.uniformLocations.rayHeight, this.rayBufferHeight);
      gl.uniform1i(programInfo.uniformLocations.rayCount, this.toCameraBufferLength);

      // gl.uniform1i(programInfo.uniformLocations.vertWidth, this.vertWidth);
      // gl.uniform1i(programInfo.uniformLocations.vertHeight, this.vertHeight);
      gl.uniform1ui(programInfo.uniformLocations.vertCount, this.vertexCount);


      // console.log(, this.test);
      // this.test.set(camera.pos);
      // vec3.inverse(this.test, camera.pos);
      // this.test[0] = camera.pos[0];
      // this.test[1] = camera.pos[1];
      // this.test[2] = camera.pos[2];
      gl.uniform4f(programInfo.uniformLocations.cameraPosition, camera.position.x, camera.position.y, camera.position.z, 1);



      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[0]);
      gl.uniform1i(programInfo.uniformLocations.texPos, 0);
  
      gl.activeTexture(gl.TEXTURE1);
      gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[1]);
      gl.uniform1i(programInfo.uniformLocations.texDir, 1);
  
      gl.activeTexture(gl.TEXTURE2);
      gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[2]);
      gl.uniform1i(programInfo.uniformLocations.texData, 2);
  
      gl.activeTexture(gl.TEXTURE3);
      gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[3]);
      gl.uniform1i(programInfo.uniformLocations.texNorm, 3);
  
      gl.activeTexture(gl.TEXTURE4);
      gl.bindTexture(gl.TEXTURE_2D, bufferFrom.textures[4]);
      gl.uniform1i(programInfo.uniformLocations.texMat, 4);
  
      gl.activeTexture(gl.TEXTURE5);
      gl.bindTexture(gl.TEXTURE_2D, this.texVert);
      gl.uniform1i(programInfo.uniformLocations.texVert, 5);
  
      gl.activeTexture(gl.TEXTURE6);
      gl.bindTexture(gl.TEXTURE_2D, this.texVertNorm);
      gl.uniform1i(programInfo.uniformLocations.texVertNorm, 6);
  
      gl.activeTexture(gl.TEXTURE7);
      gl.bindTexture(gl.TEXTURE_2D, this.texVertMat);
      gl.uniform1i(programInfo.uniformLocations.texVertMat, 7);

      gl.activeTexture(gl.TEXTURE8);
      gl.bindTexture(gl.TEXTURE_2D, this.texVertInd);
      gl.uniform1i(programInfo.uniformLocations.texVertInd, 8);


      {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBufferProp);
        // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

       // Tell WebGL how to pull out the positions from the position
       // buffer into the vertexPosition attribute.

         const numComponents = 2;  // pull out 2 values per iteration
         const type = gl.UNSIGNED_SHORT;    // the data in the buffer is 32bit floats
         const stride = 0;         // how many bytes to get from one set of values to the next
                                   // 0 = use type and numComponents above
         const offset = 0;         // how many bytes inside the buffer to start from
        //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
         gl.vertexAttribIPointer(
             programInfo.attribLocations.uv,
             numComponents,
             type,
             stride,
             offset);
         gl.enableVertexAttribArray(
             programInfo.attribLocations.uv);
       }

       // Set the shader uniforms
       const offset = 0;
       // const vertexCount = this.inputResX * this.inputResY;
       // const vertexCount = this.rayCount;
       const vertexCount = this.toCameraBufferLength;
      //  gl.drawArrays(this.drawMode, offset, vertexCount);
       gl.drawArrays(gl.POINTS, offset, vertexCount);

      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  }
  render(camera, mic, clear){
    this.calculateToCamera(mic);
    // return;
    if(!this.isReady) return;

    const gl = this.gl;
    const programInfo = this.programRender;

    // if(frameBuffer){
    //   frameBuffer.bind();
    // } else {
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
    // }


    // clear = false;
    // if (clear){
    //   gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
    //   gl.clearDepth(1.0);                 // Clear everything
    //   gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    //   // Clear the canvas before we start drawing on it.

    //   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    // }

    gl.disable(gl.DEPTH_TEST);
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE);
    // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

    // gl.enable(gl.CULL_FACE);

    // Tell WebGL to use our program when drawing
    gl.useProgram(programInfo.program);


    
    if ( camera.parent === null ) camera.updateMatrixWorld();
    this.projScreenMatrix.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );
    if(programInfo.uniformLocations.projectionMatrix !== undefined){
      if (camera){
        gl.uniformMatrix4fv(
          programInfo.uniformLocations.projectionMatrix,
          false,
          this.projScreenMatrix.elements);
        let campos = camera.position;
        gl.uniform4f(
            programInfo.uniformLocations.cameraPosition, campos.x, campos.y, campos.z, 1);
      }
    }
    // gl.uniformMatrix4fv(
    //   programInfo.uniformLocations.normalMatrix,
    //   false,
    //   camera.parent.rotateMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.toCameraBuffer.textures[0]);
    gl.uniform1i(programInfo.uniformLocations.texPos, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, this.toCameraBuffer.textures[1]);
    gl.uniform1i(programInfo.uniformLocations.texDir, 1);

    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, this.toCameraBuffer.textures[2]);
    gl.uniform1i(programInfo.uniformLocations.texData, 2);


    // gl.uniform4f(programInfo.uniformLocations.color, r,g,b,a);

   {
     gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBufferProp);
     // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute.

      const numComponents = 2;  // pull out 2 values per iteration
      const type = gl.UNSIGNED_SHORT;    // the data in the buffer is 32bit floats
      const stride = 0;         // how many bytes to get from one set of values to the next
                                // 0 = use type and numComponents above
      const offset = 0;         // how many bytes inside the buffer to start from
     //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
      gl.vertexAttribIPointer(
          programInfo.attribLocations.uv,
          numComponents,
          type,
          stride,
          offset);
      gl.enableVertexAttribArray(
          programInfo.attribLocations.uv);
    }

    // Set the shader uniforms
    const offset = 0;
    // const vertexCount = this.inputResX * this.inputResY;
    // const vertexCount = this.rayCount;
    const vertexCount = this.toCameraBufferLength;
   //  gl.drawArrays(this.drawMode, offset, vertexCount);
    gl.drawArrays(gl.POINTS, offset, vertexCount);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  }

  display(camera, frameBuffer, clear){
    // return;
    let sourceBuffer = this.rayBuffers[this.activeRayBuffer];
    this.displayRays(camera, frameBuffer, clear, sourceBuffer.textures[0], sourceBuffer.textures[1], sourceBuffer.textures[2], this.handledRayCount, 1, 1, 0, 1);
    // sourceBuffer = this.toCameraBuffer;
    // this.displayRays(camera, frameBuffer, false, sourceBuffer.textures[0], sourceBuffer.textures[1], sourceBuffer.textures[2], this.toCameraBufferLength, 0, 1, 1, 1);
  }
  displayRays(camera, frameBuffer, clear, texPos, texDir, texData, length, r,g,b,a){
    if(!this.isReady) return;

    const gl = this.gl;
    const programInfo = this.programDisplay;

    // if(frameBuffer){
    //   frameBuffer.bind();
    // } else {
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
    // }


    // clear = true;
    if (clear){
      gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
      gl.clearDepth(1.0);                 // Clear everything
      gl.enable(gl.DEPTH_TEST);           // Enable depth testing
      gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

      // Clear the canvas before we start drawing on it.

      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }

    gl.disable(gl.BLEND);
    // gl.blendFunc(gl.ONE, gl.ONE);
    // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

    // gl.enable(gl.CULL_FACE);

    // Tell WebGL to use our program when drawing
    gl.useProgram(programInfo.program);


    
    if ( camera.parent === null ) camera.updateMatrixWorld();
    this.projScreenMatrix.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );
    if(programInfo.uniformLocations.projectionMatrix !== undefined){
      if (camera){
        gl.uniformMatrix4fv(
          programInfo.uniformLocations.projectionMatrix,
          false,
          this.projScreenMatrix.elements);
        let campos = camera.position;
        gl.uniform4f(
            programInfo.uniformLocations.cameraPosition, campos.x, campos.y, campos.z, 1);
      }
    }

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texPos);
    gl.uniform1i(programInfo.uniformLocations.texPos, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texDir);
    gl.uniform1i(programInfo.uniformLocations.texDir, 1);

    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, texData);
    gl.uniform1i(programInfo.uniformLocations.texData, 2);


    gl.uniform4f(programInfo.uniformLocations.color, r,g,b,a);

    {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
      // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

     // Tell WebGL how to pull out the positions from the position
     // buffer into the vertexPosition attribute.

       const numComponents = 3;  // pull out 2 values per iteration
       const type = gl.FLOAT;    // the data in the buffer is 32bit floats
       const normalize = false;  // don't normalize
       const stride = 0;         // how many bytes to get from one set of values to the next
                                 // 0 = use type and numComponents above
       const offset = 0;         // how many bytes inside the buffer to start from
      //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
       gl.vertexAttribPointer(
           programInfo.attribLocations.uv,
           numComponents,
           type,
           normalize,
           stride,
           offset);
       gl.enableVertexAttribArray(
           programInfo.attribLocations.uv);
     }

     // Set the shader uniforms
    //  for (let i = 0; i < length; i++)
    //  {
       const offset = 0;
       // const vertexCount = this.inputResX * this.inputResY;
       const vertexCount = 2 * length;
      //  gl.drawArrays(this.drawMode, offset, vertexCount);
       gl.drawArrays(gl.LINES, offset, vertexCount);
       gl.drawArrays(gl.POINTS, offset, vertexCount);
    //  }

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  }


  // setScene(scene){
  //   this.scene = scene;
  //   this.init();
  // }

  addShape(shape){
    if(!shape.isMesh)
      throw "Shape is not a Mesh";
    this.shapes.push(shape);
  }
  addSound(sound){
    this.sounds.push(sound);
    // this.addShape(sound);
  }
}
