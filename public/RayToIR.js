class RayToIR {
  constructor(gl, inputResX, inputResY, speedOfSound, irLength) {
    this.gl = gl;
    this.irLength = irLength;
    this.fb = new FrameBuffer(gl, 4096, 1 << Math.ceil(Math.log2(irLength / 4096 / 4)), 2);
    // this.vertexBuffer = gl.createBuffer();
    this.inputResX = inputResX;
    this.inputResY = inputResY;
    this.createUVBuffer();
    this.normalizeFactor = 1000.;
    this.speedOfSound = speedOfSound || 340;
    this.ready = false;
    this.programInfo = new RayToIRProgram(gl);
    this.programInfo.ready.then(()=>{
      this.ready = true;
    });
  }
  createUVBuffer(){
    const gl = this.gl;
    let arr = new Uint16Array(this.inputResX * this.inputResY * 2);
    for (var x = 0; x < this.inputResX; x++) {
      for (var y = 0; y < this.inputResY; y++) {
        let ind = (x + y * this.inputResX) * 2 ;
        arr[ind] = x; // (this.inputResX);
        arr[ind + 1] = y; // (this.inputResY);
      }
    }
    this.uvBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, arr, gl.STATIC_DRAW);
    console.log(arr);
    
  }

  render(camera, texPos, texDir, texData, rayCount, maxIntensity, clear){
    if(!this.ready) return;
    const gl = this.gl;
    const programInfo = this.programInfo;

    this.fb.bind();

    // No depth testing
    gl.disable(gl.DEPTH_TEST);

    // Enable blending
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE);


    // let clear = true;
    if (clear){
      gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
      // gl.clearDepth(1.0);                 // Clear everything
      gl.disable(gl.DEPTH_TEST);           // Enable depth testing
      // gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

      // Clear the canvas before we start drawing on it.

      gl.clear(gl.COLOR_BUFFER_BIT);
    }


    // Tell WebGL to use our program when drawing
    gl.useProgram(programInfo.program);
    gl.uniform1f(programInfo.uniformLocations.normalize, this.normalizeFactor / (maxIntensity));

    gl.uniform1f(programInfo.uniformLocations.speedOfSound, this.speedOfSound);

    gl.uniform3fv(programInfo.uniformLocations.cameraDir, camera.getWorldDirection().toArray());

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texPos);
    gl.uniform1i(programInfo.uniformLocations.texPos, 0);
    
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texDir);
    gl.uniform1i(programInfo.uniformLocations.texDir, 1);
    
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, texData);
    gl.uniform1i(programInfo.uniformLocations.texData, 2);

    {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
      // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

     // Tell WebGL how to pull out the positions from the position
     // buffer into the vertexPosition attribute.

       const numComponents = 2;  // pull out 2 values per iteration
       const type = gl.UNSIGNED_SHORT;
       const stride = 0;         // how many bytes to get from one set of values to the next
                                 // 0 = use type and numComponents above
       const offset = 0;         // how many bytes inside the buffer to start from
      //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
       gl.vertexAttribIPointer(
           programInfo.attribLocations.uv,
           numComponents,
           type,
           stride,
           offset);
       gl.enableVertexAttribArray(
           programInfo.attribLocations.uv);
     }

     // Set the shader uniforms

     {
       const offset = 0;
       const vertexCount = rayCount;
      //  gl.drawArrays(this.drawMode, offset, vertexCount);
       gl.drawArrays(gl.POINTS, offset, vertexCount);
      //  console.log(vertexCount);

     }

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }

  getData(ind){
    let data = this.fb.getData(ind).slice(0, this.irLength);
    return data;
  }
}
