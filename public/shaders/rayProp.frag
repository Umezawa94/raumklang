#version 300 es

precision highp float;
precision highp int;

uniform float uNormalize;
uniform mat4 uProjectionMatrix;

flat in uvec2 uv;


uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;
uniform sampler2D uTexNorm;
uniform sampler2D uTexMat;
// uniform int uRayWidth;
// uniform int uRayHeight;

// uniform uint uRayCount;
uniform uint uNewRayCount;
uniform uint uHandledRayCount;
uniform uint uRayMultiplier;


layout(location = 0) out vec4 oTexPos;
layout(location = 1) out vec4 oTexDir;
layout(location = 2) out vec4 oTexData;
layout(location = 3) out vec4 oTexNorm;
layout(location = 4) out vec4 oTexMat;
// out vec4 outColor;

uniform sampler2D uTexVert;
uniform sampler2D uTexVertNorm;
uniform sampler2D uTexVertMat;
uniform highp usampler2D uTexVertInd;
// uniform int uVertWidth;
// uniform int uVertHeight;
uniform uint uVertCount;

uvec2 vectSize;
const float INFINITY = 1e6;
const float dampening = 0.1;

vec4 hash44(vec4 p4)
{
	p4 = fract(p4  * vec4(.1031, .1030, .0973, .1099));
    p4 += dot(p4, p4.wzxy+19.19);
    return fract((p4.xxyz+p4.yzzw)*p4.zywx);
}

uvec2 indToUV(uint index, uvec2 size){
  uint y = index / size.x;
  uint x = index - y * size.x;
  // uint y = 0u;
  // uint x = 0u;
  return uvec2(x,y);
}

void intersectRay(in vec3 iPos, in vec3 iDir, in vec4 iData, in uint id, inout vec4 hit){
  // hit = vec4(1.,0.,0.,0.);
  // return;
  uint i0 = texelFetch(uTexVertInd, ivec2(indToUV(id, vectSize)), 0).x;
  uint i1 = texelFetch(uTexVertInd, ivec2(indToUV(id + 1u, vectSize)), 0).x;
  uint i2 = texelFetch(uTexVertInd, ivec2(indToUV(id + 2u, vectSize)), 0).x;

  vec3 v0 = texelFetch(uTexVert, ivec2(indToUV(i0, vectSize)), 0).xyz;
  vec3 v1 = texelFetch(uTexVert, ivec2(indToUV(i1, vectSize)), 0).xyz;
  vec3 v2 = texelFetch(uTexVert, ivec2(indToUV(i2, vectSize)), 0).xyz;

  vec3 n0 = texelFetch(uTexVertNorm, ivec2(indToUV(i0, vectSize)), 0).xyz;
  vec3 n1 = texelFetch(uTexVertNorm, ivec2(indToUV(i1, vectSize)), 0).xyz;
  vec3 n2 = texelFetch(uTexVertNorm, ivec2(indToUV(i2, vectSize)), 0).xyz;

  vec3 edge1 = v1 - v0;
  vec3 edge2 = v2 - v0;
  vec3 pvec = cross( iDir.xyz, edge2 );
  float det = dot( edge1, pvec );
  float invdet = 1.0 / det;
  vec3 tvec = iPos.xyz - v0;
  vec3 qvec = cross( tvec, edge1 );
  vec4 h = vec4(
    dot( edge2, qvec ) * invdet,  //t
    dot( tvec, pvec ) * invdet,   //u
    dot( iDir.xyz, qvec ) * invdet,     //v
    float(id)                     //id
    );
  vec3 normal = normalize(n0 * (1. - h.y - h.z) + n1 * (h.y) + n2 * (h.z));
  if (h.y >= 0.0                  //u
    && h.z >= 0.0                 //v
    && h.y+h.z <= 1.0             //u+v
    && h.x < hit.x                //t<t
    && h.x > 0.001                //t
    && dot(normal, iDir) >= 0.)
    {
      hit = h;
    }
}

float calcIntensity(float diff, float spec, vec4 material){
    return (spec*material.x + diff*material.y);
}

void reflectRay(in vec3 iPos, in vec3 iDir, in vec4 iData, in uint id, in vec4 hit, out vec3 oPos, out vec3 oDir, out vec4 oData, out vec4 oNorm, out vec4 oMat){
    uint i0 = texelFetch(uTexVertInd, ivec2(indToUV(id, vectSize)), 0).x;
    uint i1 = texelFetch(uTexVertInd, ivec2(indToUV(id + 1u, vectSize)), 0).x;
    uint i2 = texelFetch(uTexVertInd, ivec2(indToUV(id + 2u, vectSize)), 0).x;

    vec3 n0 = texelFetch(uTexVertNorm, ivec2(indToUV(i0, vectSize)), 0).xyz;
    vec3 n1 = texelFetch(uTexVertNorm, ivec2(indToUV(i1, vectSize)), 0).xyz;
    vec3 n2 = texelFetch(uTexVertNorm, ivec2(indToUV(i2, vectSize)), 0).xyz;
    
    vec4 m0 = texelFetch(uTexVertMat, ivec2(indToUV(i0, vectSize)), 0);
    vec4 m1 = texelFetch(uTexVertMat, ivec2(indToUV(i1, vectSize)), 0);
    vec4 m2 = texelFetch(uTexVertMat, ivec2(indToUV(i2, vectSize)), 0);

    oPos = iPos + iDir * hit.x;
    vec3 normal = normalize(n0 * (1. - hit.y - hit.z) + n1 * (hit.y) + n2 * (hit.z));
    vec4 material = (m0 * (1. - hit.y - hit.z) + m1 * (hit.y) + m2 * (hit.z));
    oDir = normalize(hash44(vec4(uv, iDir.xz)).xyz);//iDir, normal
    oData = iData;
    oData.x = iData.x + distance(oPos, iPos);
    // oData.y = max(0., oData.y - dampening);
    if(dot(iDir, normal) > 0.0){
      // texData.y = 0.;
      normal.xyz *= -1.0;
    }
    float diff = dot(oDir, normal);
    if(diff < .0){
      diff *=-1.;
      oDir *=-1.;
    }
    vec3 reflectedDir = normalize(reflect(iDir, normal));
    float spec = max(0.0, dot(oDir, reflectedDir));

    float intensity = exp(oData.y);
    // intensity *= (spec*material.x + diff*material.y) / float(uRayMultiplier);
    intensity *= calcIntensity(diff, spec, material); // float(uRayMultiplier);
    oData.y = log(intensity);

    // oData.y *= material.x;
    // oData.y *= ;
    oData.z = float(id);
    oNorm.xyz = normal;
    oMat = material;

}

void bounce(in vec3 iPos, in vec3 iDir, in vec4 iData, out vec3 oPos, out vec3 oDir, out vec4 oData, out vec4 oNorm, out vec4 oMat){
  vec4 hit = vec4(INFINITY, 0.0, 0.0, -1.0);
  for(uint i = 0u; i < uVertCount; i += 3u)
  {
    intersectRay(iPos, iDir, iData, i, hit);
  }
  // hit.x = 1.0 + float(uVertCount);
  reflectRay(iPos, iDir, iData, uint(hit.w), hit, oPos, oDir, oData, oNorm, oMat);
  // uint i0 = texelFetch(uTexVertInd, ivec2(indToUV(3u, vectSize)), 0).x;
  // oPos = hit.xyz;
}


void main() {
  uvec2 raySize =  uvec2(textureSize(uTexPos, 0));
  vectSize =  uvec2(textureSize(uTexVert, 0));

  // uniform uint uRayCount;
  // uniform uint uNewRayCount;
  // uint newRays = uRayCount - uHandledRayCount;
  // uRayCount = uRayCount;

  uint index = uv.x + uv.y * raySize.x;
  if(index < uint(uHandledRayCount)){
    oTexPos = texelFetch(uTexPos, ivec2(uv.xy), 0);
    oTexDir = texelFetch(uTexDir, ivec2(uv.xy), 0);
    oTexData = texelFetch(uTexData, ivec2(uv.xy), 0);
    oTexNorm = texelFetch(uTexNorm, ivec2(uv.xy), 0);
    oTexMat = texelFetch(uTexMat, ivec2(uv.xy), 0);
  } else {
    uint offset = index - uHandledRayCount;
    uint i = offset / uNewRayCount;
    if(i<uRayMultiplier){
      index -= uNewRayCount * (i + 1u);
      uvec2 uvSrc = indToUV(index, raySize);
      bounce(
        texelFetch(uTexPos, ivec2(uvSrc.xy), 0).xyz,
        normalize(texelFetch(uTexDir, ivec2(uvSrc.xy), 0).xyz),
        texelFetch(uTexData, ivec2(uvSrc.xy), 0),
        oTexPos.xyz, oTexDir.xyz, oTexData, oTexNorm, oTexMat);
    }
  }
}
