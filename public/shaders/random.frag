#version 300 es
in vec2 vUv;
out vec4 oColor;
uniform float uSeed;

// highp float rand(vec2 co)
// {
//     highp float a = 12.9898;
//     highp float b = 78.233;
//     highp float c = 43758.5453;
//     highp float dt= dot(co.xy ,vec2(a,b));
//     highp float sn= mod(dt,3.14);
//     return fract(sin(sn) * c);
// }

void main() {
    oColor = noise4(vec3(vUv, uSeed)); 
}