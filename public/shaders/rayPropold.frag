#version 300 es

precision highp float;

uniform float uNormalize;
uniform mat4 uProjectionMatrix;

flat in uvec2 uv;


uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;
uniform sampler2D uTexNorm;
uniform sampler2D uTexMat;
// uniform int uRayWidth;
// uniform int uRayHeight;

// uniform uint uRayCount;
uniform uint uNewRayCount;
uniform uint uHandledRayCount;
uniform uint uRayMultiplier;


layout(location = 0) out vec4 oTexPos;
layout(location = 1) out vec4 oTexDir;
layout(location = 2) out vec4 oTexData;
layout(location = 3) out vec4 oTexNorm;
layout(location = 4) out vec4 oTexMat;
// out vec4 outColor;

uniform sampler2D uTexVert;
uniform sampler2D uTexVertNorm;
uniform sampler2D uTexVertMat;
// uniform int uVertWidth;
// uniform int uVertHeight;
uniform uint uVertCount;

uvec2 vectSize;
const float INFINITY = 1e6;
const float dampening = 0.1;


uvec2 indToUV(uint index, uvec2 size){
  uint y = index / size.x;
  uint x = index - y * size.x;
  // uint y = 0u;
  // uint x = 0u;
  return uvec2(x,y);
}

void intersectRay(in vec3 iPos, in vec3 iDir, in vec4 iData, in uint id, inout vec4 hit){
  vec3 v0 = texelFetch(uTexVert, ivec2(indToUV(id, vectSize)), 0).xyz;
  vec3 v1 = texelFetch(uTexVert, ivec2(indToUV(id + 1u, vectSize)), 0).xyz;
  vec3 v2 = texelFetch(uTexVert, ivec2(indToUV(id + 2u, vectSize)), 0).xyz;
  vec3 edge1 = v1 - v0;
  vec3 edge2 = v2 - v0;
  vec3 pvec = cross( iDir.xyz, edge2 );
  float det = dot( edge1, pvec );
  float invdet = 1.0 / det;
  vec3 tvec = iPos.xyz - v0;
  vec3 qvec = cross( tvec, edge1 );
  vec4 h = vec4(
    dot( edge2, qvec ) * invdet,  //t
    dot( tvec, pvec ) * invdet,   //u
    dot( iDir.xyz, qvec ) * invdet,     //v
    float(id)                     //id
    );
  if (h.y >= 0.0                  //u
    && h.z >= 0.0                 //v
    && h.y+h.z <= 1.0             //u+v
    && h.x < hit.x                //t<t
    && h.x > 0.0001)                //t
    {
      hit = h;
    }
}


void reflectRay(in vec3 iPos, in vec3 iDir, in vec4 iData, in uint id, in vec4 hit, out vec3 oPos, out vec3 oDir, out vec4 oData, out vec4 oNorm, out vec4 oMat){
    vec3 n0 = texelFetch(uTexVertNorm, ivec2(indToUV(id, vectSize)), 0).xyz;
    vec3 n1 = texelFetch(uTexVertNorm, ivec2(indToUV(id + 1u, vectSize)), 0).xyz;
    vec3 n2 = texelFetch(uTexVertNorm, ivec2(indToUV(id + 2u, vectSize)), 0).xyz;
    
    vec4 m0 = texelFetch(uTexVertMat, ivec2(indToUV(id, vectSize)), 0);
    vec4 m1 = texelFetch(uTexVertMat, ivec2(indToUV(id + 1u, vectSize)), 0);
    vec4 m2 = texelFetch(uTexVertMat, ivec2(indToUV(id + 2u, vectSize)), 0);

    oPos = iPos + iDir * hit.x;
    vec3 normal = normalize(n0 * (1. - hit.y - hit.z) + n1 * (hit.y) + n2 * (hit.z));
    oNorm.xyz = normal;
    vec4 material = (m0 * (1. - hit.y - hit.z) + m1 * (hit.y) + m2 * (hit.z));
    oMat = material;
    oDir = reflect(iDir, normal);
    oData = iData;
    oData.x = iData.x + distance(oPos, iPos);
    // oData.y = max(0., oData.y - dampening);
    oData.y *= material.x;
    oData.z = float(id);

}

void bounce(in vec3 iPos, in vec3 iDir, in vec4 iData, out vec3 oPos, out vec3 oDir, out vec4 oData, out vec4 oNorm, out vec4 oMat){
  vec4 hit = vec4(INFINITY, 0.0, 0.0, -1.0);
  for(uint i = 0u; i < uVertCount; i += 3u)
  {
    intersectRay(iPos, iDir, iData, i, hit);
  }
  // hit.x = 1.0 + float(uVertCount);
  reflectRay(iPos, iDir, iData, uint(hit.w), hit, oPos, oDir, oData, oNorm, oMat);
}


void main() {
  uvec2 raySize =  uvec2(textureSize(uTexPos, 0));
  vectSize =  uvec2(textureSize(uTexVert, 0));

  // uniform uint uRayCount;
  // uniform uint uNewRayCount;
  // uint newRays = uRayCount - uHandledRayCount;
  // uRayCount = uRayCount;

  uint index = uv.x + uv.y * raySize.x;
  if(index < uint(uHandledRayCount)){
    oTexPos = texelFetch(uTexPos, ivec2(uv.xy), 0);
    oTexDir = texelFetch(uTexDir, ivec2(uv.xy), 0);
    oTexData = texelFetch(uTexData, ivec2(uv.xy), 0);
    oTexNorm = texelFetch(uTexNorm, ivec2(uv.xy), 0);
    oTexMat = texelFetch(uTexMat, ivec2(uv.xy), 0);
  } else if(index < uHandledRayCount + uNewRayCount){
    index = index - uint(uNewRayCount);
    uvec2 uvSrc = indToUV(index, raySize);
    bounce(
      texelFetch(uTexPos, ivec2(uvSrc.xy), 0).xyz,
      normalize(texelFetch(uTexDir, ivec2(uvSrc.xy), 0).xyz),
      texelFetch(uTexData, ivec2(uvSrc.xy), 0),
      oTexPos.xyz, oTexDir.xyz, oTexData, oTexNorm, oTexMat);
  }
}
