#version 300 es

precision highp float;

uniform float uNormalize;
uniform mat4 uProjectionMatrix;

flat in uvec2 uv;


uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;
uniform sampler2D uTexNorm;
uniform sampler2D uTexMat;
// uniform int uRayWidth;
// uniform int uRayHeight;
uniform int uRayCount;


layout(location = 0) out vec4 oTexPos;
layout(location = 1) out vec4 oTexDir;
layout(location = 2) out vec4 oTexData;
layout(location = 3) out vec4 oTexNormal;
// out vec4 outColor;

uniform sampler2D uTexVert;
uniform sampler2D uTexVertNorm;
uniform sampler2D uTexVertMat;
uniform highp usampler2D uTexVertInd;
// uniform int uVertWidth;
// uniform int uVertHeight;
uniform uint uVertCount;

uniform vec4 uCamera;
// const vec4 uCamera = vec4(5.0, 5.0, 1.0, 1.0);
uvec2 vectSize;
const float INFINITY = 1e6;

uvec2 indToUV(uint index, uvec2 size){
  uint y = index / size.x;
  uint x = index - y * size.x;
  // uint y = 0u;
  // uint x = 0u;
  return uvec2(x,y);
}

void intersectRay(in vec3 iPos, in vec3 iDir, in vec4 iData, in uint id, inout vec4 hit){
  uint i0 = texelFetch(uTexVertInd, ivec2(indToUV(id, vectSize)), 0).x;
  uint i1 = texelFetch(uTexVertInd, ivec2(indToUV(id + 1u, vectSize)), 0).x;
  uint i2 = texelFetch(uTexVertInd, ivec2(indToUV(id + 2u, vectSize)), 0).x;

  vec3 v0 = texelFetch(uTexVert, ivec2(indToUV(i0, vectSize)), 0).xyz;
  vec3 v1 = texelFetch(uTexVert, ivec2(indToUV(i1, vectSize)), 0).xyz;
  vec3 v2 = texelFetch(uTexVert, ivec2(indToUV(i2, vectSize)), 0).xyz;
  vec3 edge1 = v1 - v0;
  vec3 edge2 = v2 - v0;
  vec3 pvec = cross( iDir.xyz, edge2 );
  float det = dot( edge1, pvec );
  float invdet = 1.0 / det;
  vec3 tvec = iPos.xyz - v0;
  vec3 qvec = cross( tvec, edge1 );
  vec4 h = vec4(
    dot( edge2, qvec ) * invdet,  //t
    dot( tvec, pvec ) * invdet,   //u
    dot( iDir.xyz, qvec ) * invdet,     //v
    float(id)                     //id
    );
  if (h.y >= 0.0                  //u
    && h.z >= 0.0                 //v
    && h.y+h.z <= 1.0             //u+v
    && h.x < hit.x                //t<t
    && h.x > 0.0001)                //t
    {
      hit = h;
    }
}

void intersect(in vec3 iPos, in vec3 iDir, in vec4 iData, out vec3 oPos, out vec3 oDir, out vec4 oData){
  vec4 hit = vec4(1.0, 0.0, 0.0, -1.0);
  for(uint i = 0u; i < uVertCount; i += 3u)
  {
    intersectRay(iPos, iDir, iData, i, hit);
    // iData.y = 0.0;
  }
  if(hit.w != -1.0){
    iData.w = 1.0;
  }
  oPos = iPos;
  oDir = iDir;
  oData = iData;
//
}
float calcIntensity(float diff, float spec, vec4 material){
    return (spec*material.x + diff*material.y);
}

void main() {
  vectSize = uvec2(textureSize(uTexVert, 0));

  uvec2 raySize =  uvec2(textureSize(uTexPos, 0));
  uint index = uv.x + uv.y * raySize.x;
  if(index < uint(uRayCount)){
    vec4 texPos = texelFetch(uTexPos, ivec2(uv.xy), 0);
    vec4 texDir = texelFetch(uTexDir, ivec2(uv.xy), 0);
    vec4 texNorm = texelFetch(uTexNorm, ivec2(uv.xy), 0);
    vec4 texData = texelFetch(uTexData, ivec2(uv.xy), 0);
    vec4 texMat = texelFetch(uTexMat, ivec2(uv.xy), 0);
    // uCamera.xyz = - uCamera.xyz;
    // vec4 camera = uCamera;
    // camera.w = 1.0;
    vec4 dir = uCamera - texPos;
    if(dot(dir, texNorm) < 0.0){
      // texData.y = 0.;
      texNorm *= -1.0;
    }
    // oTexDir = vec4(1.);
    // oTexDir.z += 2.;
    texData.x += length(dir);


    float spec = max(0.0, dot(normalize(dir), texDir));
    float diff = dot(texDir, texNorm);
    if(diff <= 0.0){
      texData.w = 1.;
    } else {
      // texData.y *= diff;    
      float intensity = exp(texData.y);
      intensity *= calcIntensity(diff, spec, texMat);
      texData.y = log(intensity);
      
    }

    intersect(texPos.xyz, dir.xyz, texData, oTexPos.xyz, oTexDir.xyz, oTexData);
  }
}
