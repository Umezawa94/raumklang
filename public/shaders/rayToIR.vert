#version 300 es

precision highp float;

in uvec2 aUVPosition;

uniform float uNormalize;

out vec4 colorLeft;
out vec4 colorRight;

const uint bufferWidth = 4096u;
const uint bufferHeight = 2u;
uniform lowp float uSpeedOfSound;
const uint resolution = 44100u;

uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;

uniform vec3 uCameraDir;
const vec3 up = vec3(0., 1., 0.);

float toScreen(uint val, uint size){
  return  float(val+1u)/float(size/2u)-1.0;
}
vec2 toScreen(uvec2 val, uvec2 size){
  return  vec2(toScreen(val.x, size.x),toScreen(val.y, size.y));
}

void main() {
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  vec3 right = normalize(cross(up, uCameraDir));
  vec3 left = -right;

  // vec3 dir = normalize(texture(uTexDir, aUVPosition).xyz);
  vec3 dir =  texelFetch(uTexDir, ivec2(aUVPosition.xy), 0).xyz;

  vec4 data =  texelFetch(uTexData, ivec2(aUVPosition.xy), 0);

  float intensity = exp(data.y);
  // intensity = 0.1;
  if(data.w > .5) intensity = 0.;
  intensity *=  uNormalize;
  float intensityLeft = intensity;// * max(0., dot(left, dir)*0.5 + 0.5);
  float intensityRight = intensity;// * max(0., dot(right, dir)*0.5 + 0.5);

  
  uint frame = uint(data.x / (uSpeedOfSound / float(resolution)));
  // frame = 0u;

  // frame = floor(aUVPosition.x*512.0 + aUVPosition.y*512.0*512.0);
  // intensity = frame/(512.0*512.0);
  // intensity = aVertexPosition.y;

  uint ind = frame % 4u;
  frame = frame/4u;
  // aVertexPosition.xy = aUVPosition.xy * vec2(2.0, 2.0) - 1.0 ;
  uint y = frame/bufferWidth;
  uint x = frame % bufferWidth;
  // color.r = intensity;
  // color.g = intensity;

  gl_Position = vec4(toScreen(x,bufferWidth),toScreen(y,bufferHeight), 0.0, 1.0);
  // gl_Position = aVertexPosition;
  // gl_Position.x = color.x / 100.0;
  // gl_Position = vec4(0.5, 0.5, 0.0, 1.0);

  if(ind == 0u){
    colorLeft.r = intensityLeft;
    colorRight.r = intensityRight;
  }
  if(ind == 1u){
    colorLeft.g = intensityLeft;
    colorRight.g = intensityRight;
  }
  if(ind == 2u){
    colorLeft.b = intensityLeft;
    colorRight.b = intensityRight;
  }
  if(ind == 3u){
    colorLeft.a = intensityLeft;
    colorRight.a = intensityRight;
  }

  // intensity = aVertexPosition.y;
  gl_PointSize = 1.0;
}
