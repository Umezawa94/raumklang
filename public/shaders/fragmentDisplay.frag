precision highp float;

uniform sampler2D uTex;

varying lowp vec2 uv;
void main() {
  // gl_FragColor = //vec4(uv, 0.0, 1.0);
  gl_FragColor = texture2D(uTex, uv);
  // gl_FragColor = texture2D(uTex, vec2(atan(uv.x)/atan(1.0),atan(uv.y)/atan(1.0)));
}
