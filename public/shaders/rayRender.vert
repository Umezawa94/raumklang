#version 300 es

precision highp float;

in uvec3 aUVPosition;

uniform mat4 uProjectionMatrix;
uniform mat4 uNormalMatrix;


uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;

vec4 texData;

out vec4 color;

uniform vec4 uColor;

float fade(float low, float high, float value){
    float mid = (low+high)*0.5;
    float range = (high-low)*0.5;
    float x = 1.0 - clamp(abs(mid-value)/range, 0.0, 1.0);
    return smoothstep(0.0, 1.0, x);
}

vec3 getColor(float intensity){
    vec3 blue = vec3(0.0, 0.0, 1.0);
    vec3 cyan = vec3(0.0, 1.0, 1.0);
    vec3 green = vec3(0.0, 1.0, 0.0);
    vec3 yellow = vec3(1.0, 1.0, 0.0);
    vec3 red = vec3(1.0, 0.0, 0.0);

    vec3 color = (
        fade(-0.25, 0.25, intensity)*blue +
        fade(0.0, 0.5, intensity)*cyan +
        fade(0.25, 0.75, intensity)*green +
        fade(0.5, 1.0, intensity)*yellow +
        smoothstep(0.75, 1.0, intensity)*red
    );
    return color;
}

float toScreen(float val, float size){
  return  (val+1.0)/(size/2.0)-1.0;
}

void main() {
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  vec4 texPos =  texelFetch(uTexPos, ivec2(aUVPosition.xy), 0);
  vec4 texDir =  texelFetch(uTexDir, ivec2(aUVPosition.xy), 0);
  vec4 texData =  texelFetch(uTexData, ivec2(aUVPosition.xy), 0);
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  // vec4 aVertexPosition =  texture2D(uTexPos, aUVPosition.xy);
  // vec4 aVertexDirection =  texture2D(uTexDir, aUVPosition.xy);

  // texData = texture2D(uTexData, aUVPosition.xy);
  float intensity = sqrt(sqrt(exp(texData.y)));
  // intensity = 1. + .1 * log(intensity));
  color = vec4(getColor(intensity), 1.);
  // color = vec4(1.) * intensity;
  if (texData.w > .5){
    color = vec4(0.);
  }
  // color = vec4(1.);
  // color = uColor;
  texPos.w = 1.0;
  texDir.w = 1.0;
  // if(aUVPosition.z > 0.0){
  //   aVertexPosition.xyz += aVertexDirection.xyz;
  //   // aVertexPosition.xyz += ;
  //   // color = 1. - color;
  // }


  gl_Position = uProjectionMatrix * texPos;
  // gl_Position.w = 1.;
  gl_PointSize = 1.;
}
