// attribute vec3 inputPosition;
// attribute vec2 inputTexCoord;
// attribute vec3 inputNormal;
//
// uniform mat4 projection, modelview, normalMat;
//
// varying vec3 normalInterp;
// varying vec3 vertPos;
//
// void main(){
//     gl_Position = projection * modelview * vec4(inputPosition, 1.0);
//     vec4 vertPos4 = modelview * vec4(inputPosition, 1.0);
//     vertPos = vec3(vertPos4) / vertPos4.w;
//     normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
// }

precision highp float;

attribute vec3 aUVPosition;

uniform mat4 uProjectionMatrix;
uniform mat4 uNormalMatrix;


uniform sampler2D uTexPos;
uniform sampler2D uTexDir;
uniform sampler2D uTexData;

vec4 texData;

varying vec4 color;

uniform vec4 uColor;

float fade(float low, float high, float value){
    float mid = (low+high)*0.5;
    float range = (high-low)*0.5;
    float x = 1.0 - clamp(abs(mid-value)/range, 0.0, 1.0);
    return smoothstep(0.0, 1.0, x);
}

vec3 getColor(float intensity){
    vec3 blue = vec3(0.0, 0.0, 1.0);
    vec3 cyan = vec3(0.0, 1.0, 1.0);
    vec3 green = vec3(0.0, 1.0, 0.0);
    vec3 yellow = vec3(1.0, 1.0, 0.0);
    vec3 red = vec3(1.0, 0.0, 0.0);

    vec3 color = (
        fade(-0.25, 0.25, intensity)*blue +
        fade(0.0, 0.5, intensity)*cyan +
        fade(0.25, 0.75, intensity)*green +
        fade(0.5, 1.0, intensity)*yellow +
        smoothstep(0.75, 1.0, intensity)*red
    );
    return color;
}

float toScreen(float val, float size){
  return  (val+1.0)/(size/2.0)-1.0;
}

void main() {
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  vec4 aVertexPosition =  texture2D(uTexPos, aUVPosition.xy);
  vec4 aVertexDirection =  texture2D(uTexDir, aUVPosition.xy);

  texData = texture2D(uTexData, aUVPosition.xy);
  
  float intensity = sqrt(sqrt(exp(texData.y)));
  // intensity = 1. + .1 * log(intensity));
  color = vec4(getColor(intensity), 1.);
  // color = uColor;
  aVertexPosition.w = 1.0;
  if(aUVPosition.z > 0.0){
    aVertexPosition.xyz += normalize(aVertexDirection.xyz) * 0.2;
    // aVertexPosition.xyz += ;
    // color = 1. - color;
  }


  gl_Position = uProjectionMatrix * aVertexPosition;
  gl_PointSize = 3.0;
}
