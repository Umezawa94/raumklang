// attribute vec3 inputPosition;
// attribute vec2 inputTexCoord;
// attribute vec3 inputNormal;
//
// uniform mat4 projection, modelview, normalMat;
//
// varying vec3 normalInterp;
// varying vec3 vertPos;
//
// void main(){
//     gl_Position = projection * modelview * vec4(inputPosition, 1.0);
//     vec4 vertPos4 = modelview * vec4(inputPosition, 1.0);
//     vertPos = vec3(vertPos4) / vertPos4.w;
//     normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
// }

precision highp float;

vec4 aVertexPosition;
attribute vec2 aUVPosition;

uniform float uNormalize;

varying vec4 color;

const lowp float bufferWidth = 4096.0;
const lowp float bufferHeight = 2.0;
const lowp float speedOfSound = 340.0*1.0;
const lowp float resolution = 44100.0;

uniform sampler2D uTex;

float toScreen(float val, float size){
  return  (val+1.0)/(size/2.0)-1.0;
}

void main() {
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  aVertexPosition = texture2D(uTex, aUVPosition);
  float intensity = aVertexPosition.y * uNormalize;
  float frame = floor(aVertexPosition.x / (speedOfSound / resolution));

  // frame = floor(aUVPosition.x*512.0 + aUVPosition.y*512.0*512.0);
  // intensity = frame/(512.0*512.0);
  // intensity = aVertexPosition.y;

  float ind = mod(frame, 4.0);
  frame = frame/4.0;
  // aVertexPosition.xy = aUVPosition.xy * vec2(2.0, 2.0) - 1.0 ;
  float x = mod(frame, bufferWidth);
  float y = floor(frame/bufferWidth);
  // color.r = intensity;
  // color.g = intensity;

  gl_Position = vec4(toScreen(x,bufferWidth),toScreen(y,bufferHeight), 0.0, 1.0);
  // gl_Position = aVertexPosition;
  // gl_Position.x = color.x / 100.0;
  // gl_Position = vec4(0.5, 0.5, 0.0, 1.0);

  if(ind == 0.0){
    color.r = intensity;
  }
  if(ind == 1.0){
    color.g = intensity;
  }
  if(ind == 2.0){
    color.b = intensity;
  }
  if(ind == 3.0){
    color.a = intensity;
  }

  // intensity = aVertexPosition.y;
  gl_PointSize = 1.0;
}
