
precision highp float;

attribute vec4 aVertexPosition;
// attribute vec2 aInputTexCoord;
attribute vec3 aInputNormal;
attribute vec2 aUV;
attribute vec4 aColor;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

varying vec4 vPos;
varying vec4 vColor;

void main() {
  
  vPos = uModelViewMatrix * vec4(aVertexPosition.xyz, 1.);
  gl_Position = uProjectionMatrix * vPos;
  vColor = vec4(aColor.rgb, 1.);
}
