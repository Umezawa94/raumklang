#version 300 es
// attribute vec3 inputPosition;
// attribute vec2 inputTexCoord;
// attribute vec3 inputNormal;
//
// uniform mat4 projection, modelview, normalMat;
//
// varying vec3 normalInterp;
// varying vec3 vertPos;
//
// void main(){
//     gl_Position = projection * modelview * vec4(inputPosition, 1.0);
//     vec4 vertPos4 = modelview * vec4(inputPosition, 1.0);
//     vertPos = vec3(vertPos4) / vertPos4.w;
//     normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
// }

precision highp float;

// vec4 aVertexPosition;
in uvec2 aUVPosition;

uniform float uNormalize;
uniform mat4 uProjectionMatrix;

flat out uvec2 uv;


uniform sampler2D uTexPos;
// uniform sampler2D uTexDir;
// uniform float uRayWidth;
// uniform float uRayHeight;
// uniform float uRayCount;
//
//
// uniform sampler2D uTexVert;
// uniform sampler2D uTexVertNorm;
// uniform float uVertWidth;
// uniform float uVertHeight;
// uniform float uVertCount;

float toScreen(float val, int size){
  return  (val+1.0)/float(size/2)-1.0;
}

vec2 toScreen(vec2 val, ivec2 size){
  return vec2(toScreen(val.x, size.x), toScreen(val.y, size.y));
}

void main() {
  // aVertexPosition = vec4(0.0,0.0,0.0,1.0);
  // aVertexPosition =  texture2D(uTexPos, aUVPosition.xy);
  // vec4 aVertexDirection =  texture2D(uTexDir, aUVPosition.xy);
  // aVertexPosition.w = 1.0;
  // if(aUVPosition.z > 0.0){
  //   aVertexPosition = aVertexPosition + aVertexDirection;
  // }
  uv.xy = aUVPosition.xy;
  ivec2 raySize =  textureSize(uTexPos, 0);
  gl_Position =  vec4(toScreen(vec2(float(aUVPosition.x), float(aUVPosition.y)), raySize.xy), 0.0, 1.0);
  gl_PointSize = 1.0;
}
