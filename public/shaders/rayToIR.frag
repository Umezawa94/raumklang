#version 300 es
precision highp float;

in vec4 colorLeft;
in vec4 colorRight;

layout(location = 0) out vec4 leftChannel;
layout(location = 1) out vec4 rightChannel;

void main() {
  leftChannel = colorLeft;
  rightChannel = colorRight;
}
