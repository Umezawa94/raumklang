// attribute vec3 inputPosition;
// attribute vec2 inputTexCoord;
// attribute vec3 inputNormal;
//
// uniform mat4 projection, modelview, normalMat;
//
// varying vec3 normalInterp;
// varying vec3 vertPos;
//
// void main(){
//     gl_Position = projection * modelview * vec4(inputPosition, 1.0);
//     vec4 vertPos4 = modelview * vec4(inputPosition, 1.0);
//     vertPos = vec3(vertPos4) / vertPos4.w;
//     normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
// }

precision highp float;

attribute vec4 aVertexPosition;
// attribute vec2 aInputTexCoord;
attribute vec3 aInputNormal;
attribute vec2 aUV;
attribute vec4 aColor;

uniform mat4 uModelViewMatrix;
uniform mat3 uNormalMatrix;
uniform mat4 uProjectionMatrix;

uniform vec4 uSoundPosition;// = vec4(0.0,+10.0,-26.0,1.0);
// const vec4 uSoundPosition = vec4(0.0,+10.0,-26.0,1.0);

varying vec4 vPos;
// varying lowp vec4 vSoundPosition;
varying vec3 vNormal;
varying vec2 vUV;
varying vec4 vColor;


varying lowp float test;


void main() {
  vPos = uModelViewMatrix * vec4(aVertexPosition.xyz, 1.);
  gl_Position = uProjectionMatrix * vPos;
  // test = gl_Position.z/20.0;
  // gl_Position.z = - gl_Position.z / gl_Position.w;
  // gl_Position.z = 1.0 - gl_Position.z;

  // vPos = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
  vNormal =  uNormalMatrix * (aInputNormal);
  vUV = aUV;
  vColor = aColor;

  gl_PointSize = 4.0;

  // soundPosition = uProjectionMatrix * uModelViewMatrix * aVertexPosition;

  // vSoundPosition =  uProjectionMatrix * uSoundPosition;
  // soundPosition = uSoundPosition;
  // pos = pos / pos.x;
}
