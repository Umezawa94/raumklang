precision highp float;

varying float intensity;
uniform float uNormalize;

// uniform sampler2D uTexData;
varying vec4 color;

// uniform vec4 uColor;
void main() {
  // gl_FragColor = //vec4(uv, 0.0, 1.0);
  // gl_FragColor = texture2D(uTex, uv);
  gl_FragColor = color;
  // gl_FragColor = color;
  // gl_FragColor = vec4(intensity,0.0,0.0,1.0);
  // gl_FragColor = vec4(intensity * uNormalize,0.0,0.0,1.0);
}
