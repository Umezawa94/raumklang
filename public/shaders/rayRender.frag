#version 300 es

precision highp float;

in float intensity;
uniform float uNormalize;

// uniform sampler2D uTexData;
in vec4 color;

layout(location = 0) out vec4 oColor;

// uniform vec4 uColor;
void main() {
  // gl_FragColor = //vec4(uv, 0.0, 1.0);
  // gl_FragColor = texture2D(uTex, uv);
  oColor = color;
  // highp vec2 coord= (gl_PointCoord * 2.) - 1.;
  // float len = length(coord);
  // oColor *= (1.- len);
  // gl_FragColor = color;
  // gl_FragColor = vec4(intensity,0.0,0.0,1.0);
  // gl_FragColor = vec4(intensity * uNormalize,0.0,0.0,1.0);
}
