precision highp float;

varying float intensity;
uniform float uNormalize;

varying vec4 color;

void main() {
  // gl_FragColor = //vec4(uv, 0.0, 1.0);
  // gl_FragColor = texture2D(uTex, uv);
  // gl_FragColor = vec4(1.0,1.0,1.0,1.0);
  gl_FragColor = color;
  // gl_FragColor = vec4(intensity,0.0,0.0,1.0);
  // gl_FragColor = vec4(intensity * uNormalize,0.0,0.0,1.0);
}
