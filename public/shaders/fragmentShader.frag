// precision mediump float;
//
// varying vec3 normalInterp;
// varying vec3 vertPos;
//
// uniform int mode;
//
// const vec3 lightPos = vec3(1.0,1.0,1.0);
// const vec3 diffuseColor = vec3(0.5, 0.0, 0.0);
// const vec3 specColor = vec3(1.0, 1.0, 1.0);
//
// void main() {
//
//   vec3 normal = normalize(normalInterp);
//   vec3 lightDir = normalize(lightPos - vertPos);
//
//   float lambertian = max(dot(lightDir,normal), 0.0);
//   float specular = 0.0;
//
//   if(lambertian > 0.0) {
//
//     vec3 reflectDir = reflect(-lightDir, normal);
//     vec3 viewDir = normalize(-vertPos);
//
//     float specAngle = max(dot(reflectDir, viewDir), 0.0);
//     specular = pow(specAngle, 4.0);
//
//     // the exponent controls the shininess (try mode 2)
//     if(mode == 2)  specular = pow(specAngle, 16.0);
//
//     // according to the rendering equation we would need to multiply
//     // with the the "lambertian", but this has little visual effect
//     if(mode == 3) specular *= lambertian;
//
//     // switch to mode 4 to turn off the specular component
//     if(mode == 4) specular *= 0.0;
//
//   }
//
//   gl_FragColor = vec4( lambertian*diffuseColor +
//                         specular*specColor, 1.0);
// }



precision highp float;

uniform mat4 uProjectionMatrix;
uniform vec4 uSoundPosition;
uniform vec4 uCamera;
uniform int uDrawMode;

varying vec4 vPos;
varying vec3 vNormal;
varying vec2 vUV;
varying vec4 vColor;

varying lowp float test;

vec4 soundPosition;
vec3 origin;
// const vec3 diffuseColor = vec3(0.5, 0.0, 0.0);
// const vec3 specColor = vec3(1.0, 1.0, 1.0);

vec3 material = vec3(1.0, 0.5, 0.4);//lamb, phong, ambient








float dist() {
  float dist;
  dist = (distance(soundPosition, vPos));
  dist = dist + (distance(vPos.xyz, uCamera.xyz));
  return dist;
}

float intensity() {
  vec3 normal = -normalize(vNormal);
  vec3 lightDir = soundPosition.xyz - vPos.xyz;
  if(length(lightDir) <= 0.01)
    return 10.0;
  lightDir = normalize(lightDir);

  // vec3 lightDir = soundPosition.xyz - vPos.xyz;
  // lightDir = (lightDir/2.0)+0.5;
  // float lambertian = max(dot(lightDir,normal), 0.0);
  float lambertian = max(dot(lightDir,normal), 0.0);
  if (lambertian < 0.0){
    lambertian = -1.0 * lambertian;
    normal = -1.0 * normal;
  }
  // float lambertian = dot(lightDir,-normal)/2.0+0.5;


  // float invDist = 1.0/(length(lightDir)/20.0);


  // return vec3(lambertian, 0.0, 0.0);
  // return lightDir;
    float specular = 0.0;

    if(lambertian > 0.0) {
  //
      vec3 reflectDir = reflect(lightDir, normal);
      vec3 viewDir = normalize(uCamera.xyz - vPos.xyz);
  //
      float specAngle = max(dot(reflectDir.xyz, viewDir.xyz), 0.0);
      specular = pow(specAngle, 4.0);
  //
  //     // the exponent controls the shininess (try mode 2)
  //     if(mode == 2)  specular = pow(specAngle, 16.0);
  //
  //     // according to the rendering equation we would need to multiply
  //     // with the the "lambertian", but this has little visual effect
  //     if(mode == 3) specular *= lambertian;
  //
  //     // switch to mode 4 to turn off the specular component
  //     if(mode == 4) specular *= 0.0;
  //
    }
    float result = dot(vec3(lambertian, specular, 1.0), material);
  return result;







}

void main() {
  soundPosition = uSoundPosition;
  // origin = -(uProjectionMatrix * vec4(0.0,0.0,0.0,1.0)).xyz;
  // origin.z = -1.0 * origin.z;
  // origin = vCamera.xyz;

  // float bla = length(origin) / 10.0;
  // origin = vec3(0.0,0.0,0.0);


  float fDist = dist();
  // fDist = distance(vPos.xyz, origin)/10.0;
  float invDist = 1.0/(1.0+fDist/80.0);
  // invDist = 1.0/(fDist);
  // gl_FragColor = vec4(0.0, intensity(), 0.0, 1.0);
  // gl_FragColor = vec4(intensity(), 1.0);

  // vec2 UV = vUV;
  // bool isLine = false;
  // if(UV.x*UV.x < 0.0001) isLine = true;
  // if(UV.y*UV.y < 0.0001) isLine = true;
  // UV -= 1.0;
  // if(UV.x*UV.x < 0.0001) isLine = true;
  // if(UV.y*UV.y < 0.0001) isLine = true;

  float mult = 1.0;
  // if(isLine == false){
  mult = invDist * intensity();
  // } 
  gl_FragColor = vColor * mult;
  // gl_FragColor.b = bla;
  // gl_FragColor = vec4(0.0, 0.0, test, 1.0);


  // gl_FragColor = normal;
}
