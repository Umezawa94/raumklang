class Input {
  constructor(element) {
    this.element = element;

    this.keymap = {};

    this.actions = {};

    window.addEventListener('keydown', (e)=>{
      if(document.activeElement.type == "text")
        return false;
      var code = e.keyCode || e.which;
      let bind = this.getKeybind(code);
      bind.onDown();
    });
    window.addEventListener('keyup', (e)=>{
      var code = e.keyCode || e.which;
      let bind = this.getKeybind(code);
      bind.onUp();
    });

    
    this.onDrag = null;
    this.lastX = null;
    this.lastY = null;
    var mouseDown = false;
    $(element).on('mousedown touchstart', (e)=>{
      e.preventDefault();
      mouseDown = true;
    });
    $(element).on('mousemove touchmove', (e)=>{
      e.preventDefault();
      e.deltaX = e.clientX - this.lastX;
      e.deltaY = e.clientY - this.lastY;
      this.lastX = e.clientX;
      this.lastY = e.clientY;
      if(e.buttons > 0 && this.onDrag)
        this.onDrag(e);
    });
    $(window.document).on('mouseup touchend', (e)=>{
      // Capture this event anywhere in the document, since the mouse may leave our element while mouse is down and then the 'up' event will not fire within the element.
      mouseDown = false;
    });

  }

  addAction(name){
    return this.actions[name] = new Action(name);
  }
  addKeybind(code, actionName){
    let action = this.actions[actionName];
    if(action === undefined) throw 'Action not found!';
    let bind = this.getKeybind(code);
    bind.actions.push(action);
    action.keys.push(bind);
    return bind;
  }
  getKeybind(code){
    return this.keymap[code] = this.keymap[code] || new Keybind(code);
  }
  isKeyDown(code){
    return this.getKeybind(code).down;
  }
  isActionDown(name){
    return this.actions[name].down;
  }
}

class Action {
  constructor(name){
    this.name = name;
    this.keys = [];
    this.down = false;
    this.onPress = null;
  }
  keyDown(){
    if(this.down) return;
    this.onDown();
  }
  onDown(){
    this.down = true;
    if(this.onPress)
    this.onPress();
  }
  keyUp(){
    for (let i = 0; i < this.keys.length; i++) {
      if(this.keys[i].down) return;
    }
    this.onUp();
  }
  onUp(){
    this.down = false;
  }
}

class Keybind {
  constructor(code){
    this.code = code;
    this.actions = [];
    this.down = false;
  }
  onDown(){
    this.down = true;
    for (let i = 0; i < this.actions.length; i++) {
      const action = this.actions[i];
      action.keyDown();
    }
  }
  onUp(){
    this.down = false;
    for (let i = 0; i < this.actions.length; i++) {
      const action = this.actions[i];
      action.keyUp();
    }
  }
}

