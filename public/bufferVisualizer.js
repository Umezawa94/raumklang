class BufferVisualizer {
  constructor(canvas, accurate) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');
    this.frameRate = 2;
    this.lastFrame = Date.now();
    this.accurate = accurate;
  }
  visualize(bufferL, bufferR, color, noclear, accurate){
    if (accurate === undefined){
      accurate = this.accurate;
    }
    let time = Date.now();
    if(time - this.lastFrame < 1000/this.frameRate){
      return;
    }
    this.lastFrame = time;

    let ctx = this.ctx;
    let w = this.canvas.width, h = this.canvas.height

    if (!noclear)
      ctx.clearRect(0, 0, w, h);

    let topCenter = 1/2;
    let bottomCenter = 1/2;
    let width = 1/2;

    ctx.beginPath();
    ctx.moveTo(0,h*topCenter);
    if(accurate){

      let l = bufferL.length;
      for (var i = 0; i < l; i++) {
        // let value = bufferL[Math.floor((x/l) * bufferL.length)];
        let value = Math.max(-1, Math.min(bufferL[i], 1));
        let x = w * i / (l-1) ;
        let y = h*topCenter - value*h*width;
        ctx.lineTo(x,y);
      }
      ctx.lineTo(w,h*topCenter);
      ctx.moveTo(0,h*bottomCenter);
      l = bufferR.length;
      for (var i = 0; i < l; i++) {
        // let value = bufferR[Math.floor((x/l) * bufferR.length)];
        let value = Math.max(-1, Math.min(bufferR[i], 1));
        let x = w * i / (l-1) ;
        let y = h*bottomCenter + value*h * width;
        ctx.lineTo(x,y);
      }
      ctx.lineTo(w,h*bottomCenter);
    } else {

      let l = w;
      for (var i = 0; i < l; i++) {
        let value = bufferL[Math.floor((i/l) * bufferL.length)];
        value = Math.max(-1, Math.min(value, 1));
        let x = i;
        let y = h*topCenter - value*h * width;
        ctx.lineTo(x,y);
      }
      ctx.lineTo(w,h*topCenter);
      ctx.moveTo(0,h*bottomCenter);
      l = w;
      for (var i = 0; i < l; i++) {
        let value = bufferR[Math.floor((i/l) * bufferR.length)];
        value = Math.max(-1, Math.min(value, 1));
        let x = i;
        let y = h*bottomCenter + value*h * width;
        ctx.lineTo(x,y);
      }
    }
    ctx.lineTo(w,h*bottomCenter);
    ctx.strokeStyle = "white";
    if(color)
      ctx.strokeStyle = color;
    ctx.stroke();
  }
}
