class DebugInfo{
    constructor(glStuff){
        this.div = $('#frameCounter')[0];
        this.refreshTime = 0.2;
        this.refreshTimer = 0;

        this.input = glStuff.input;
    }
    frame(delta){



        this.refreshTimer -= delta;
        if(this.refreshTimer<=0){
            this.refreshTimer = this.refreshTime;

            let framerate = Math.floor(1/delta);

            let keysDown = [];
            for (const i in this.input.keymap) {
                if (this.input.keymap.hasOwnProperty(i)) {
                    const key = this.input.keymap[i];
                    if(key.down) keysDown.push(key.code);
                }
            }
            
            let actionsDown = [];
            for (const i in this.input.actions) {
                if (this.input.actions.hasOwnProperty(i)) {
                    const action = this.input.actions[i];
                    if(action.down) actionsDown.push(action.name);
                }
            }

          this.div.innerHTML = `${framerate}<br/>${keysDown}<br/>${actionsDown}`;
        }
    }
}