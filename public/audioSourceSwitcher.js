class AudioSourceSwitcher {
    constructor(ctx, start){
        this.ctx = ctx;
        this.output = ctx.createGain();

        this.sources = [];
        this.currentSource;

        this.switchTo(start || 0);
    }
    async switchTo(index){
        if (!this.sources[index]){
            switch (index) {
                case 0:
                    this.sources[index] = this.initPianoSource();
                    break;
                
                case 1:
                    this.sources[index] = this.initMicSource();
                    break;
                    
                case 2:
                this.sources[index] = this.initSineSource();
                break;
            }
        }
        let newSource = await this.sources[index];
        if (this.currentSource)
            this.currentSource.disconnect(this.output);
        newSource.connect(this.output);
        this.currentSource = newSource;
    }
    async initPianoSource(){
        const ctx = this.ctx;
        return ctx.createMediaElementSource(document.getElementById('pianoloop'));
    }
    async initMicSource(){
        const ctx = this.ctx;  
        let stream = await navigator.mediaDevices.getUserMedia({audio:true});
        return ctx.createMediaStreamSource(stream);
    }
    async initSineSource(){
        const ctx = this.ctx;  
        let osc = ctx.createOscillator();
        osc.frequency.value = 440;
        osc.start();
        return osc;
    }

}