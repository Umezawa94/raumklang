class TexProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/fragmentDisplay.frag', 'shaders/vertexDisplay.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
      modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
      tex: gl.getUniformLocation(program, 'uTex'),
    }
  }
}
