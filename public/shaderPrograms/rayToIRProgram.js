class RayToIRProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/rayToIR.frag', 'shaders/rayToIR.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
      normalize: gl.getUniformLocation(program, 'uNormalize'),
      texPos: gl.getUniformLocation(program, 'uTexPos'),
      texDir: gl.getUniformLocation(program, 'uTexDir'),
      texData: gl.getUniformLocation(program, 'uTexData'),
      cameraDir: gl.getUniformLocation(program, 'uCameraDir'),
      speedOfSound: gl.getUniformLocation(program, 'uSpeedOfSound'),
    }
  }
}
