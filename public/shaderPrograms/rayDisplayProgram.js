class RayDisplayProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/rayDisplay.frag', 'shaders/rayDisplay.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      // normal: gl.getAttribLocation(program, 'aInputNormal'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
        projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
        texPos: gl.getUniformLocation(program, 'uTexPos'),
        texDir: gl.getUniformLocation(program, 'uTexDir'),
        texData: gl.getUniformLocation(program, 'uTexData'),
        color: gl.getUniformLocation(program, 'uColor'),
    }
  }
}
