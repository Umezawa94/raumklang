class ShaderProgram {
  constructor(gl, vertex, fragment){
    this.gl = gl;
    var files = Promise.all([
      $.get(vertex, null, 'text'),
      $.get(fragment, null, 'text')
    ]);

    this.ready = files.then(([fragmentShader, vertexShader])=>{
      vertexShader = this.loadShader(gl.VERTEX_SHADER, vertexShader);
      fragmentShader = this.loadShader(gl.FRAGMENT_SHADER, fragmentShader);

      let shaderProgram = gl.createProgram();
      gl.attachShader(shaderProgram, vertexShader);
      gl.attachShader(shaderProgram, fragmentShader);
      gl.linkProgram(shaderProgram);

      // If creating the shader program failed, alert
      if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return Promise.reject();
      }

      this.program = shaderProgram;
      this.getLocations(gl, shaderProgram);
      this.isReady = true;
      return this;
    });
    this.ready.then();
  }

  loadShader(type, source) {
    let gl = this.gl;

    const shader = gl.createShader(type);
    // Send the source to the shader object
    gl.shaderSource(shader, source);
    // Compile the shader program
    gl.compileShader(shader);
    // See if it compiled successfully
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
    }
    return shader;
  }
  getLocations(){
  }
}
ShaderProgram.prototype.isShaderProgram = true;
