class SoundProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/fragmentShader.frag', 'shaders/vertexShader.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      normal: gl.getAttribLocation(program, 'aInputNormal'),
      uv: gl.getAttribLocation(program, 'aUV'),
      color: gl.getAttribLocation(program, 'aColor'),
    };
    this.uniformLocations = {
        projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
        modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
        normalMatrix: gl.getUniformLocation(program, 'uNormalMatrix'),
        soundPosition: gl.getUniformLocation(program, 'uSoundPosition'),
        cameraPosition: gl.getUniformLocation(program, 'uCamera'),
        drawMode: gl.getUniformLocation(program, 'uDrawMode'),
    }
  }
}
