class RandomProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/fragmentShader.frag', 'shaders/vertexShader.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      uv: gl.getAttribLocation(program, 'aUv'),
    };
    this.uniformLocations = {
        seed: gl.getUniformLocation(program, 'uSeed'),
    }
  }
}
