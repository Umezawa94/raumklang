class RayToCameraProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/rayToCamera.frag', 'shaders/rayToCamera.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      // normal: gl.getAttribLocation(program, 'aInputNormal'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
        cameraPosition: gl.getUniformLocation(program, 'uCamera'),

        texPos: gl.getUniformLocation(program, 'uTexPos'),
        texDir: gl.getUniformLocation(program, 'uTexDir'),
        texData: gl.getUniformLocation(program, 'uTexData'),
        texNorm: gl.getUniformLocation(program, 'uTexNorm'),
        texMat: gl.getUniformLocation(program, 'uTexMat'),
        rayWidth: gl.getUniformLocation(program, 'uRayWidth'),
        rayHeight: gl.getUniformLocation(program, 'uRayHeight'),
        rayCount: gl.getUniformLocation(program, 'uRayCount'),


        texVert: gl.getUniformLocation(program, 'uTexVert'),
        texVertNorm: gl.getUniformLocation(program, 'uTexVertNorm'),
        texVertMat: gl.getUniformLocation(program, 'uTexVertMat'),
        texVertInd: gl.getUniformLocation(program, 'uTexVertInd'),
        vertWidth: gl.getUniformLocation(program, 'uVertWidth'),
        vertHeight: gl.getUniformLocation(program, 'uVertHeight'),
        vertCount: gl.getUniformLocation(program, 'uVertCount'),
    }
  }
}
