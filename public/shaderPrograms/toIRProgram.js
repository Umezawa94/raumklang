class ToIRProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/toIR.frag', 'shaders/toIR.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
      normalize: gl.getUniformLocation(program, 'uNormalize'),
      tex: gl.getUniformLocation(program, 'uTex'),
    }
  }
}
