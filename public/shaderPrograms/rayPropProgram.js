class RayPropProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/rayProp.frag', 'shaders/rayProp.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      // normal: gl.getAttribLocation(program, 'aInputNormal'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
        texPos: gl.getUniformLocation(program, 'uTexPos'),
        texDir: gl.getUniformLocation(program, 'uTexDir'),
        texData: gl.getUniformLocation(program, 'uTexData'),
        texNorm: gl.getUniformLocation(program, 'uTexNorm'),
        texMat: gl.getUniformLocation(program, 'uTexMat'),
        rayWidth: gl.getUniformLocation(program, 'uRayWidth'),
        rayHeight: gl.getUniformLocation(program, 'uRayHeight'),
        newRayCount: gl.getUniformLocation(program, 'uNewRayCount'),
        handledRayCount: gl.getUniformLocation(program, 'uHandledRayCount'),
        rayMultiplier: gl.getUniformLocation(program, 'uRayMultiplier'),


        texVert: gl.getUniformLocation(program, 'uTexVert'),
        texVertNorm: gl.getUniformLocation(program, 'uTexVertNorm'),
        texVertMat: gl.getUniformLocation(program, 'uTexVertMat'),
        texVertInd: gl.getUniformLocation(program, 'uTexVertInd'),
        vertWidth: gl.getUniformLocation(program, 'uVertWidth'),
        vertHeight: gl.getUniformLocation(program, 'uVertHeight'),
        vertCount: gl.getUniformLocation(program, 'uVertCount'),
    }
  }
}
