class LineProgram extends ShaderProgram {
    constructor(gl) {
      super(gl, 'shaders/lineShader.frag', 'shaders/lineShader.vert');
    }
    getLocations(gl, program){
      // const shaderProgram = this.shaderProgram;
      this.attribLocations = {
        vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
        color: gl.getAttribLocation(program, 'aColor'),
      };
      this.uniformLocations = {
          projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
          modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
      }
    }
  }
  