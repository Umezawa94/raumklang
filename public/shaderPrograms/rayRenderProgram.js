class RayRenderProgram extends ShaderProgram {
  constructor(gl) {
    super(gl, 'shaders/rayRender.frag', 'shaders/rayRender.vert');
  }
  getLocations(gl, program){
    // const shaderProgram = this.shaderProgram;
    this.attribLocations = {
      // vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      // normal: gl.getAttribLocation(program, 'aInputNormal'),
      uv: gl.getAttribLocation(program, 'aUVPosition'),
    };
    this.uniformLocations = {
        projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
        normalMatrix: gl.getUniformLocation(program, 'uNormalMatrix'),
        texPos: gl.getUniformLocation(program, 'uTexPos'),
        texDir: gl.getUniformLocation(program, 'uTexDir'),
        texData: gl.getUniformLocation(program, 'uTexData'),
        color: gl.getUniformLocation(program, 'uColor'),
    }
  }
}
