class TexToIR {
  constructor(gl, program, inputResX, inputResY) {
    this.gl = gl;
    this.fb = new FrameBuffer(gl, 4096, 2);
    this.vertexBuffer = gl.createBuffer();
    this.inputResX = inputResX;
    this.inputResY = inputResY;
    this.createUVBuffer();
    this.normalizeFactor = 20;
    this.ready = false;
    this.programInfo = program;
    this.programInfo.ready.then(()=>{
      this.ready = true;
    });
  }
  createUVBuffer(){
    const gl = this.gl;
    let arr = new Float32Array(this.inputResX * this.inputResY * 2);
    for (var x = 0; x < this.inputResX; x++) {
      for (var y = 0; y < this.inputResY; y++) {
        let ind = (x + y * this.inputResX) * 2 ;
        arr[ind] = x / (this.inputResX);
        arr[ind + 1] = y / (this.inputResY);
      }
    }
    this.uvBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, arr, gl.STATIC_DRAW);
  }

  render(texture, clear){
    if(!this.ready) return;
    const gl = this.gl;
    const programInfo = this.programInfo;

    this.fb.bind();

    // No depth testing
    gl.disable(gl.DEPTH_TEST);

    // Enable blending
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE);


    // let clear = true;
    if (clear){
      gl.clearColor(0.0, 0.0, 0.0, 0.0);  // Clear to black, fully opaque
      // gl.clearDepth(1.0);                 // Clear everything
      gl.disable(gl.DEPTH_TEST);           // Enable depth testing
      // gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

      // Clear the canvas before we start drawing on it.

      gl.clear(gl.COLOR_BUFFER_BIT);
    }


    // Tell WebGL to use our program when drawing
    gl.useProgram(programInfo.program);
    gl.uniform1f(programInfo.uniformLocations.normalize, this.normalizeFactor / (this.inputResX * this.inputResY));

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(programInfo.uniformLocations.tex, 0);

    {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
      // gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

     // Tell WebGL how to pull out the positions from the position
     // buffer into the vertexPosition attribute.

       const numComponents = 2;  // pull out 2 values per iteration
       const type = gl.FLOAT;    // the data in the buffer is 32bit floats
       const normalize = false;  // don't normalize
       const stride = 0;         // how many bytes to get from one set of values to the next
                                 // 0 = use type and numComponents above
       const offset = 0;         // how many bytes inside the buffer to start from
      //  gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
       gl.vertexAttribPointer(
           programInfo.attribLocations.uv,
           numComponents,
           type,
           normalize,
           stride,
           offset);
       gl.enableVertexAttribArray(
           programInfo.attribLocations.uv);
     }

     // Set the shader uniforms

     {
       const offset = 0;
       const vertexCount = this.inputResX * this.inputResY;
      //  gl.drawArrays(this.drawMode, offset, vertexCount);
       gl.drawArrays(gl.POINTS, offset, vertexCount);

     }

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }

  getData(){
    return this.fb.getData();
  }
}
