let qs = (function(a) {
  if (a == "") return {};
  var b = {};
  for (var i = 0; i < a.length; ++i)
  {
      var p=a[i].split('=', 2);
      if (p.length == 1)
          b[p[0]] = "";
      else
          b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
  }
  return b;
})(window.location.search.substr(1).split('&'));

$(document).ready(function() {

  for (const key in qs) {
    if (qs.hasOwnProperty(key)) {
      let value = qs[key];
      $(`[name="${key}"]`).val(value);
    }
  }
  $("#toolbox form [type='checkbox']").each((a,b)=>{
    b.checked = qs[b.name] === "on";
  })

  let AudioContext = window.AudioContext || window.webkitAudioContext;
  let ctx = new AudioContext();
  window.audioContext = ctx;
  

  let convOptions = {
    irLength: (qs.i_audio_bufferlength || 32768)*1.0,
    bufferLength: 1024,
    dspBufLength: 512,
  }
  let convolution = new RaumklangNode(ctx, (qs.i_audio_mute)==="on", convOptions);
  window.convolution = convolution;
  convolution.output.connect(ctx.destination);

  if((qs.i_view_displayir)==="on")
    convolution.visualizer = new BufferVisualizer(document.getElementById('graph'), true);

  let glOptions = {    
    irLength: (qs.i_audio_bufferlength || 32768)*1.0,
    rayCount: (qs.i_raytracing_count || 100)*1.0,
    bounceCount: (qs.i_raytracing_bounces || 10)*1.0,
    rayBufferWidth: (qs.i_raytracing_raytexwidth ||2048)*1.0,
    rayBufferHeight: (qs.i_raytracing_raytexheight || 2048)*1.0,
    speedOfSound: (qs.i_raytracing_speed || 340)*1.0,
    viewOnAudio: (qs.i_view_onaudio)==="on",
    displayRays: (qs.i_view_displayrays)==="on",
    orthographic: (qs.i_view_orthographic)==="on",
    rayMultiplier: (qs.i_raytracing_raymultiplier || 2)*1.0,
  }

  let glStuff = new GLStuff(glOptions);
  window.glStuff = glStuff;
  glStuff.ready.then(()=>{
    window.gl = glStuff.gl;
    glStuff.start();

  });

  let source = new AudioSourceSwitcher(ctx, (qs.i_audio_source || 0)*1.0);
  window.audioSource = source;
  source.output.connect(convolution.input);

  $(".i_loc").change((e)=>{
    glStuff.updateFromNumberBoxes();
  });
  
});
