%========================================================================================
% TU Dortmund, Informatik Lehrstuhl VII
%========================================================================================

\chapter{Raytracing}
\label{Raytracing}
%
In Kapitel \ref{geometrische_raumakustik} wurde behandelt, wie Strahlen zur Modellierung von Schallquellen genutzt werden können. Dazu werden Reflexionen an Oberflächen im Raum berechnet. Um diese Reflexionen durchführen zu können, müssen allerdings zunächst die Punkte ermittelt werden, an denen diese Reflexionen auftreten. Dazu soll in diesem Kapitel Raytracing vorgestellt werden, eine Methode, die es erlaubt, die Schnittpunkte zwischen einem Strahl und der Umgebung zu finden. In Abschnitt \ref{raytracing_basics} wird dazu zunächst auf das Grundprinzip des Raytracings eingegangen. Nachfolgend wird in Abschnitt \ref{schnitttests} dann konkret auf Schnitttests zwischen einzelnen Umgebungsobjekten und einem Strahl eingegangen.

\section{Raytracing zur Simulation strahlenförmiger Ausbreitung im Raum}
\label{raytracing_basics}
%
Raytracing ist ein Verfahren, bei dem Strahlen im zu untersuchenden Raum im Hinblick auf Schnitte mit der Raumgeometrie betrachtet werden. Dieses Verfahren wird in verschiedenen Anwendungsgebieten genutzt; insbesondere in der Computergrafik wird es zum Beispiel bei Beleuchtungsberechnungen oder bei der Auswahl von auf einem Bildschirm abgebildeten Objekten mithilfe von \enquote{Ray Picking} genutzt. Da Licht, ähnlich wie Schall, ein Wellenphänomen darstellt, liegt es nahe, Raytracing analog zur Beleuchtungsberechnung auch für Klangausbreitung zu verwenden. Im Abschnitt \ref{geometrische_raumakustik} wurde bereits darauf eingegangen, wie Strahlen für Raumakustik verwendet werden können. In diesem Abschnitt soll erläutert werden, wie Raytracing im Allgemeinen angewendet wird.\\

Raytracing basiert auf der Verfolgung eines Strahls
	\begin{align}
		\label{raytracing_strahl}
		\mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, u \in \mathbb{R} > 0
	\end{align}
mit Anfangspunkt $\mathbf{a} \in \mathbb{R}^3$ und Richtungsvektor $\mathbf{d} \in \mathbb{R}^3$ \cite{ericson2004real}. 

    \begin{figure}
    \centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/Raypick.pdf_tex}}
    \caption[Strahl kollidiert mit Geometrie]{Im Raytracing werden Strahlen verfolgt und auf Kollision mit Szenenobjekten untersucht. Dadurch lassen sich die geschnittenen Objekte sowie die Schnittpunkte ermitteln.}
        \label{raytracing_raypick}
    \end{figure}
Dieser Strahl soll auf Schnitte mit Objekten in der Umgebung untersucht werden. Die Umgebung wird durch eine Szene $\mathcal{Z}$ repräsentiert, die eine Menge von $N$ Objekten darstellt:
	\begin{align}
		\label{raytracing_scene}
		\mathcal{Z} = \{\mathcal{O}_1, \mathcal{O}_2, ..., \mathcal{O}_N\}.
	\end{align}
Diese Objekte $\mathcal{O}_n$ können beliebige Oberflächen repräsentieren, jedoch werden Objekte zur Vereinfachung der Schnitttests (siehe Abschnitt \ref{schnitttests}) in der Regel durch Primitiven wie Dreiecke oder Kugeln dargestellt. Insbesondere Dreiecksnetze werden in der Computergrafik oft genutzt, um Objekte zu modellieren. Im Rahmen dieser Arbeit wird davon ausgegangen, dass Szenen ausschließlich aus Dreiecksprimitiven aufgebaut sind. Unabhängig von der Form der Oberfläche besteht das Objekt aus einer Menge von Oberflächenpunkten $\mathbf{o} \in \mathbb{R}^3$.\\

Die Menge der Schnittpunkte $\mathcal{S}_n$ zwischen dem Strahl und einem Szenenobjekt ergibt sich aus dem Schnitt der Menge der Punkte auf dem Strahl
	\begin{align}
		\label{raytracing_points_on_ray}
		\mathcal{R} = \{\mathbf{r}(u) | \mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, u \in \mathbb{R} > 0\}
	\end{align}
und der Menge der Punkte des Objekts $O_n$:
	\begin{align}
		\label{raytracing_points_hit}
		\mathcal{S}_n &= \{\mathbf{r}(u) | \mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, u \in \mathbb{R} > 0\} \cap O_n \notag\\
		&= \{\mathbf{r}(u) | \mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, u \in \mathbb{R} > 0,  \mathbf{r}(u) \in O_n\}.
	\end{align}
Wie dieser Schnitt berechnet werden kann, wird in Abschnitt \ref{schnitttests} näher betrachtet. Ein Objekt $O_n$ gilt dann als geschnitten, wenn $\mathcal{S}_n  \neq \emptyset$.\\
Um alle Schnittpunkte $\mathcal{S}_\mathcal{Z}$ einer Szene zu erhalten, lassen sich die Schnittmengen mit den Objekten der Szene vereinigen: 

	\begin{align}
		\label{raytracing_vereinigung}
		\mathcal{S}_\mathcal{Z} = \bigcup_{n=1}^{N}\mathcal{S}_n.
	\end{align}
In vielen Anwendungsbereichen, insbesondere auch bei der Berechnung der Schallausbreitung, werden nicht alle Schnittpunkte der Szene benötigt, sondern nur der erste Schnittpunkt, auf den der Strahl trifft, da die Kollision an diesem Punkt Auswirkungen auf den Strahl hat, wie etwa Absorption oder Ablenkung des Strahls. Gesucht wird also der Schnittpunkt $s(u_\mathit{min})$ mit dem kleinsten Parameter $u_\mathit{min}$:
	\begin{align}
		\label{raytracing_first_u}
		u_\mathit{min} = min\{u | \mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, \mathbf{r}(u)\in \mathcal{S}_\mathcal{Z}\}.
	\end{align}
Dieses Minimum lässt sich effizient bestimmen, indem bei der Überprüfung von Objekten auf Schnittpunkte ein Punkt $s(u)$ nur akzeptiert wird, wenn der Parameter $u$ kleiner als der Parameter $u_\mathit{min}$ des zuletzt akzeptierten Punktes ist (oder der erste Schnittpunkt gefunden wurde).


\section{Schnitttests}
\label{schnitttests}
Zur Durchführung von Raytracing ist es essenziell, die Raumgeometrie auf Schnitte mit einem beliebigen Strahl zu untersuchen und die Schnittpunkte bestimmen zu können. In Gleichung \ref{raytracing_points_hit} wurde vorausgesetzt, dass ein Schnitt zwischen einem Objekt $\mathcal{O}_n$ und einem Strahl $\mathcal{R}$ berechnet werden kann. In diesem Kapitel soll darauf eingegangen werden, wie eine solche Berechnung durchgeführt werden kann.\\

Wie im letzten Kapitel erwähnt soll davon ausgegangen werden, dass alle Szenen\-objekte Dreiecke sind. Um die Schnittpunkte zu berechnen, muss zunächst eine geeignete Darstellung für ein Dreieck gewählt werden. Ein Dreieck kann eindeutig durch seine Eckpunkte $\mathbf{v}_0$, $\mathbf{v}_1$ und $\mathbf{v}_2$ bestimmt werden. Ein beliebiger Punkt $\mathbf{v}(\mu_0, \mu_1, \mu_2)$ auf der Ebene, die durch das Dreieck aufgespannt wird, kann als eine Linearkombination der Eckpunkte mit den Koeffizienten $\mu_0$, $\mu_1$ und $\mu_2$ erreicht werden:
	\begin{align}
		\label{schnitttests_linearkombination}
		\mathbf{v}(\mu_0, \mu_1, \mu_2) = \mu_0\mathbf{v}_0 + \mu_1\mathbf{v}_1 + \mu_2\mathbf{v}_2, mu_0 + \mu_1 + \mu_2 = 1.
	\end{align}
	
	\begin{figure}
		\centering
		
    	\subfigure[]{
    		\def\svgwidth{0.6\columnwidth}
	    	\resizebox{0.45\textwidth}{!}{\input{graphs/Baryzentrisch.pdf_tex}}
    		\label{fig_Baryzentrisch}
    	}
    	%\hspace{0.5cm}%
    	\subfigure[]{
    		\def\svgwidth{0.6\columnwidth}
	    	\resizebox{0.45\textwidth}{!}{\input{graphs/BaryzentrischModifiziert.pdf_tex}}
    		\label{fig_BaryzentrischModifiziert}
    	}
		\caption[Baryzentrisches Koordinatensystem]{In Abbildung (a) wird gezeigt, wie im baryzentrischen Koordinatensystem ein Punkt auf dem Dreieck durch die Affinkombination der Ortsvektoren dargestellt werden kann. Abbildung (b) visualisiert die Umformung aus Gleichung \ref{schnitttests_baryzentrisch_umformung}, durch die es möglich ist, den gleichen Punkt mit nur zwei Koeffizienten zu erreichen, da der dritte Koeffizient implizit durch die anderen Koeffizienten bestimmt ist.}
		\label{schnitttests_baryzentrisch}
	\end{figure}
	
Dieser Spezialfall der Linearkombination, bei dem die Summe der Koeffizienten 1 ergibt, wird auch als affine Kombination bezeichnet. Wird ein Punkt $\mathbf{v}(\mu_0, \mu_1, \mu_2)$ in dieser Form als eine affine Kombination dargestellt, dann werden die Koeffizienten $\mu_0$, $\mu_1$ und $\mu_2$ als baryzentrische Koordinaten des Punktes bezeichnet. Eine wichtige Eigenschaft des baryzentrischen Koordinatensystems ist, dass ein Punkt genau dann auf dem durch die Eckpunkte aufgespannten Dreieck liegt, wenn $\mu_0, \mu_1, \mu_2 \geq 0$. Unter diesen Voraussetzungen kann die Menge der Punkte eines Objekts $\mathcal{O}_n$, das ein Dreieck mit den Eckpunkten $\mathbf{v}_0$, $\mathbf{v}_1$ und $\mathbf{v}_2$ darstellt, beschrieben werden als:

	\begin{align}
		\label{schnitttests_punkte}
		\mathcal{O}_n = \{\mathbf{v}(\mu_0, \mu_1, \mu_2) \:|\: & \mathbf{v}(\mu_0, \mu_1, \mu_2) = \mu_0\mathbf{v}_0 + \mu_1\mathbf{v}_1 + \mu_2\mathbf{v}_2, \nonumber \\
		& \mu_0 + \mu_1 + \mu_2 = 1, \nonumber \\
		& \mu_0,\mu_1,\mu_2 \geq 0 \}.
	\end{align}
Die Bedingung $\mu_0 + \mu_1 + \mu_2 = 1$ kann nun umgestellt werden nach
	\begin{align*}
		\mu_0 = 1 - \mu_1 - \mu_2,
	\end{align*}
was erlaubt, $\mu_0$ aus der affinen Kombination zu ersetzen:
	\begin{align}
		\label{schnitttests_baryzentrisch_umformung}
		\mathbf{v}(\mu_1, \mu_2) &= (1 - (\mu_1 + \mu_2)\mathbf{v}_0 + \mu_1\mathbf{v}_1 + \mu_2\mathbf{v}_2 \notag\\
		\mathbf{v}(\mu_1, \mu_2) &= \mathbf{v}_0 - \mu_1\mathbf{v}_0 - \mu_2\mathbf{v}_0 + \mu_1\mathbf{v}_1 + \mu_2\mathbf{v}_2 \notag\\
		\mathbf{v}(\mu_1, \mu_2) &= \mathbf{v}_0 + \mu_1(\mathbf{v}_1\mathbf{v}_0) + \mu_2(\mathbf{v}_2 - \mathbf{v}_0).
	\end{align}
Auch in der Bedingung $\mu_0\geq 0$ lässt sich die Variable ersetzen, wodurch $\mu_0$ vollständig aus dem Gleichungssystem eliminiert werden kann:
	
	\begin{align}
		\label{schnitttests_umformung_mu12}
		1 - (\mu_1 + \mu_2) \geq 0 \notag\\
		1 \geq (\mu_1 + \mu_2).
	\end{align}
	
Um den Schnittpunkt zu erhalten, kann nun die Gleichung \ref{schnitttests_baryzentrisch_umformung} mit der Strahlengleichung \ref{raytracing_strahl} gleichgesetzt werden:
		
	\begin{align*}
		\mathbf{a} + u \cdot \mathbf{d} = \mathbf{v}_0 + \mu_1(\mathbf{v}_1-\mathbf{v}_0) + \mu_2(\mathbf{v}_2 - \mathbf{v}_0).
	\end{align*}

Durch Ersetzen von $\mathbf{e}_1 = \mathbf{v}_1-\mathbf{v}_0$, $\mathbf{e}_2 = \mathbf{v}_2-\mathbf{v}_0$ und $\mathbf{t} = \mathbf{a}-\mathbf{v}_0$ lässt sich die Gleichung als Matrixmultiplikation notieren:

	\begin{align*}
		\mathbf{t} = - u \mathbf{d} + \mu_1\mathbf{e}_1 + \mu_2\mathbf{e}_2 \\
		\begin{bmatrix}-\mathbf{d}& \mathbf{e}_1& \mathbf{e}_2\end{bmatrix}\begin{bmatrix}u \\ \mu_1 \\ \mu_2\end{bmatrix} = \mathbf{t}.
	\end{align*}
	
Durch Anwendung der Cramerschen Regel können die Koeffizienten $u$, $\mu_1$ und $\mu_2$
berechnet werden durch\cite{moller2005fast}: 

	\begin{align*}
		\begin{bmatrix}u \\ \mu_1 \\ \mu_2\end{bmatrix} = 
		\frac{1}{\begin{vmatrix}-\mathbf{d}& \mathbf{e}_1& \mathbf{e}_2\end{vmatrix}}	
		\begin{bmatrix}
			\begin{vmatrix}\,\ \mathbf{t}& \mathbf{e}_1& \mathbf{e}_2\end{vmatrix} \\[6pt]
			\begin{vmatrix}-\mathbf{d}& \mathbf{t}  & \mathbf{e}_2\end{vmatrix} \\[6pt]
			\begin{vmatrix}-\mathbf{d}& \mathbf{e}_1& \mathbf{t}  \end{vmatrix}
		\end{bmatrix}	
	\end{align*}
wobei $\begin{vmatrix}\mathbf{a}&\mathbf{b}&\mathbf{c}\end{vmatrix}, \mathbf{a},\mathbf{b},\mathbf{c} \in \mathbb{R}^3$ die Determinante einer $3 \times 3$ Matrix mit den Spaltenvektoren $\mathbf{a}$, $\mathbf{b}$ und $\mathbf{c}$ ist. Eine solche Determinante lässt sich mithilfe linearer Algebra als $\begin{vmatrix}\mathbf{a}&\mathbf{b}&\mathbf{c}\end{vmatrix} = -(\mathbf{a}\times \mathbf{c})\cdot \mathbf{b} = -(\mathbf{c}\times \mathbf{b})\cdot \mathbf{a}$ berechnen. Dadurch kann die Gleichung umgeschrieben werden als:

	\begin{align}
		\label{schnitttests_gleichung}
		\begin{bmatrix}u \\ \mu_1 \\ \mu_2\end{bmatrix} = 
		\frac{1}{(\mathbf{d} \times \mathbf{e}_2) \cdot \mathbf{e}_1}	
		\begin{bmatrix}
			(\mathbf{t} \times \mathbf{e}_1) \cdot \mathbf{e}_2 \\[6pt]
			(\mathbf{d} \times \mathbf{e}_2) \cdot \mathbf{t}   \\[6pt]
			(\mathbf{t} \times \mathbf{e}_1) \cdot \mathbf{d}
		\end{bmatrix}	= 
		\frac{1}{\mathbf{p} \cdot \mathbf{e}_1}	
		\begin{bmatrix}
			\mathbf{q} \cdot \mathbf{e}_2 \\[6pt]
			\mathbf{p} \cdot \mathbf{t}   \\[6pt]
			\mathbf{q} \cdot \mathbf{d}
		\end{bmatrix}	,
	\end{align}
wobei $\mathbf{p} = \mathbf{d} \times \mathbf{e}_2$ und $\mathbf{q} = \mathbf{t} \times \mathbf{e}_1$ gewählt werden, damit diese Berechnungen im resultierenden Algorithmus wiederverwendet werden können. Mithilfe dieses Vektors lässt sich nun der Schnittpunkt zwischen der Ebene und dem Strahl eindeutig bestimmen. \\
Auffällig ist, dass die Gleichung im Fall $\mathbf{p} \cdot \mathbf{e}_1 = 0$ eine Division durch 0 hervorrufen würde. Dieser spezifische Fall tritt auf, wenn der Strahl parallel zur Ebene verläuft. Da in diesem Fall kein eindeutiger Schnittpunkt bestimmt werden kann, ist es möglich, vor der Berechnung auf diesen Fall zu prüfen, und wenn dieser auftritt, zurückgegeben werden, dass kein Schnittpunkt vorhanden ist.\\
Desweiteren muss noch geprüft werden, ob der Schnittpunkt valide ist, das heißt dass er im positiven Bereich des Strahls und innerhalb des zu prüfenden Dreiecks liegt. Ersteres ist einfach mit einem Test von $u \geq 0$ zu erledigen. Zweiteres lässt sich - wie in Gleichung \ref{schnitttests_umformung_mu12} etabliert - durch eine Prüfung von $1 \geq (\mu_1 + \mu_2)$ behandeln. Ist eine dieser Bedingungen nicht gegeben, kann der Schnittpunkt abgelehnt werden.\\
In Abschnitt \ref{raytracing_basics} wurde erwähnt, dass in einigen Fällen nur der nächste Schnittpunkt gesucht wird. In diesem Fall kann der Schnittpunkt auch dann abgelehnt werden, wenn $u \geq u_\mathit{min}$ ist, also der neue Schnittpunkt weiter entfernt wäre als der momentan akzeptierte.

%
%
