%========================================================================================
% TU Dortmund, Informatik Lehrstuhl VII
%========================================================================================

\chapter{Schallausbreitung}
\label{Schallausbreitung}
%
Da das Ziel dieser Arbeit ist, ein Modell aufzustellen, das die Approximation realer Schallausbreitungsszenarien ermöglicht, soll dieses Kapitel zunächst die relevanten physikalischen Zusammenhänge erläutern, mit denen sich reale Umgebungen beschreiben lassen. Dazu beschäftigt sich Abschnitt \ref{Schallwellen} mit den grundlegenden Größen, mit denen sich im Weiteren beschäftigt werden soll, und der Ausbreitung von Schallwellen.
Die physikalischen Grundlagen, die in diesem Kapitel genutzt werden, orientieren sich an \citet*{muller2004taschenbuch}.\\
In Abschnitt \ref{einführung_in_die_problemstellung} soll die Problemstellung der Arbeit illustriert werden. Abschnitt \ref{Schallwellen} etabliert die Grundlagen, die zum Verstehen von Schall notwendig sind. In Abschnitt \ref{Systemtheorie_und_Impulse_Response} wird in die Systemtheorie eingeführt, die es erlaubt, Schallphänomene konzis darzustellen. In Abschnitt \ref{Punktschallquellen} wird betrachtet, wie Punktschallquellen sich in einer freien Umgebung verhalten. Daraufhin wird in Abschnitt \ref{reflexion} behandelt, wie Schall von Hindernissen beeinflusst wird.



\section{Einführung in die Problemstellung}
\label{einführung_in_die_problemstellung}
Um zu verdeutlichen, welche Problematiken in dieser Arbeit zu bewältigen sind, soll zunächst ein Überblick über die Problemstellung gegeben werden. Auf die einzelnen Aspekte wird näher in den folgenden Kapiteln eingegangen.\\

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/Raum.pdf_tex}}
	\caption[Beispielraum]{Horizontaler Schnitt eines bespielhaften Raumes. Für eine beliebige Umgebung wie diese soll es möglich sein, den Klang zu simulieren, der an $\mathbf{s}_E$ ankommt, wenn an $\mathbf{s}_Q$ Schall erzeugt wird.}
	\label{beispielraum}
\end{figure}

Die Simulation soll ermöglichen, das Klangverhalten eines beliebigen Raumes anhand seiner Geometrie zu betrachten. Dabei wird angenommen, dass Szenendaten zur Verfügung stehen. Dies beinhaltet die Position und Form aller Objekte im Raum, angenähert durch eine Menge von Dreiecken, Materialdaten zu den Oberflächen der Objekte und Positionen der Schallquelle und des Schallempfängers.\\
Eine Schallquelle in der Simulation kann dabei für eine beliebige Schallquelle in der Realität stehen, beispielsweise einen Lautsprecher, ein Instrument oder die menschliche Stimme. Abgesehen von der Position werden Daten benötigt, die beschreiben, wie die Schallquelle klingt. Es soll davon ausgegangen werden, dass diese Klangdaten von einer realen Schallquelle mit einem Mikrofon aufgenommen wurden. Es wird angenommen, dass diese Aufnahme in einem konstanten Abstand $r_0$ und unter Idealbedingungen geschieht, das heißt, dass nur Schall direkt von der Quelle aufgenommen wird, nicht aber Reflexionen oder Störgeräusche, und dass durch die Messungen keine Abweichungen entstehen. Diese Klangdaten stellen das \textit{Eingangssignal} dar. \\
Ein Schallempfänger soll ebenso für einen beliebigen Schallempfänger in der Realität stehen, wie ein Mikrofon oder das menschliche Ohr. Die Messwerte am Schallempfänger stellen das \textit{Ausgangssignal} dar.\\

Das Ziel dieser Arbeit ist es, aus dem Eingangssignal und den Szenendaten ein Ausgangssignal zu generieren, das dem einer realen Umgebung nahe kommt.


\section{Schallwellen}
\label{Schallwellen}
%
Als Schall werden mechanische Schwingungen eines Mediums im Bereich hörbarer Frequenzen bezeichnet. Je nach Medium wird hierbei zwischen Luftschall, Flüssigkeitsschall und Körperschall unterschieden, wobei in dieser Arbeit nur Luftschall behandelt werden wird.
Der physikalische Zustand eines gasförmigen Kontinuums lässt sich durch seine Dichte $\rho_G$, seine Temperatur $T_G$ und seinen Druck $p_G$ beschreiben. Schallereignisse in der Luft spiegeln sich in kleinen, lokalen Änderungen der physikalischen Ruhegrößen $\rho_0$, $T_0$ und $p_0$ wider. Da übliche Mikrofone den Schall anhand der Druckschwankungen messen, soll in dieser Arbeit Schall insbesondere mithilfe dieser Druckschwankungen in der Luft betrachtet werden.\\

Unter Berücksichtigung des Schalldrucks $p(\mathbf{v},t)$ kann der Luftdruck als 

	\begin{align}
		\label{schallwellen_pg}
		p_G = p_0 + p(\mathbf{v}, t)
	\end{align}
beschrieben werden, wobei $\mathbf{v}\in\mathbb{R}^3$ die Position im Raum und $t$ die Zeit ist. 

\section{Systemtheorie und Impulse Response}
\label{Systemtheorie_und_Impulse_Response}
%
Da sich diese Arbeit mit der Simulation des Klangverhaltens in verschieden Umgebungen befasst, muss es möglich sein, diese Umgebungen zu beschreiben. Dazu soll in diesem Abschnitt in die Systemtheorie eingeführt werden. 
\subsection{Systemtheorie}
\label{Systemtheorie}
Die Ausbreitung von Schall lässt sich als eine Kette von Ursache und Auswirkung betrachten, die als \textit{System} bezeichnet wird. Die Schwingung $x(t)$, im Folgenden als \textit{Eingangssignal} bezeichnet, regt das System an; die Schwingung propagiert sich durch das System und kann an einem beliebigen Ort im System abgegriffen werden. Die beobachtete Schwingungsgröße $y(t)$ wird als \textit{Ausgangssignal} bezeichnet. Die Verformung des Eingangssignals soll im Folgenden durch den Operator L dargestellt werden:

	\begin{align}
		\label{system_L}
		y(x) = L\{x(t)\}.
	\end{align}

Von den in dieser Arbeit behandelten Systemen wird angenommen, dass sie sich linear und zeitinvariant verhalten. Linear bedeutet, dass Signale sich überlagern können, ohne sich gegenseitig zu beeinflussen. Dieses Prinzip wird auch Superpositionsprinzip genannt. In einem linearen System gilt also für beliebige Konstanten $a_1$, $a_2$ und beliebige Signale $x_1(t)$, $x_2(t)$: 

	\begin{align}
		\label{systemtheorie_superposition}
		L\{a_1 x_1(t) + a_2 x_2(t)\} &= a_1 L\{x_1(t)\} + a_2 L\{x_2(t)\}	.
	\end{align}\\
Als zeitinvariant wird ein System bezeichnet, wenn eine zeitliche Verzögerung $\tau$ des Eingangssignals die gleiche Verzögerung im Ausgangssignal hervorruft: 
	\begin{align}
		\label{systemtheorie_zeitinvarianz}
		y(t-\tau)&=L\{x(t-\tau)\} .
	\end{align}	 
	
\subsection{Impulse Response}
\label{Impulse_Response}
Eine Möglichkeit, ein lineares und zeitinvariantes System darzustellen, bietet die \enquote{Impulse Response}. Wie diese im Zusammenhang mit dem Eingangssignal $x(t)$ steht, lässt sich aus der Darstellung des Signals als \enquote{Delta Kamm} herleiten:
	\begin{align}
		\label{ir_delta_kamm}
		x(t) &= \int_{-\infty}^{\infty} x(\tau)\delta(t-\tau) d\tau.
	\end{align}
Diese Darstellung betrachtet das Signal als Reihe von Impulsen mit infinitesimal kleinem Abstand. Die Impulse sind durch die Diracsche Delta-Funktion mit $\delta(t \neq 0) = 0$ und $\int_{-\infty}^{\infty} \delta(t) dt = 1$ bestimmt.\\

Wird der Operator $L$ auf das Eingangssignal in dieser Form angewendet, lässt sich die Linearität des Systems anwenden: 
	\begin{align}
		\label{ir_system}
		y(t)&=L\{\int_{-\infty}^{\infty} x(\tau)\delta(t-\tau) d\tau\}\\
		&=\int_{-\infty}^{\infty} x(\tau)L\{\delta(t-\tau)\} d\tau.
	\end{align}
In dieser Form kann $L\{\delta(t-\tau)\}$ durch die Impulse Response substituiert werden:
	\begin{align}
		\label{ir_system_ir}
		y(t)&=\int_{-\infty}^{\infty} x(\tau)h(t-\tau) d\tau.
	\end{align}
	
Die Impulse Response ist definiert als 
	\begin{align}
		\label{ir_def}
		 h(t)=L\{\delta(t)\}.
	\end{align}
und stellt das Ausgangssignal des Systems bei Anregung durch einen infinitesimal kurzen Impuls dar.
Wie eine solche Impulse Response auf ein Eingangssignal angewendet wird, wird näher in Kapitel \ref{Convolution_Filter} betrachtet. Die Synthese einer Impulse Response wird in Kapitel \ref{geometrische_raumakustik} betrachtet.

\section{Punktschallquellen}
\label{Punktschallquellen}
%
Tritt Schall in einem elastischen Medium auf, propagiert die Vibration sich durch das Medium. Dies resultiert in einer wellenförmigen Ausbreitung, ausgehend von der Schallquelle. Um diese Wellen beschreiben zu können, gibt es verschiedene Vereinfachungen von Schallquellen. In dieser Arbeit werden diese zu Punktschallquellen vereinfacht. Punktschallquellen beschreiben im Allgemeinen eine im Volumen pulsierende Quelle, die durch ihren Volumenfluss $Q$ gekennzeichnet ist. Diese Quelle wird in ihrer vereinfachten Form als \enquote{atmende Kugel} (eine Kugel, die sich im Radius ausdehnt und zusammenzieht) betrachtet. Unter der Annahme, dass sich der Schall in alle Richtungen gleichmäßig ausbreitet, wird $p$ abhängig von dem Abstand $r$ betrachtet, anstatt von der Position $\mathbf{v}$ wie im vorherigen Abschnitt.
Für den Schalldruck $p(r,t)$, der von dieser Quelle ausgeht, gilt\cite{muller2004taschenbuch}:
	
	\begin{align}
		\label{schallwellen_druck_punktquelle}
		p(r,t) = \frac{\rho_0}{4{\pi}r}\frac{{\delta}Q(t-\frac{r}{c}))}{\delta t}.
	\end{align}
	
Betrachtet man diese Quelle anhand eines bekannten Schalldruckverlaufs $p(r_0,t)$, der in einer beliebigen aber konstanten Entfernung $r_0$ aufgenommen wurde, dann ist es interessant, welchen Schalldruck die Quelle in einer beliebigen Entfernung $r$ hervorrufen würde. Im Folgenden soll dieser Zusammenhang aus der Gleichung \ref{schallwellen_druck_punktquelle} hergeleitet werden. Dazu werden zunächst $r_0$ und $t+\frac{r_0}{c}-\frac{r}{c}$ in Gleichung \ref{schallwellen_druck_punktquelle} eingesetzt:  

	\begin{align*}
		p(r_0,t+\frac{r_0}{c}-\frac{r}{c}) &= \frac{\rho_0}{4{\pi}r_0}\frac{{\delta}Q((t+\frac{r_0}{c}-\frac{r}{c})-\frac{r_0}{c})}{\delta t}.
	\end{align*}
Durch Umformung entsteht die Gleichung:
	\begin{align*}
		p(r_0,t+\frac{r_0}{c}-\frac{r}{c}) &= \frac{\rho_0}{4{\pi}r_0}\frac{{\delta}Q((t+\frac{r_0}{c}-\frac{r}{c})-\frac{r_0}{c}))}{\delta t}\\
		&= \frac{\rho_0}{4{\pi}}\frac{{\delta}Q(t-\frac{r}{c})}{\delta t} \cdot \frac{1}{r_0}.
	\end{align*}
Wird die rechte Seite mit $1 = r/r$ multipliziert, lässt sich Gleichung \ref{schallwellen_druck_punktquelle} anwenden:
	\begin{align*}
		p(r_0,t-\frac{r-r_0}{c}) &= \frac{\rho_0}{4{\pi}}\frac{{\delta}Q(t-\frac{r}{c})}{\delta t} \cdot \frac{r}{r}\frac{1}{r_0}\\
		&= \frac{\rho_0}{4{\pi}r}\frac{{\delta}Q(t-\frac{r}{c})}{\delta t} \cdot \frac{r}{r_0}\\
		&= p(r,t) \cdot \frac{r}{r_0}.
	\end{align*}
Diese Gleichung lässt sich nach $p(r,t)$ auflösen, sodass das gesuchte Verhältnis zwischen $p(r,t)$ und $p(r_0,t)$ ersichtlich wird:
	\begin{align}
		\label{schallwellen_pr0}
		 p(r,t) &= \frac{r_0}{r} \cdot p(r_0, t - \frac{r-r_0}{c}).
	\end{align}

Aus dieser Gleichung geht hervor, dass sich die Amplitude umgekehrt proportional zur Entfernung verhält und der zeitliche Versatz zwischen den Funktionen proportional mit der Differenz $r-r_0$ wächst. 

\section{Reflexion von Schallwellen}
\label{reflexion}
%
Um das Klangverhalten einer Schallquelle im Raum zu beschreiben, ist es neben dem Verhalten der Quelle im freien Feld auch notwendig, den Einfluss von Hindernissen zu untersuchen. Hindernisse sind charakterisiert durch einen Übergang von Luft auf ein anderes Medium. Trifft eine Schallwelle auf einen solchen Übergang, wird ein Teil der Schwingungen auf das neue Medium übertragen. Da diese Arbeit nur Luftschall behandeln soll, wird dieser Teil vernachlässigt. Die Schwingungen, die nicht auf das neue Medium übertragen werden, werden nach dem Huygensschen Prinzip reflektiert und propagieren sich weiter durch die Luft\cite{mathpageshuygens}.\\
Nach dem diesem Prinzip kann jeder beliebige Punkt im Raum, der durch die ursprüngliche Schallquelle angeregt wird, als Punktschallquelle betrachtet werden. Die Überlagerung dieser Sekundärquellen am Rand des Mediums resultiert in einer Reflexion der Welle. Da dieser Prozess in realen Bedingungen sehr komplex ist, sollen hier einige Vereinfachungen genutzt werden. Zunächst soll angenommen werden, dass die Distanz von der Ursprungsquelle groß genug ist, dass die Krümmung der Wellenfront so gering ist, dass sie ausreichend genau durch eine ebene Wellenfront ersetzt werden kann. Weiterhin soll davon ausgegangen werden, dass die Oberfläche des Hindernisses eine Ebene ist. Eine solche Reflexion wird in Abbildung \ref{reflexion_fig_huygens_reflexion} visualisiert. \\

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/HuygenReflexion.pdf_tex}}
	\caption[Reflexion nach Huygensschem Prinzip]{Reflexion einer ebenen Wellenfront unter Anwendung des Huygensschen Prinzips. Die verschiedenen Punkte der Wellenfront erreichen die Ebene mit verschiedenen Verzögerungen. Punkte, die die Ebene früher erreichen, können sich während dieser Zeit bereits weiterbewegt haben.}
	\label{reflexion_fig_huygens_reflexion}
\end{figure}
Trifft eine Wellenfront mit der Ausbreitungsrichtung $\mathbf{d}_e$ auf eine Ebene mit dem Normalenvektor $\mathbf{n}$, dann treffen verschiedene Punkte der Wellenfront zu verschiedenen Zeitpunkten $t$ auf die Ebene. Die zeitliche Verzögerung zwischen zwei Punkten $\mathbf{p}_0$ und $\mathbf{p}_1$ sei als $\Delta t$ bezeichnet und $x$ bezeichne den Abstand der Punkte entlang der Wellenfront (in einer durch $\mathbf{n}$ und $\mathbf{d}_e$ aufgespannten Ebene). $c \Delta t$ (mit Ausbreitungsgeschwindigkeit $c$) bezeichnet dann den Unterschied des Weges, den die Punkte auf der Welle vor dem Auftreffen auf der Ebene zurückgelegt haben. Dieser Weg lässt sich mithilfe von Trigonometrie berechnen als
		\begin{align}
			\label{reflexion_wegdifferenz}
			c \Delta t = \frac{x}{\cos \theta_e},
		\end{align}
wobei $\theta_e$ der Einfallswinkel $\theta_e = \cos^{-1}(\frac{\mathbf{n}}{|\mathbf{n}|} \times \frac{\mathbf{d}_e}{|\mathbf{d}_e|}) $ ist. Trifft die Wellenfront nach der Verzögerung an $\mathbf{p}_1$ auf die Ebene, dann ist an $\mathbf{p}_0$ bereits eine Zeitspanne $\Delta t$ vergangen. In dieser Zeit hat sich die nach dem Huygensschen Prinzip von $\mathbf{p}_0$ ausgehende kugelförmige Wellenfront bereits um $c \Delta t$ ausgebreitet. Die Überlagerung dieser kugelförmigen Wellenfronten, ausgehend von allen Punkten auf der Wellenfront, die mit der Ebene kollidieren, ergibt dann wiederum eine ebene Wellenfront mit der Ausbreitungsrichtung $\mathbf{d}_r$ und dem Ausfallswinkel $\theta_r = \cos^{-1}(\frac{\mathbf{n}}{|\mathbf{n}|} \times \frac{\mathbf{d}_r}{|\mathbf{d}_r|}) $. Da für diesen Winkel die gleichen Bedingungen gelten wie in Gleichung \ref{reflexion_wegdifferenz}, gilt:
		\begin{align}
			\label{reflexion_reflektierter_winkel}
			\cos \theta_e &= \frac{x}{c \Delta t} = \cos \theta_r \notag \\
			\theta_e &= \theta_r.
		\end{align}

Die Ausbreitungsrichtung der reflektierten Wellenfront $\mathbf{d}_r$ entspricht also der an dem Normalenvektor $\mathbf{n}$ der Fläche gespiegelten Ausbreitungsrichtung der eingehenden Wellenfront $\mathbf{d}_e$.









%
