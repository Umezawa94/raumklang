%========================================================================================
% TU Dortmund, Informatik Lehrstuhl VII
%========================================================================================

\chapter{Convolution-Filter}
\label{Convolution_Filter}
%
Ziel dieser Arbeit ist es, das Klangverhalten einer Quelle in einem virtuellen Raum hörbar zu machen. Dazu muss es möglich sein, das Eingangssignal der virtuellen Quelle zu verarbeiten, um das Ausgangssignal zu berechnen, das am  virtuellen Empfänger eingeht. Dieses Kapitel soll mit dem Convolution-Filter ein Verfahren vorstellen, mit dem es möglich ist, ein Klangprofil des Raumes auf ein Signal anzuwenden. Wie ein solches Klangprofil erstellt werden kann, wird in Kapitel \ref{Raytracing} behandelt.

In Abschnitt \ref{Digital_signal_processing} wird zunächst eine Einführung in Digital Signal Processing gegeben, welches die Grundlage für dieses Verfahren liefert. Abschnitt \ref{Convolution} beschäftigt sich mit der Convolution und wie sie auf Audiosignale angewendet werden kann. Abschnitt \ref{Fourier_Transformation} geht auf den Zusammenhang zwischen Convolution und Fourier Transformation ein, der genutzt werden kann, um die Berechnung der Convolution zu beschleunigen. In Abschnitt \ref{Fast_Fourier_transform} wird mit dem Fast Fourier Transform ein Verfahren zur Berechnung der Fourier-Transformation vorgestellt, welches weniger Laufzeit als ein iterativer Ansatz benötigt.

\section{Digital Signal Processing}
\label{Digital_signal_processing}
%
Da Berechnungen für diese Arbeit von einem digitalen System durchführbar sein sollen, müssen Audiodaten in einem Format betrachtet werden, auf dem ein solches System arbeiten kann. Im vorherigen Kapitel wurde Schall im Wesentlichen als eine Funktion $p(r,t)$ betrachtet, die die Druckschwankungen des Mediums abhängig von Distanz $r$ zur Quelle und der Zeit $t$ abbildet. Eine von der Zeit abhängige Funktion wird auch als \textit{Signal} bezeichnet. Da es sich um eine kontinuierliche Abbildung von Zeit auf eine kontinuierliche Skala von Amplituden handelt, wird hierbei von einem es sich um ein \textit{analoges Signal} gesprochen. Im Kontrast dazu stehen \textit{digitale Signale}, die im Zeit- und Wertebereich diskret sind\cite{hussain2011digital}.\\

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/Digital_signal_discret.pdf_tex}}
	\caption[Analoges und digitales Signal]{Beispiel eines analogen und eines entsprechenden digitalen Signals}
	\label{fig_analog_digital}
\end{figure}

Um ein analoges Signal $f(t)$ in ein digitales Signal $f[n]$ (im Weiteren werden zeitdiskrete Signale mit eckigen Klammern dargestellt) umzuwandeln, sind zwei Prozesse notwendig: Abtastung, um den Zeitbereich zu diskretisieren, und Quantisierung, um den Wertebereich zu diskretisieren. In der Abtastung werden die Werte an diskreten Zeitpunkten $t = nT_s, n\in\mathbb{N}$ abgelesen, wobei $T_s$ der konstante Abtastzeitraum ist und mit der konstanten Abtastrate im Zusammenhang $T_s=1/f_s$ steht.  
In der Quantisierung wird das Signal durch Runden oder Abschneiden auf eine endliche Menge von Werten abgebildet. So wird bei einer M-Bit-Quantisierung das Signal auf den nächsten Wert in Schritten von $\delta = (p_\mathit{max} - p_\mathit{min})/2^M$ gerundet, wobei $p_\mathit{min}$ und $p_\mathit{max}$ die erwarteten minimalen und maximalen Messwerte des Signals sind.\\

Nach Abtastung und Quantisierung verbleibt ein digitales Signal, das als Strom von Samples, einzelnen Abtastwerten, verstanden werden kann. Auf diesem Datenstrom können Modifikationen am Signal vorgenommen werden, um verschiedene Effekte zu erzielen. Während es für einige Effekte ausreicht, einzelne Samples des Stroms zu modifizieren (beispielsweise ein einfacher Verstärker-Effekt, der jedes Sample mit einem konstanten Wert multipliziert und zurückgibt), ist für andere Effekte ein größeres Fenster des Signals nötig (ein Beispiel dafür ist der Convolution-Effekt der in den folgenden Kapiteln vorgestellt wird). Ist dies der Fall, müssen Samples in einem Puffer gesammelt werden, bevor sie verarbeitet werden. Dies führt zu einer Verzögerung im Signal von $t = |B| \cdot T_s$, wobei $|B|$ die Länge des Puffers ist.
    


\section{Convolution}
\label{Convolution}
%
Die Convolution ist die Operation, die es erlauben soll, die Impulse Response eines Systems auf ein Eingangssignal anzuwenden. Wie ein System durch eine Impulse Response beschrieben werden kann, wurde bereits in Abschnitt \ref{Systemtheorie_und_Impulse_Response} behandelt. In diesem Abschnitt soll konkret darauf eingegangen werden, wie die Convolution im Digital Signal Processing angewendet werden kann.
Die Convolution ist für zwei analoge Signale $f(t)$ und $g(t)$ definiert als: 

	\begin{align}
		\label{convolution_analog_def}
		f(t)**g(t) = \int_{-\infty}^{\infty}f(t-\tau)g(\tau) \mathrm{d}\tau.
	\end{align}
	
Für zeitdiskrete Signale $f[n]$ und $g[n]$ ist die Convolution analog definiert als\cite{mitra1993handbook}: 

	\begin{align}
		\label{convolution_diskret_def}
		f[n]**g[n] = \sum _{k=-\infty}^{\infty}f[n-k]g[k].
	\end{align}
	
Dieser Definition entsprechend, lässt sich das Ausgangssignal in Gleichung \ref{ir_system_ir} als Convolution zwischen Eingangssignal und Impulse Response beschreiben: 

	\begin{align}
		\label{convolution_ir}
		y(t) = x(t) ** h(t).
	\end{align}

Da in dieser Arbeit Convolution insbesondere mit Hilfe von Digital Signal Processing berechnet werden soll, wird hier das diskrete Äquivalent dieser Gleichung benutzt: 
	
	\begin{align}
		\label{convolution_ir_diskret}
		y[n] &= x[n] ** h[n] \\
		&= \sum _{k=-\infty}^{\infty}x[n-k]h[k].
	\end{align}
	
Weiterhin soll davon ausgegangen werden, dass das System kausal ist, das heißt, das Ausgangssignal kann nicht ausschlagen, ohne dass zuvor ein Ausschlag im Eingangssignal gegeben sein muss. Dies spiegelt sich in der Impulse Response insofern wider, als dass sie für $n < 0$ nur Nullwerte enthält. Dadurch lässt sich die Convolution-Formel reduzieren: 

	\begin{align}
		\label{convolution_ir_diskret_kausal}
		y[n] &= \sum _{k=0}^{\infty}x[n-k]h[k].
	\end{align}

Darüber hinaus soll die Impulse Response in ihrer Länge auf $K$ Samples begrenzt werden, sodass $h(n\geq K) = 0$. Diese Einschränkung basiert auf der Annahme, dass der Einfluss von einem eingehenden Impuls nach einer endlichen Anzahl von $K$ Samples vernachlässigbar gering ist. $K$ soll hierbei beliebig, aber endlich sein. Hierdurch lässt sich die Convolution auch nach oben begrenzen: 

	\begin{align}
		\label{convolution_ir_diskret_begrenzt}
		y[n] &= \sum _{k=0}^{K-1}x[n-k]h[k].
	\end{align}
	
In dieser Form lässt sich die Convolution für jedes einzelne Sample $n$ berechnen, sofern die Samples des Eingangssignals von $x[n-K]$ bis $x[n]$ zur Verfügung stehen. Ein Algorithmus, der diese Formel naiv implementiert, würde theoretisch keine Verzögerung einbringen. Allerdings beträgt die Laufzeit, um $N$ Samples des Eingangssignals zu verarbeiten, $O(N \cdot K)$, was insbesondere für große $K$ in der Praxis Probleme bereiten würde.
 
\section{Fourier-Transformation}
\label{Fourier_Transformation}
%
Ein Mittel, um die Laufzeit der Berechnung des Ausgangssignals bei der Convolution zu reduzieren, stellt die Fourier-Transformation dar. In diesem Abschnitt soll auf die Funktionsweise der Fourier-Transformation eingegangen, sowie das Convolution-Theorem betrachtet werden, welches es ermöglicht, die Fourier-Transformation zur Berechnung der Convolution zu nutzen.\\

Die Fourier-Transformation basiert auf der Annahme, dass sich eine Funktion durch eine Summe von trigonometrischen Funktionen mit verschiedenen Phasen und Amplituden darstellen lässt. Sie ermöglicht, eine komplexwertige Funktion im Zeitbereich $x(t)$ (eine von der Zeit abhängige Funktion) auf eine komplexwertige Funktion im Frequenzbereich $X(\omega)$ abzubilden. $X(\omega)$ bildet dann die Frequenzen $\omega$ auf die Amplituden ab, mit der sie im Signal $x(t)$ vorkommen. Die kontinuierliche Fourier-Transformation ist definiert als:
	\begin{align}
		\label{fourier_transformation_definition_kontinuierlich}
		(\mathcal{F} x)(\omega) &= X(\omega) = \int_{-\infty}^{\infty} x(t)\cdot e^{-\mathrm{i} y \cdot t} \,\mathrm{d} x.
	\end{align}
	
Da die Fourier-Transformation in dieser Arbeit im Kontext des Digital Signal Processings genutzt wird, soll hier das diskrete Analog verwendet werden, die Diskrete Fourier-Transformation ($\mathrm{DFT}$):

	\begin{align}
		\label{fourier_transformation_definition_diskret}
		\mathrm{DFT}(x)[k] &= X[k] = \sum_{n=0}^{N-1}x[n]\cdot e^{-\frac{2 \pi i}{N} k n}, & &
		k = 0,...,N-1.
	\end{align}
$N$ ist hierbei die Länge des Eingangssignals $x[n]$ und des Ausgangssignals $X[k]$.\\
Relevant ist auch die Inverse Diskrete Fourier-Tranformation ($\mathrm{IDFT}$), welche die Umkehrfunktion zur $\mathrm{DFT}$ darstellt und somit ermöglicht, vom Frequenzbereich in den Zeitbereich zu überführen: 

	\begin{align}
		\label{fourier_transformation_definition_diskret_invers}
		\mathrm{IDFT}(X)[n] &= x[n] = \frac{1}{N} \sum_{k=0}^{N-1}X[k]\cdot e^{\frac{2 \pi i}{N} k n}.
	\end{align}

Um das Convolution-Theorem herzuleiten, soll nun die Diskrete Fourier-Transformation auf eine diskrete Convolution zwischen $x[n]$ und $h[n]$ mit der Länge N angewendet werden:

	\begin{align*}
		\mathrm{DFT}(x ** h)[k] &= \sum_{n=0}^{N-1}(x**h)[n]\cdot e^{-\frac{2 \pi i}{N} k n}\\
		&= \sum_{n=0}^{N-1} \sum _{m=0}^{M-1}x[n-m]h[m] \cdot e^{-\frac{2 \pi i}{N} k n}.
	\end{align*}
	
Unter Anwendung des Fubini-Theorems\cite{fubini} können die Summen vertauscht werden:
	
	\begin{align*}
		&= \sum_{m=0}^{M-1} \sum _{n=0}^{N-1}x[n-m]h[m] \cdot e^{-\frac{2 \pi i}{N} k n}.
	\end{align*}

Dies ermöglicht, $h(m)$ unter Anwendung des Assoziativgesetzes aus der inneren Summe herauszuziehen, da es nicht von n abhängig ist.
	\begin{align*}
		&= \sum_{m=0}^{M-1} h[m] \sum _{n=0}^{N-1}x[n-m] \cdot e^{-\frac{2 \pi i}{N} k n}.
	\end{align*}
	
Unter Berücksichtigung der Definition der Diskreten Fourier-Transformation ergibt sich:
	
	\begin{align*}
		&= \sum_{m=0}^{M-1} h[m] X[k-m].
	\end{align*}
	
Auf diesen Term lässt sich das Shift-Theorem anwenden, welches aussagt, dass eine Verschiebung von $m$ im Zeitbereich einer Multiplikation mit $e^{-\frac{2 \pi i}{N} k m}$ im Frequenzbereich entspricht \cite{smith2007mathematics}.

	\begin{align*}
		&= \sum_{m=0}^{M-1} h[m]e^{-\frac{2 \pi i}{N} k m} X[k]\\
		&= \Big(\sum_{m=0}^{M-1} h[m]e^{-\frac{2 \pi i}{N} k m}\Big) X[k].
	\end{align*}
Eine weitere Anwendung der Definition ergibt:
	\begin{align}
		\label{fourier_transformation_convolution_therorem}
		\mathrm{DFT}(x ** h)[k] &= X[k] H[k].
	\end{align}

Wird nun die Inverse Diskrete Fourier-Tranformation angewendet, ist hiermit gezeigt, wie die Convolution mithilfe der Diskreten Fourier-Transformation berechnet werden kann:
	\begin{align}
		\label{fourier_transformation_convolution_therorem2}
		(x ** h)[n] &= \mathrm{IDFT}(X \cdot H)[n].
	\end{align}

\section{Fast Fourier Transform}
\label{Fast_Fourier_transform}
%
Im vorhergehenden Abschnitt wurde etabliert, wie die diskrete Convolution mithilfe der Diskreten Fourier-Transformation $\mathrm{DFT}$ berechnet werden kann. Jedoch würde eine einfache iterative Implementierung der $\mathrm{DFT}$  eine Laufzeit von $O(N^2)$ benötigen, was keine Verbesserung gegenüber einer iterativen Implementierung der Convolution darstellen würde. Deshalb soll in diesem Abschnitt der Fast Fourier Transform ($\mathrm{FFT}$) vorgestellt werden, der es ermöglicht, die Diskreten Fourier-Transformation in $O(N \log N)$ zu berechnen.\\

Als Fast Fourier Transform wird nicht ein spezifischer Algorithmus bezeichnet, stattdessen bezeichnet er eine beliebige Methode, um die Diskreten Fourier-Transformation in $O(N \log N)$ zu berechnen. In dieser Arbeit soll insbesondere der radix-2 Cooley-Tukey Algorithmus vorgestellt werden. Dabei handelt es sich um einen Teile-und-herrsche-Algorithmus, der rekursiv die $\mathrm{DFT}$ in zwei gleich große Teilprobleme zerlegt. Dafür muss die Anzahl der Samples $N$ eine Zweierpotenz sein, damit das Problem auf Probleme mit nur einem Sample reduziert werden kann.\\

Wird die $\mathrm{DFT}$ aus Gleichung \ref{fourier_transformation_definition_diskret} auf einer Menge von $2N$ Samples angewendet, entsteht die folgende Gleichung:

	\begin{align}
		\label{fft_2n}
		\mathrm{DFT}(x)[k] &= X[k] = \sum_{n=0}^{2N-1}x[n]\cdot e^{-\frac{2 \pi i}{2N} k n}, && k = 0, ..., 2N-1.
	\end{align}
	
Nun soll $x[n]$ aufgeteilt werden in die Elemente mit geraden Indizes
	\begin{align*}
		x'[n] = x[2n], && n = 0, ..., N-1
	\end{align*}
und Elemente mit ungeraden Indizes
	\begin{align*}
		x''[n] = x[2n], && n = 0, ..., N-1.
	\end{align*}
Die $\mathrm{DFT}$ von $x'[n]$ und $x''[n]$ lassen sich dann bestimmen als:
	\begin{align}
		\label{fft_split_dft_even}
		\mathrm{DFT}(x')[k] &= X'[k] = \sum_{n=0}^{N-1}x'[n]\cdot e^{-\frac{2 \pi i}{N} k n}, && k = 0, ..., N-1,\\
		\label{fft_split_dft_odd}
		\mathrm{DFT}(x'')[k] &= X''[k] = \sum_{n=0}^{N-1}x''[n]\cdot e^{-\frac{2 \pi i}{N} k n}, && k = 0, ..., N-1.
	\end{align}
	
Wird in Gleichung \ref{fft_2n} $x[n]$ durch $x'[n]$ und $x''[n]$ ersetzt, lautet die neue Gleichung:

	\begin{align*}
		X[k] &= \sum_{n=0}^{N-1}x[2n]\cdot e^{-\frac{2 \pi i}{2N} k 2n}
		+ \sum_{n=0}^{N-1}x[2n + 1]\cdot e^{-\frac{2 \pi i}{2N} k (2n + 1)}\\
		&= \sum_{n=0}^{N-1}x'[n]\cdot e^{-\frac{2 \pi i}{N} k n}
		+ \sum_{n=0}^{N-1}x''[n]\cdot e^{-\frac{2 \pi i}{2N} k 2n} \cdot e^{-\frac{2 \pi i}{2N} k}\\
		&= \sum_{n=0}^{N-1}x'[n]\cdot e^{-\frac{2 \pi i}{N} k n}
		+ e^{-\frac{2 \pi i}{2N} k} \cdot \sum_{n=0}^{N-1}x''[n]\cdot e^{-\frac{2 \pi i}{N} k n}, && k = 0, ..., N-1. 
	\end{align*}
Hier lassen sich nun die $\mathrm{DFT}$  von $x'[n]$ und $x''[n]$ (Gleichung \ref{fft_split_dft_even} und \ref{fft_split_dft_odd}) einsetzen:

	\begin{align*}
		X[k] &= X[k] + e^{-\frac{\pi i}{N} k} \cdot X[k], && k = 0, ..., N-1. 
	\end{align*}
	
Da die $\mathrm{DFT}$  außerhalb des Definitionsbereich des Eingangssignals implizit periodisch ist \cite{mitra1993handbook}, also insbesondere $X'[k + N] = X'[k]$ und $X''[k + N] = X''[k]$ gilt, kann dadurch der Bereich $X[k], \: k = N, ... ,2N-1$ berechnet werden:

	\begin{align}
		\label{fft_almost_complete}
X[k] &=
\left\{
	\begin{array}{ll}
		 X'[k]     + e^{-\frac{\pi i}{N} k}  X''[k]     & \mbox{für } 0 \leq k < N, \\
		 X'[k - N] + e^{-\frac{\pi i}{N} k}  X''[k - N] & \mbox{für } N \leq k < 2N.
	\end{array}
\right.
	\end{align}

Der Faktor $e^{-\frac{\pi i}{N} k}$ wird hierbei auch als \textit{Twiddle Faktor} bezeichnet. Eine Umformung für den Bereich $N \leq k < 2N$ ergibt:
	\begin{align*}
		e^{-\frac{\pi i}{N} (k + N)} 
			&= e^{-\frac{\pi i}{N} k}e^{-\frac{\pi i}{N} N}\\
			&= e^{-\frac{\pi i}{N} k}e^{-\pi i}\\
			&= \frac{1}{e^{\pi i}}e^{-\frac{\pi i}{N} k}.
	\end{align*}
Wird Eulers Identität $e^{\pi i} + 1 = 0$ \cite{conway1996farey} angewendet, entsteht
	\begin{align}
		\label{fft_twiddle_factor}
			&= \frac{1}{-1}e^{-\frac{\pi i}{N} k} \notag \\
		e^{-\frac{\pi i}{N} (k + N)}
			&= -e^{-\frac{\pi i}{N} k}.
	\end{align}
Wird diese Umformung wiederum in Gleichung \ref{fft_almost_complete} angewendet, resultiert:
	
	\begin{align}
		\label{fft_complete}
X[k] &=
\left\{
	\begin{array}{ll}
		 X'[k]     + e^{-\frac{\pi i}{N} k      }  X''[k]     & \mbox{für } 0 \leq k < N, \\
		 X'[k - N] - e^{-\frac{\pi i}{N} (k - N)}  X''[k - N] & \mbox{für } N \leq k < 2N.
	\end{array}
\right.
	\end{align}
Mit dieser Formel müssen nur $X'[k]$ und $e^{-\frac{\pi i}{N} (k)}  X''[k]$ auf $N$ Samples berechnet werden, um $X[k]$ mit $2N$ Samples zu berechnen. Diese Formel lässt sich dann wiederum rekursiv anwenden um $X'[k]$ und $X''[k]$ zu bestimmen. Die Abbruchbedingung der Rekursion liegt bei $N=1$. An diesem Punkt ist die $\mathrm{DFT}$  gleich der ursprünglichen Funktion, wie sich einfach an der Definition der $\mathrm{DFT}$ (Gleichung \ref{fourier_transformation_definition_diskret}) zeigen lässt:

	\begin{align}
		\label{fft_abbruch}
		X[k] = \sum_{n=0}^{0}x[n]\cdot e^{-\frac{2 \pi i}{1} k n}, & &
		k = 0\notag\\
		X[0] = x[0]\cdot e^{-\frac{2 \pi i}{1} \cdot 0 \cdot 0}\notag\\
		X[0] = x[0].
	\end{align}

	


%\section{Zero Delay Fast Fourier Transform}
%\label{Zero_Delay_Fast_Fourier_transform}
%
%Im voherigen Abschnitt wurde mit dem Fast Fourier Transform ein Algorithmus vorgestellt, der die Berechnung der Convolution beschleunigen kann. Jedoch muss dafür eine ausreichend große Menge an Samples betrachtet werden, um tatsächlich einen Vorteil gegenüber einem iterativen Ansatz zu zeigen. Dafür müssen Samples in einem Puffer gesammelt werden, bevor sie verarbeitet werden. Dadurch entsteht eine Verzögerung, die der Länge des Puffers entspricht. Der Zero Delay Fast Fourier Transform ermöglicht, diese Verzögerung zu vermeiden, indem eine Kombination aus Iterativer Convolution und Convolution mithilfe von FFT genutzt wird. \cite{gardner1994efficient}

%\tbc


%
