%========================================================================================
% TU Dortmund, Informatik Lehrstuhl VII
%========================================================================================

\chapter{Geometrische Raumakustik}
\label{geometrische_raumakustik}
%
Ein akustisches System vollständig zu beschreiben ist - insbesondere in einer Umgebung mit Hindernissen - ein komplexer Prozess. Deswegen soll in diesem Kapitel ein Ansatz vorgestellt werden, mit dem es möglich ist, sich einem System ausreichend genau anzunähern.\\

In Abschnitt \ref{strahlen} wird darauf eingegangen, wie Strahlen genutzt werden können, um eine Schallquelle zu modellieren. In Abschnitt \ref{regulaere_reflexion} und \ref{diffuse_reflexion} wird auf die verschiedenen Arten von Reflexion eingegangen und wie diese mithilfe von Strahlen modelliert werden können. Abschnitt \ref{berechnung_ir} soll darauf zeigen, wie aus den Strahlen eine Impulse Response generiert werden kann, die von einem Convolution-Reverb, wie er in Kapitel \ref{Convolution_Filter} vorgestellt wurde, benutzt werden kann, um ein Ausgangsignal zu synthetisieren.

\section{Strahlen}
\label{strahlen}
%
Die geometrische Raumakustik verwendet Strahlen, um einen infinitesimalen Ausschnitt eines Schallfeldes einer Punktschallquelle zu repräsentieren. Ein Strahl wird als
	\begin{align}
		\label{strahlen_strahl}
		\mathbf{r}(u) = \mathbf{a} + u \cdot \mathbf{d}, u \in \mathbb{R} > 0
	\end{align}
durch den Anfangspunkt $\mathbf{a} \in \mathbb{R}^3$ und den Richtungsvektor $\mathbf{d} \in \mathbb{R}^3$ bestimmt. Insbesondere soll entlang eines Strahls die Schalldruckfunktion $p(r,t)$ (siehe Gleichung \ref{schallwellen_pr0}) betrachtet werden. Dazu wird für den Anfangspunkt die Position der Schallquelle gewählt. Dann ist die Entfernung eines Punktes $\mathbf{r}(u)$ auf dem Strahl zu der Schallquelle $r = \frac{u}{|\mathbf{d}|}$, oder insbesondere  $r = u$, falls $\mathbf{d}$ als ein Einheitsvektor (ein Vektor mit Länge $|\mathbf{d}| = 1$) gewählt ist. 


\section{Reguläre Reflexion}
\label{regulaere_reflexion}
%
In Abschnitt \ref{reflexion} wurde bereits behandelt, wie eine Wellenfront an einer glatten Oberfläche reflektiert wird. Da ein Strahl einen infinitesimal schmalen Ausschnitt aus einer Welle repräsentiert, verhält sich ein Strahl in der geometrischen Raumakustik analog: Trifft er mit einem Einfallswinkel $\theta_e$ auf eine Ebene mit dem Normalenvektor $\mathbf{n}$, dann wird der Strahl mit einem Aufallswinkel von $\theta_r = \theta_e$ reflektiert. 

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/GeometrischeReflexion.pdf_tex}}
	\caption[Reflexion und Spiegelschallquelle]{Werden Schallstrahlen von einer Ebene reflektiert, werden sie im gleichen Winkel zurückgeworfen. Die reflektierten Strahlen können behandelt werden, als würden sie von der Spiegelschallquelle $\mathbf{a}'$ ausgehen.}
	\label{regulaere_reflexion_GeometrischeReflexion}
\end{figure}

Der reflektierte Strahl $\mathbf{r}'(u)$ geht von dem Punkt $\mathbf{r}(u_0)$ aus, an dem der ursprüngliche Strahl auf die Ebene trifft und breitet sich in die reflektierte Richtung $\mathbf{d}$ aus. Da bis zur Reflexion bereits ein Weg von $u_0$ zurückgelegt wurde, muss dieser in der Schalldruckfunktion berücksichtigt werden: $p(u_0 + u, t)$. Wird für $\mathbf{r}'(u)$ der Punkt betrachtet, an dem der insgesamt zurückgelegte Weg 0 wäre, also $\mathbf{r}'(-u_0)$, dann wird die Position der \textit{Spiegelschallquelle} $a'$ erreicht. Die Position der Spiegelschallquelle entspricht der an der Ebene gespiegelten Position der ursprünglichen Schallquelle und ist gleich für alle an der gleichen Ebene reflektierten Strahlen mit dem gleichen Anfangspunkt. Da der Weg von der Spiegelschallquelle zu einem Punkt auf dem reflektierten Strahl gleich lang wie der Weg von der ursprünglichen Schallquelle ist, können von der Ebene reflektierte Strahlen behandelt werden, als würden sie von der Spiegelschallquelle ausgehen.\\
In der Realität wird bei einer Reflexion auch ein Teil der Schwingungen an das Material der Ebene übertragen. Dadurch sinkt die Amplitude der reflektierten Welle. Wie sehr die Amplitude reduziert wird, ist von verschiedenen Faktoren, wie zum Beispiel der Elastizität des Materials abhängig; im Rahmen dieser Arbeit soll die Absorption des Objektes jedoch auf eine Konstante $\alpha$ vereinfacht werden, womit die Amplitude der Spiegelquelle dann um einen Faktor von $1-\alpha$ reduziert wird:

	\begin{align}
		\label{regulaere_reflexion_absorption}
		p'(r, t) = (1-\alpha)p(r, t)
	\end{align}
wobei $p'(r, t)$ die Schalldruckfunktion für die Spiegelschallquelle ist.

\section{Diffuse Reflexion}
\label{diffuse_reflexion}
%
Im vorherigen Abschnitt wurde auf die Reflexion an einer glatten Ebene eingegangen. In der Realität sind Wände allerdings in den wenigsten Fällen perfekt eben. Oberflächen weisen für gewöhnlich unregelmäßige Strukturen auf, wodurch reflektierte Wellen keine eindeutig bestimmbare Richtung haben. Bei einer idealen diffusen Reflexion werden Schallwellen gleichmäßig in alle Richtungen abgestrahlt. Eine Approximation solcher Verhältnisse ist durch das Lambertsche Gesetz gegeben\cite{lambert1892lamberts}. Dieses sagt aus, dass bei einer idealen diffusen Oberfläche die Intensität der Abstrahlung proportional zum Kosinus des Einfallswinkel $\theta_e$ ist. Dieser Satz ist nicht durch physikalische Gegebenheiten hergeleitet, sondern stellt lediglich eine empirische Beobachtung dar und ist somit nicht komplett akkurat. In der Realität ist in vielen Fällen auch eine Relevanz des Ausfallwinkels zu beobachten. Dennoch lassen sich mit dem Lambertschen Gesetz in den meisten Fällen ausreichend genau die tatsächlichen Verhältnisse approximieren.

Das Lambertsche Gesetz soll in diesem Modell dadurch berücksichtigt werden, dass die Amplitude der Spiegelschallquelle zusätzlich zum Faktor $1-\alpha$ mit dem Kosinus des Einfallswinkels $\cos \theta_e = -\mathbf{d} \cdot \mathbf{n}$ (mit $|\mathbf{d}| = |\mathbf{n}| = 1$) multipliziert wird:

	\begin{align}
		\label{diffuse_reflexion_prt}
		p'(r, t) = (-\mathbf{d} \cdot \mathbf{n})(1-\alpha) p(r, t).
	\end{align}
	
Da bei der Diffusen Reflexion die Strahlung in alle Richtungen gleichmäßig abgegeben wird, kann keine eindeutige Richtung für den Strahl bestimmt werden. Aus diesem Grund wird die Richtung des reflektierten Strahls zufällig bestimmt. Dies führt unter der Annahme, dass alle Richtungen gleich wahrscheinlich sind, dazu, dass bei einer ausreichend großen Menge von Strahlen, die über diesen Ansatz verfolgt werden, die gleichmäßige Verteilung ausreichend genau approximiert werden kann.
	



\section{Berechnung einer Impulse Response}
\label{berechnung_ir}
%
In den vorherigen Abschnitten wurde gezeigt, wie mithilfe von Strahlen Spiegelschallquellen durch Reflexionen bestimmt werden können. In diesem Abschnitt wird behandelt, wie diese Spiegelschallquellen genutzt werden können, um eine Impulse Response (siehe \ref{Impulse_Response}) zu synthetisieren, die das akustische System der Umgebung beschreiben kann.\\
 Sei $\mathcal{A}$ die Menge, welche die ursprüngliche Schallquelle und alle durch die Strahlen bestimmten Spiegelschallquellen enthält. Sei $\mathbf{a}$ die Position einer beliebigen Schallquelle $\mathbf{a} \in \mathcal{A}$, $p_\mathbf{a}(r,t)$ die Schalldruckfunktion, die von dieser Quelle ausgeht und $r_a = |\mathbf{a} - \mathbf{b}|$ der Weg zwischen der Quelle und dem Schallempfänger $\mathbf{b}$.\\
 
Zunächst soll eine Impulse Response $h_\mathbf{a}(t)$ generiert werden, die nur eine einzelne Quelle $\mathbf{a}$ berücksichtigt. Das System, welches es in diesem Sinne zu beschreiben gilt, soll als Eingangssignal $x(t) = p_\mathbf{a}(r_0,t)$ erhalten. Das Ausgangssignal soll der an dem Empfänger entstehende Schalldruck $y(t) = p_\mathbf{a}(r_\mathbf{a},t)$ sein. Wie diese im Zusammenhang stehen, wurde bereits in Abschnitt \ref{Punktschallquellen} (Gleichung \ref{schallwellen_pr0}) gezeigt:
	\begin{align}
		\label{berechnung_ir_system_a}
		 y(t)= p_\mathbf{a}(r_\mathbf{a},t)
		 	&= \frac{r_0}{r_\mathbf{a}} \cdot p_\mathbf{a}(r_0, t - \frac{r_\mathbf{a}-r_0}{c})\\
		 	&= \frac{r_0}{r_\mathbf{a}} \cdot x( t - \frac{r_\mathbf{a}-r_0}{c}).
	\end{align}

In Gleichung \ref{ir_def} wurde die Impulse Response als die Reaktion des Systems auf die Diracsche Delta-Funktion  $\delta(t \neq 0) = 0$ mit $\int_{-\infty}^{\infty} \delta(t) dt = 1$ definiert. Um die Impulse Response des Systems $h_\mathbf{a}(t)$ zu erhalten, muss also das System auf die Delta-Funktion angewendet werden: 
	\begin{align}
		\label{berechnung_ir_ir_kontinuierlich}
		 h_\mathbf{a}(t) &= \frac{r_0}{r_\mathbf{a}} \cdot \delta( t - \frac{r_\mathbf{a}-r_0}{c})\\
		 h_\mathbf{a}(t \neq \frac{r_\mathbf{a}-r_0}{c}) &= 0 \quad
		 \mbox{mit} \int_{-\infty}^{\infty} h_\mathbf{a}(t) dt = \frac{r_0}{r_\mathbf{a}}.
	\end{align}
	
Um im Digital Signal Processing (siehe Kapitel \ref{Digital_signal_processing}) verwendet werden zu können, wird allerdings eine diskrete Impulse Response $h_\mathbf{a}[n]$ mit $n\in\mathbb{N}$ benötigt. Da die Delta-Funktion mit einem infinitesimal kurzem Impuls realistisch nicht dargestellt werden kann, soll diese durch einen Impuls der Länge eines Samples $T_s$ approximiert werden:

	\begin{align}
		\label{berechnung_ir_approx_delta}
		d[n] =
		\left\{\begin{array}{ll}
			\frac{1}{T_s} & \mbox{für } n = 0, \\
			0 & \mbox{sonst}.
		\end{array}\right. 
	\end{align}
Darüber hinaus muss beachtet werden, dass die Verzögerung im Signal $\frac{r_\mathbf{a}-r_0}{c}$ reellwertig ist und durch Division durch die Abtastzeit $T_s$ und Rundung in eine diskrete Anzahl von Samples übertragen werden muss. Unter Berücksichtigung dieser Approximationen ergibt sich die diskrete Impulse Response:
	\begin{align}
		\label{berechnung_ir_ir_diskret}
		 h_\mathbf{a}[n] &= \frac{r_0}{r_\mathbf{a}} \cdot \delta[ n - \lfloor \frac{r_\mathbf{a}-r_0}{c T_s} \rfloor ]\\
		h_\mathbf{a}[n] &=
		\left\{\begin{array}{ll}
			\frac{r_0}{r_\mathbf{a} T_s} & \mbox{für } n = \lfloor \frac{r_\mathbf{a}-r_0}{c T_s} \rfloor, \\
			0 & \mbox{sonst}.
		\end{array}\right.
	\end{align}

Unter der Annahme, dass sich das beschriebene System linear verhält, was bei den meisten akustischen Systemen der Fall ist \cite{muller2004taschenbuch}, kann die Impulse Response $h_\mathcal{A}[n]$, die das System darstellt, welches alle Schallquellen $\mathbf{a} \in \mathcal{A}$ berücksichtigt, durch die Summe der Impulse Responses $h_\mathbf{a}[n]$ der einzelnen Quellen betrachtet werden:

	\begin{align}
		\label{berechnung_ir_ir_all}
		h_\mathcal{A}[n] = \sum_{\mathbf{a} \in \mathcal{A}} h_\mathbf{a}[n].
	\end{align}
	

