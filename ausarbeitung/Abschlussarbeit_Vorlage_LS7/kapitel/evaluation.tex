%========================================================================================
% TU Dortmund, Informatik Lehrstuhl VII
%========================================================================================

\chapter{Evaluation}
\label{Evaluation}
%
In diesem Kapitel sollen die Ergebnisse vorgestellt werden, die durch Anwendung der in dieser Arbeit vorgestellten Methode erzielt wurden. Dazu soll in Abschnitt \ref{versuchsaufbau} ein kurzer Überblick über die Implementierung der Methode gegeben werden. In Abschnitt \ref{ergebnisse} sollen dann die Ergebnisse des Aufbaus vorgestellt werden.
\section{Versuchsaufbau}
\label{versuchsaufbau}
%
Die Implementierung wurde mithilfe von Webtechnologien unter der Verwendung der HTML5 Umgebung mit Javascript vorgenommen. Die Digital-Signal-Processing-Komponente des Verfahrens wurde mithilfe der \textit{Web Audio API} implementiert, welche Teil des HTML5-Standards ist. Dabei wurde die \textit{AudioWorkerShim}-Bibliothek\cite{audioWorkerShim} verwendet, um die Audio Worklet Komponente zu verwenden, welche bisher noch nicht von aktuellen Browsern implementiert wird. Für die Raytracing-Komponente und die Synthese einer Impulse Response aus den Raytracing-Daten wurde ein WebGL2 Kontext verwendet, der ebenfalls Teil des HTML5-Standards ist. Hierbei wurde die Bibliothek \textit{three.js} \cite{three} für Vektor- und Matrixberechnung und Organisation der Szenendaten verwendet, der Renderprozess wurde jedoch selbst implementiert, da zur Zeit dieser Arbeit noch keine WebGL2-Unterstützung gegeben war.\\

Für den Test wurde eine einfache Umgebung modelliert, in der Raytracing von der Schallquelle aus durchgeführt wurde (siehe Grafik \ref{screenshotRoom}). 

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\input{graphs/screenshotRoom.pdf_tex}}
	\caption[Versuchsaufbau]{Versuchsaufbau. Dies ist ein Screenshot der modellierten Umgebung. Die Punkte dienen als Visualisierung der im Schallempfänger $s_E$ einfallenen Strahlen. Schallquelle $s_Q$ und Schallempfänger $s_E$ wurden zur Klarheit nachträglich markiert.}
	\label{screenshotRoom}
\end{figure}


\section{Ergebnisse}
\label{ergebnisse}
%
Da im Rahmen dieser Arbeit keine vergleichbaren Messungen in einer realen Umgebung durchgeführt wurden, können die Ergebnisse der Simulation nur vorgestellt und subjektiv bewertet werden. \\
In Grafik \ref{ir_example_simulated} wird eine durch die Simulation generierte Impulse Response dargestellt. Zum Vergleich wird in Grafik \ref{ir_example_measured} eine in einer realen Umgebung aufgenommene Impulse Response gezeigt. Bei beiden Verläufen lässt sich zunächst eine Verzögerung erkennen, welche sich durch den Weg, den der Schall schon auf dem kürzesten Weg zurücklegen muss, zurückführen lässt. In beiden Verläufen lässt sich in der Tendenz der Amplituden die umgekehrte Proportionalität wiederfinden, die zwischen dem Schalldruck und dem zurückgelegten Weg einer Schallwelle herrscht. Weiterhin sind in beiden Funktionen Ausschläge in unregelmäßigen Abständen zu finden. Diese könnten durch Flächen in entsprechender Distanz hervorgerufen werden, welche den Schall durch ihren Reflexionswinkel besonders gut in Richtung des Schallempfängers reflektieren. Ein auffälliger Unterschied zwischen den Funktionen ist, dass die Aufnahme auch negative Werte enthält. Dies ist darauf zurückzuführen, dass in einer realen Umgebung ein idealer positiver Impuls kaum realisierbar ist. Der begleitende negative Impuls breitet sich ebenso wie der positive Impuls im Raum aus und wird bei der Messung aufgenommen. Weiterhin klingt die aufgenommene Impulse Response länger als die synthetisierte, was sich allerdings auf Unterschiede in den Dimensionen des Raumes zurückführen lässt.\\
Subjektiv lässt sich über das durch die synthetisierte Impulse Response generierte Ausgangssignal sagen, dass es dem Hörempfinden einer ähnlichen Umgebung entspricht. Insbesondere Änderungen der Szene machen sich hörbar bemerkbar, so erweckt zum Beispiel die Skalierung des Raumes das Empfinden von verschieden großen Räumen, und Verschieben des Schallempfängers hinter ein Hindernis macht sich durch einen gedämpften Klang bemerkbar.\\

	\begin{figure}
		\centering
		
    	\subfigure[]{
    		\def\svgwidth{0.6\columnwidth}
    	\resizebox{0.45\textwidth}{!}{\includegraphics{IR-Example.png}}
    		\label{ir_example_simulated}
    	}
    	%\hspace{0.5cm}%
    	\subfigure[]{
    		\def\svgwidth{0.6\columnwidth}
	    	\resizebox{0.45\textwidth}{!}{\includegraphics{impulse.png}}
    		\label{ir_example_measured}
    	}
		\caption[Beispiele Impulse Response]{Graph (a) zeigt eine durch die Simulation synthetisierte Impulse Response, Graph (b) zeigt eine Impulse Response, die in einem akustischen Modell einer Konzerthalle gemessen wurde\cite{irexample}.}
		\label{ir_example}
	\end{figure}
	
Um die Lautstärke an verschiedenen Positionen des Raumes zu visualisieren, wurden die Impulse Responses an diesen Positionen gemessen und die Amplitudenwerte dieser Responses aufsummiert. In Grafik \ref{raumHeatmap} werden diese Werte normiert dargestellt. Hier ist auffällig, dass Bereiche, die durch Hindernisse verdeckt werden, teilweise scharfe Übergänge aufweisen. In realen Umgebungen sind meist eher fließende Übergänge von Lautstärken vorzufinden. Dieser Umstand lässt sich darauf zurückführen, dass einige Aspekte der Schallübertragung nicht modelliert wurden. So wurde zum einen Körperschall in dieser Arbeit nicht behandelt, wodurch Schallwellen, die sich in der Realität auch durch Objekte hindurch ausbreiten würden, hier komplett wegfallen. Darüber hinaus werden in der Realität Schallwellen, die sich nahe an Objekten vorbei ausbreiten, gebrochen, sodass ein Teil der Welle auch hinter dem Objekt zu hören ist. Auch dieser Aspekt wurde in dieser Arbeit nicht betrachtet. 

\begin{figure}
	\centering
  	\def\svgwidth{\columnwidth}
    	\resizebox{0.75\textwidth}{!}{\includegraphics{raumHeatmap.png}}
	\caption[Heatmap Schallintensität]{Darstellung der Gesamtintensität, gemessen an verschiedenen Punkten auf einer Ebene des Testraumes, dargestellt als Heatmap.}
	\label{raumHeatmap}
\end{figure}



%
